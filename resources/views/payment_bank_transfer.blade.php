@extends('layouts/master')

@section('css')

<style>
    @media (min-width: 1200px){
        .container {
            max-width: 900px !important;
        }
    }

    img.img-bank{
        position: absolute; 
        top:0;
        object-fit: contain;
    }

    button{
        background: none;
    }

    .line{
        height: 2px;
    }

</style>
    
@endsection

@section('master-content')
    <div class="">
        <h4 class="text-center py-4 bg-primary font-semiBold text-white mb-0">UBERVEST CHECKOUT</h4>
    </div>

    {{-- <div class="py-3" style="color: #e84855; background-color: #fdedee">
        <p class="text-center font-bold">Pay before JANUARY 01, 2021 00:00pm</p>
    </div> --}}
    
    <div class="container pb-5">
        <div class="w-100 my-5" style="border: 1px solid rgba(0,0,0,0.15);">
            <div class="row">
                <div class="col-12 col-md-6 order-2 order-md-1">
                    <div class="p-4">
                        <div class="mb-2">
                            <p>Virtual Account</p>
                            <div class="d-flex">
                                <h3 class="font-regular">{{ $response->account_number }}</h3>
                            </div>
                        </div>

                        <div class="mb-2">
                            <p>Virtual Account Name</p>
                            <div class="d-flex">
                                <h3 class="font-regular">{{ $response->name }}</h3>
                            </div>
                        </div>
                        
                        <div class="">
                            <p>Amount to Pay</p>
                            <div class="d-flex">
                                <h3 class="font-regular">Rp{{ number_format($response->expected_amount + 4500, 0, ',', '.') }}</h3>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-12 col-md-6 order-1 order-md-2">
                    <div class="d-flex h-100 p-4 mx-5 mx-md-0">
                        <div class="w-100 d-flex my-auto mx-md-5" style="position: relative; padding-top:50%;">
                            @switch(Request::segment(3))
                                @case('mandiri')
                                    <img class="img-bank w-100 h-100 my-auto" src="{{ asset('images/bank_icon/ic_mandiri.png') }}" alt="">
                                    @break
                                @case('bri')
                                    <img class="img-bank w-100 h-100 my-auto" src="{{ asset('images/bank_icon/ic_bri.png') }}" alt="">
                                    @break
                                @case('bni')
                                    <img class="img-bank w-100 h-100 my-auto" src="{{ asset('images/bank_icon/ic_bni.png') }}" alt="">
                                    @break
                                @case('permata')
                                    <img class="img-bank w-100 h-100 my-auto" src="{{ asset('images/bank_icon/ic_permata.png') }}" alt="">
                                    @break
                                @case('bca')
                                    <img class="img-bank w-100 h-100 my-auto" src="{{ asset('images/bank_icon/ic_bca.png') }}" alt="">
                                    @break
                                @default
                            @endswitch
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="d-flex">
            <a class="btn btn-primary px-5 py-2 mx-auto" href="{{ url('/purchase_package/buy') }}">
                Back to Dashboard
            </a>
        </div>

        {{-- <div class="">
            <div class="d-flex">
                <div>
                    <button class="btn btn-method p-0 w-100">
                        <p class="px-4 mb-3">ATM</p>
                        <div class="line w-100 bg-primary"></div>
                    </button>
                </div>
                
                <div>
                    <button class="btn btn-method p-0">
                        <p class="px-4 mb-3">IBANKING</p>
                        <div class="line w-100 bg-primary d-none"></div>
                    </button>
                </div>

                <div>
                    <button class="btn btn-method p-0">
                        <p class="px-4 mb-3">MBANKING</p>
                        <div class="line w-100 bg-primary d-none"></div>
                    </button>
                </div>
            </div>
            <hr class="mt-0">
        </div> --}}

        {{-- <div class="mb-4">
            <p class="font-bold mb-3">STEP 1: FIND NEAREST ATM</p>
            <p class="mb-3">1. Insert your ATM card and PIN</p>
            <p class="mb-3">2. Insert your ATM PIN</p>
        </div>

        <div>
            <p class="font-bold mb-3">STEP 2: FIND NEAREST ATM</p>
            <p class="mb-3">1. Select Menu "Other Transaction"</p>
            <p class="mb-3">2. Insert your ATM PIN</p>
        </div> --}}

    </div>
@endsection

@section('javascript')
@endsection