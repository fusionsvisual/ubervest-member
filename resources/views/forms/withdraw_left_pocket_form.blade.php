@extends('layouts/dashboard_master')

@section('title')
    WITHDRAWAL REQUEST
@endsection

@section('content')
<div class="row d-flex">

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Date</p>
            <input class="form-control" type="text" name="" value="{{ date_format(date_create($withdraw->created_at), "d F Y") }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Status</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->status->status_name }}" disabled>
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Username</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->username }}" disabled>
        </div>
    </div>

    {{-- <div class="col-12">
            <div class="mb-4">
                <p class="font-semiBold mb-2">Amount</p>
                <input class="form-control" type="text" name="" value="Rp{{number_format($withdraw->amount, 0, ',', '.')}}" disabled>
</div>
</div> --}}

<div class="col-12">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Description</p>
        <textarea class="form-control" name="" id="" cols="1" rows="5" disabled>{{ $withdraw->description }}</textarea>
        {{-- <input class="form-control" type="text" name="" value="{{ $withdraw->description }}" disabled> --}}
    </div>
</div>

<div class="col-12">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Bank Code</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->bank_code }}" disabled>
    </div>
</div>

<div class="col-12 col-md-6">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Account Name Holder</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_holder_name }}" disabled>
    </div>
</div>

<div class="col-12 col-md-6">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Account Number</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_number }}" disabled>
    </div>
</div>

<div class="col-12">
    <div class="table-responsive mb-2">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Package</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Seasonal Withdraw</td>
                    <td>${{number_format($withdraw->seasonal_withdraw_amount, 0, ',', '.')}}</td>
                </tr>

                <tr>
                    <td>2</td>
                    <td>Fast Track Withdraw</td>
                    <td>Rp{{number_format($withdraw->fast_track_withdraw_amount, 0, ',', '.')}}</td>
                </tr>

                <tr>
                    <td>3</td>
                    <td>Link Up Withdraw</td>
                    <td>Rp{{number_format($withdraw->link_up_withdraw_amount, 0, ',', '.')}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="col-12">
    <div class="d-flex mb-5">
        <p class="ml-auto text-large font-bold">Total: Rp{{number_format(($withdraw->seasonal_withdraw_amount + $withdraw->fast_track_withdraw_amount + $withdraw->link_up_withdraw_amount), 0, ',', '.')}}</p>
    </div>
</div>
</div>
@endsection