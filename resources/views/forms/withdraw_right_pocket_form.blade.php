@extends('layouts/dashboard_master')

@section('title')
    WITHDRAWAL REQUEST
@endsection

@section('content')
<div class="row d-flex">

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Date</p>
            <input class="form-control" type="text" name="" value="{{ date_format(date_create($withdraw->created_at), "d F Y") }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Type</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->type }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Status</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->status->status_name }}" disabled>
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Username</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->username }}" disabled>
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Amount</p>
            <input class="form-control" type="text" name="" value="Rp{{number_format($withdraw->amount, 0, ',', '.')}}" disabled>
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Description</p>
            <textarea class="form-control" name="" id="" cols="1" rows="5" disabled>{{ $withdraw->description }}</textarea>
            {{-- <input class="form-control" type="text" name="" value="{{ $withdraw->description }}" disabled> --}}
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Bank Code</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->bank_code }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Account Name Holder</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_holder_name }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Account Number</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_number }}" disabled>
        </div>
    </div>

    {{-- <div class="col-12">
        <div class="d-flex mb-5 flex-column flex-md-row">
            <a class="btn btn-danger mr-md-2 ml-md-auto mb-3 mb-md-0 py-3 px-5" href="{{ url('admin/withdrawal_request/right_pocket/reject/'.$withdraw->id) }}">Reject</a>
            <a class="btn btn-primary py-3 px-5" href="{{ url('admin/withdrawal_request/right_pocket/send/'.$withdraw->id) }}">Send</a>
        </div>
    </div> --}}
</div>
@endsection