@extends('layouts/dashboard_master')

@section('title')
    WITHDRAW HISTORY
@endsection

@section('dashboard-css')

<style>
    @media only screen and (max-width: 768px) {
        .page-content{
            padding-top: 150px!important;
        }
    }
</style>
    
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-md-8 mb-3 d-none d-md-block">
        {{-- <input class="form-control" type="text" placeholder="Search"> --}}
    </div>

    <div class="col-12 col-md-4 mb-4">
        <div class="row d-flex flex-wrap">
            <div class="col-5">
                <input class="form-control" type="text" name="start_date">
            </div>
            <div class="col-2 d-flex px-0">
                <p class="m-auto text-muted">sampai</p>
            </div>
            <div class="col-5">
                <input class="form-control" type="text" name="end_date">
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Date</th>
                  <th scope="col">Type</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody class="item-list">
                  @foreach ($withdraws->data as $idx => $withdraw)
                  <tr>
                    <td scope="row">{{ $idx + 1 }}</td>
                    <td>{{ date_format(date_create($withdraw->created_at), "d F Y") }}</td>
                    <td>{{ $withdraw->type }}</td>
                    <td>Rp{{number_format(($withdraw->amount), 0, ',', '.')}}</td>
                    <td>{{ $withdraw->status->status_name }}</td>
                    <td>
                        <div class="d-flex">
                          <a class="btn btn-primary mr-2" href="{{ url('withdraw_history/right_pocket/'.$withdraw->id) }}">
                            <p>Detail</p>
                          </a>
                        </div>
                      </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>

          <div class="pagination-container">
            @include('templates.pagination', ["items" => $withdraws, "page" => $page])
          </div>
    </div>
</div>
@endsection

@section('dashboard-javascript')
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>

<script>
    $url = "{{ url('withdraw_history/right_pocket/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $withdraws->total/$withdraws->per_page }}"

    $(document).ready(function(){

        // setupButton();

        $('input[name="start_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $startDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth(), 1));

        $('input[name="end_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top right",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $endDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0));
    });

    // $('input[name="search"]').keyup(function (e) { 
    //     if ($(this).val() != ""){
    //         $isEmpty = false;
    //     }

    //     if (!$isEmpty){
    //         $page = 1
    //         $search_query = $(this).val();
    //         filter();
    //     }

    //     if ($(this).val() == ""){
    //           $isEmpty = true;
    //     }
    // });
</script>
@endsection