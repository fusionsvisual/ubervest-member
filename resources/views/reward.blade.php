@extends('layouts/dashboard_master')

@section('title')
    BONUS KANTONG KIRI
@endsection

@section('dashboard-css')

<style>
    @media only screen and (max-width: 768px) {
        .page-content{
            padding-top: 150px!important;
        }
    }
</style>
    
@endsection

@section('content')
{{-- <p></p> --}}

    <div class="row">
        @if(Session::has('response'))   
        <div class="col-12">
            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
        </div>
        @endif
        
        @foreach ($rewards as $reward)
            <div class="col-12 col-md-4">
                <div class="flex-rectangle mb-4">
                    <div class="" style="position:relative; padding-top:60%;">
                        <form method="POST">
                            @csrf
                        <button class="btn w-100 p-0" style="border:0; background:red;" @if (Session::get('user.data')->left_total < $reward->left || Session::get('user.data')->right_total < $reward->right || $reward->is_claimed ||  $reward->is_reviewed) disabled @endif>
                            <img class="reward-image h-100 w-100" src="{{ Network::ASSET_URL.$reward->reward_image }}">
                            @if (Session::get('user.data')->left_total >= $reward->left && Session::get('user.data')->right_total >= $reward->right && !$reward->is_claimed && !$reward->is_reviewed)
                                <div class="reward-overlay w-100 h-100 d-flex" style="position: absolute; top:0; left:0; background:#33D94E;">
                                    <h3 class="m-auto font-white">CLAIM</h3>
                                </div>
                            @elseif($reward->is_claimed)
                            <div class="w-100 h-100 d-flex" style="position: absolute; top:0; left:0; background:rgba(51, 217, 78, 0.75);">
                                <h3 class="m-auto font-white">CLAIMED</h3>
                            </div>
                            @elseif($reward->is_reviewed)
                            <div class="w-100 h-100 d-flex" style="position: absolute; top:0; left:0; background:rgba(194, 24, 8, 0.75);">
                                <h3 class="m-auto font-white">REVIEWED</h3>
                            </div>
                            @else
                                <div class="w-100 h-100 d-flex" style="position: absolute; top:0; left:0; background:rgba(0,0,0,0.5);">
                                    <h3 class="m-auto font-white">UNAVAILABLE</h3>
                                </div>
                            @endif
                            <input type="hidden" name="reward_id" id="" value="{{ $reward->id }}">
                        </button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection