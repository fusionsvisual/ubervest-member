@extends('layouts/dashboard_master')

@section('title')
    SUBMINE BONUS
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px) {
        .page-content{
            padding-top: 150px!important;
        }
    }
</style>
    
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-xl-4">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-xl-12 mb-4">
                <div class="bonus-container py-4 py-xl-5 px-4" style="background: url('../images/bg_seasonal_bonus.png');">
                    <div class="d-flex justify-content-between mb-3">
                        <p class="d-inline-block font-medium font-white ">Seasonal Bonus</p>
                        <p class="d-inline-block font-white ">31 Dec 2020</p>
                    </div>
                    <h2 class="font-white font-bold mb-4">${{ number_format(Session::get('user.data')->seasonal_bonus, 0, ',', '.') }}</h2>
                    <h4 class="font-white font-regular">${{ number_format(Session::get('user.data')->total_seasonal_bonus, 0, ',', '.') }}</h4>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-xl-12 mb-4">
                <div class="bonus-container py-4 py-xl-5 px-4" style="background: url('../images/bg_fast_track.png');">
                    <div class="d-flex justify-content-between mb-3">
                        <p class="d-inline-block font-medium font-white ">Fast Track Bonus</p>
                    </div>
                    <h2 class="font-white font-bold mb-4">Rp{{ number_format(Session::get('user.data')->fast_track_bonus, 0, ',', '.') }}</h2>
                    <h4 class="font-white font-regular">Rp{{ number_format(Session::get('user.data')->total_fast_track_bonus, 0, ',', '.') }}</h4>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-xl-12 mb-4">
                <div class="bonus-container py-4 py-xl-5 px-4" style="background: url('../images/bg_link_up.png');">
                    <div class="d-flex justify-content-between mb-3">
                        <p class="d-inline-block font-medium font-white ">Link Up Bonus</p>
                    </div>
                    <h2 class="font-white font-bold mb-4">Rp{{number_format(Session::get('user.data')->link_up_bonus, 0, ',', '.')}}</h2>
                    <h4 class="font-white font-regular">Rp{{ number_format(Session::get('user.data')->total_link_up_bonus, 0, ',', '.') }}</h4>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-xl-12 mb-4">
                <div class="bonus-container py-4 py-xl-5 px-4" style="background: url('../images/bg_total_bonus.png');">
                    <div class="d-flex justify-content-between mb-3">
                        <p class="d-inline-block font-medium font-white ">Total Bonus</p>
                    </div>
                    <h2 class="font-white font-bold mb-4">Rp{{number_format((Session::get('user.data')->link_up_bonus + Session::get('user.data')->fast_track_bonus), 0, ',', '.')}}</h2>
                    <h4 class="font-white font-regular">Rp{{number_format((Session::get('user.data')->total_link_up_bonus + Session::get('user.data')->total_fast_track_bonus), 0, ',', '.')}}</h4>
                </div>
            </div>
            
        </div>
    </div>

    <div class="col-12 col-xl-8">
        <div class="ml-0 ml-xl-5">
            <div class="rounded-container p-4 p-lg-5">
                <form action="{{ url('left_bonus/bonus') }}" method="POST" onsubmit="$('.btn-withdraw').prop('disabled', true); return true;">
                    @csrf   
                    @if(Session::has('response'))   
                    <div class="">
                        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                    </div>
                    @endif
                    <h4 class="font-medium text-large mb-4">Withdrawal</h4>
    
                    <div class="row mb-4">
                        <div class="col-4 d-flex">
                            <p class="mb-3 my-auto">Seasonal</p>
                        </div>
                        <div class="col-8">
                            <input class="form-control w-100" type="number" name="seasonal" id="" placeholder="$0">
                        </div>
                    </div>
        
                    <div class="row mb-4">
                        <div class="col-4 d-flex">
                            <p class="mb-3 my-auto">Fast Track</p>
                        </div>
                        <div class="col-8">
                            <input class="form-control w-100" type="number" name="fast_track" id="" placeholder="Rp0">
                        </div>
                    </div>
        
                    <div class="row mb-4">
                        <div class="col-4 d-flex">
                            <p class="mb-3 my-auto">Link Up</p>
                        </div>
                        <div class="col-8">
                            <input class="form-control w-100" type="number" name="link_up" id="" placeholder="Rp0">
                        </div>
                    </div>
        
                    <div class="row mb-4">
                        <div class="col-4 d-flex">
                            <p class="mb-3">Keterangan</p>
                        </div>
                        <div class="col-8">
                            <textarea class="form-control" name="description" id="" cols="1" rows="6" style="box-shadow: unset; border: 1px solid #ced4da !important;" required></textarea>
                        </div>
                    </div>
        
                    </div>
            
                    <button class="btn btn-withdraw btn-primary w-100 mb-5" style="height:50px!important;">Withdraw</button>
                </form>
        </div>
        

    </div>

</div>
@endsection

@section('dashboard-javascript')
    <script>
    </script>
@endsection