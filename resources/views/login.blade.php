@extends('layouts/master')
@section('master-content')

<div class="forgot-password-container d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:4;">
    <div class="forgot-password-overlay" style="position: fixed; top:0; left:0; right:0; bottom:0; background:rgba(0,0,0,0.5);"></div>
    <div class="m-auto p-4 p-md-5" style="z-index:5; background: white; border-radius:10px; width:450px; max-width:90%;">
        <form action="{{ url('forgot_password') }}" method="POST">
            @csrf
            <p class="text-large font-semiBold mb-3 text-center">Forgot Password</p>
            <div class="input-container mb-3" style="border:0px;">
                <img class="d-inline" src="{{ asset('images/ic_profile.svg') }}" alt="">
                <input type="email" name="email" placeholder="Email" class="d-inline form-control p-3">
            </div>

            <div class="row mt-4">
                <div class="col-12 col-md-6 pr-2">
                    <input type="submit" class="btn btn-cancel btn-outline muli-bolder d-block w-100 text-center" value="Cancel" style="border: 1px solid #999999; color:#999999; background:transparent; border-radius: 20px">
                </div>
                <div class="col-12 col-md-6 pl-2">
                    <input type="submit" class="btn btn-save-change-password muli-bolder d-block w-100 d-block text-center" value="Save" style="border-radius: 20px">
                </div>
            </div>

        </form>
    </div>
</div>

<div id="login" class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <form class="col-12 d-flex" method="POST" action="{{ url('login') }}">
            @csrf
            <div class=""></div>
            <div class="d-inline-block mx-auto" style="width:500px">
                <img class="w-100" src="{{ asset('images/ic_logo.png') }}" alt="">
                <div class="bg-white p-md-5">
                    <div class="mb-3">
                        <h4 class="muli-bold text-center mb-4">Sign in to your account</h4>
                        @isset($message)
                            <p class="empty-state mt-2" style="color:red">**{{ $message}}</p>
                        @endisset
                        <p class="empty-state my-2 d-none" style="color:red">Please fill the blanks.</p>
                        @if(Session::has('response'))   
                            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                        @endif 
                    </div>
                    
                    <div class="input-container mb-3" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_profile.svg') }}" alt="">
                        <input name="username" type="text" placeholder="Username" class="d-inline form-control p-3">
                    </div>

                    <div class="input-container mb-4" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input name="password" type="password" placeholder="Password" class="d-inline form-control p-3">
                    </div>

                    <input type="submit" class="btn muli-bolder d-block w-100 d-block text-center" value="Login" style="border-radius: 20px">
                    <div class="d-flex mt-3">
                        <button class="btn btn-forgot-password btn-transparent mx-auto font-semiBold text-primary p-0" >Forgot Password?</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script>

    $('.btn-forgot-password').click(function (e) { 
        e.preventDefault();
        $('.forgot-password-container').addClass('d-flex');
        $('.forgot-password-container').removeClass('d-none');
    });

    $('.btn-cancel').click(function (e) { 
        e.preventDefault();
        $('.forgot-password-container').addClass('d-none');
        $('.forgot-password-container').removeClass('d-flex');
    });

    $('.forgot-password-overlay').click(function (e) {
        $('.forgot-password-container').addClass('d-none');
        $('.forgot-password-container').removeClass('d-flex');
    });
</script>
@endsection
