@extends('layouts/dashboard_master')

@section('title')
PURCHASE PACKAGE
@endsection

@section('dashboard-css')
<style>
  @media only screen and (max-width: 768px) {
    .page-content {
      padding-top: 100px !important;
    }

    .right-sidebar-toggler {
      display: block !important;
    }
  }
</style>
@endsection

@section('content')

@if(Session::has('response'))   
<p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
@endif

<h4 class="font-medium text-large mb-4">Choose Package</h4>
<form action="{{ url('purchase_package/buy') }}" method="POST">
  @csrf
  <div class="table-responsive mb-4 mb-md-4">
    <table class="table">
      <thead>
        <tr>
          <th scope="col" style="width:10%">No</th>
          <th scope="col" style="width:40%">Package</th>
          <th scope="col" style="width:20%">Quantity</th>
          <th scope="col" style="width:30%">Amount</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($packages as $idx => $package)
        <tr>
          <td scope="row">{{ $idx+1 }}</td>
          <td>{{ $package->package_name }}</td>
          <td><input class="form-control" type="number" placeholder="0" name="{{ "package".$package->id }}" style="width: 100px" min="0" pattern="\d*" value="0"></td>
          <td>Rp{{ number_format($package->package_price, 0, ',', '.') }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  
  <div class="row">
  
    <div class="col-12 col-md-7 mb-5 mb-md-1">
      <h4 class="font-medium text-large mb-4">Send Invoice to</h4>
      <div class="row">
  
        <div class="col-12 col-md-6 mb-4">
          <p class="mb-2">Email</p>
          <input class="form-control" type="email" name="email" id="" required>
        </div>
  
        <div class="col-12 col-md-6 mb-4">
          <p class="mb-2">No. Telp</p>
          <input class="form-control" type="text" name="phone_number" id="" required>
        </div>
  
        <div class="col-12">
          <p class="mb-2">Keterangan</p>
          <textarea class="form-control" name="description" id="" cols="1" rows="6" required></textarea>
        </div>
  
      </div>
    </div>
  
    <div class="col-12 col-md-5 pl-md-5 mb-5">
      <h4 class="font-medium text-large mb-4">Rekap Belanja</h4>
  
      <div class="card w-100 mb-4" style="width: 18rem;">
        <ul class="list-group list-group-flush">
  
          <li class="list-group-item">
            <div class="row">
              <div class="col-6">
                <p>Start-Up</p>
              </div>
              <div class="col-6">
                <p class="sup-subtotal text-right">IDR 0</p>
              </div>
            </div>
          </li>
  
          <li class="list-group-item">
            <div class="row">
              <div class="col-6">
                <p>Pro</p>
              </div>
              <div class="col-6">
                <p class="pp-subtotal text-right">IDR 0</p>
              </div>
            </div>
          </li>
  
          <li class="list-group-item">
            <div class="row">
              <div class="col-6">
                <p>Upgrade</p>
              </div>
              <div class="col-6">
                <p class="upgrade-subtotal text-right">IDR 0</p>
              </div>
            </div>
          </li>
  
          <li class="list-group-item">
            <div class="row">
              <div class="col-6">
                <p>Total</p>
              </div>
              <div class="col-6">
                <p class="total text-right font-secondary">Rp 0</p>
                <input type="hidden" name="total_amount" value="">
              </div>
            </div>
          </li>
  
        </ul>
      </div>
  
      <input type="submit" class="btn w-100 btn-primary" value="Proceed">
    </div>
  </div>
</form>
@endsection

@section('dashboard-javascript')
<script>

  $supSubtotal = 0;
  $ppSubtotal = 0;
  $upgradeSubtotal = 0;

  $('input[name="package1"]').change(function () { 
    $price = "{{ json_encode($packages[0]->package_price) }}";
    $supSubtotal = $(this).val()*$price;
    $('.sup-subtotal').text("IDR "+($(this).val()*$price).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    updateTotal();
  });

  $('input[name="package2"]').change(function () { 
    $price = "{{ json_encode($packages[1]->package_price) }}";
    $ppSubtotal = $(this).val()*$price;
    $('.pp-subtotal').text("IDR "+$ppSubtotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    updateTotal();
  });

  $('input[name="package3"]').change(function () { 
    $price = "{{ json_encode($packages[2]->package_price) }}";
    $upgradeSubtotal = $(this).val()*$price;
    $('.upgrade-subtotal').text("IDR "+$upgradeSubtotal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
    updateTotal();
  });

  $('form').submit(function (e) { 
    $('input[type="submit"]').prop('disabled', true); 
    return true
  });

  function updateTotal(){
    $('input[name="total_amount"]').val($supSubtotal + $ppSubtotal + $upgradeSubtotal);
    $('.total').text("IDR "+($supSubtotal + $ppSubtotal + $upgradeSubtotal).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
  }

</script>
@endsection