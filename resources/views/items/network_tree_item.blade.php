<div class="tree" style="overflow: scroll">
    <ul>
        <li>
            <button class="btn btn-network-tree p-0" data-id="{{ $network_tree[0]->id }}">
                <div class="branch d-flex flex-column">
                    <p class="font-bold text-small mx-auto pt-2">{{ $network_tree[0]->username }}</p>
                    <p class="phone_number text-small pb-2">{{ $network_tree[0]->phone_number }}</p>
                    <p class="package text-small text-muted py-2">{{ explode("_", $network_tree[0]->e_pin)[0] }} Package</p>
                    <p class="deposit font-bold text-small font-secondary py-2">${{ number_format($network_tree[0]->deposit, 0, ',', '.') }}</p>
                    <div class="row d-flex mx-0 flex-grow-1" style="min-height: 100%">
                        <div class="left-leg col-6 p-2 d-flex">
                            <div class="m-auto">
                                <p class="text-small font-bold">Left</p>
                                <p class="text-small">{{ $network_tree[0]->left_total }}</p>
                            </div>
                        </div>
                        <div class="col-6 p-2 d-flex">
                            <div class="m-auto">
                                <p class="text-small font-bold">Right</p>
                                <p class="text-small">{{ $network_tree[0]->right_total }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </button>
            @if(count($network_tree) >= 2)
                <ul>
                    <li>
                        @php
                            $left_upline = null;
                        @endphp
                        @if(count($network_tree[1]) > 0)
                            @foreach($network_tree[1] as $idx => $tree)
                                @if($tree->leg == "LEFT")
                                    @php
                                        $left_upline = $tree;
                                    @endphp
                                    <button class="btn btn-network-tree p-0" data-id="{{ $tree->id }}">
                                        <div class="branch d-flex flex-column">
                                            <p class="font-bold text-small mx-auto pt-2">{{ $tree->username }}</p>
                                            <p class="phone_number text-small pb-2">{{ $tree->phone_number }}</p>
                                            <p class="package text-small text-muted py-2">{{ explode("_", $tree->e_pin)[0] }} Package</p>
                                            <p class="deposit font-bold text-small font-secondary py-2">${{ number_format($tree->deposit, 0, ',', '.') }}</p>
                                            <div class="row d-flex mx-0 flex-grow-1" style="min-height: 100%">
                                                <div class="left-leg col-6 p-2 d-flex">
                                                    <div class="m-auto">
                                                        <p class="text-small font-bold">Left</p>
                                                        <p class="text-small">{{ $tree->left_total }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6 p-2 d-flex">
                                                    <div class="m-auto">
                                                        <p class="text-small font-bold">Right</p>
                                                        <p class="text-small">{{ $tree->right_total }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </button>
                                @endif
                            @endforeach

                            @if ($left_upline != null)
                                @if(count($network_tree) >= 3)
                                    <ul class="d-none d-md-block">
                                        @for($i = 0; $i < 2; $i++)
                                            <li>    
                                                @php
                                                    $third_tree = null;
                                                @endphp
    {{-- {{ $network_tree[1][1]->id }} --}}
                                                @foreach($network_tree[2] as $index => $tree_item)
                                                    @if($tree_item->upline_user_id == $left_upline->id)
                                                        @if($i == 0 & $tree_item->leg == "LEFT")
                                                            @php
                                                                $third_tree = $tree_item;
                                                            @endphp
                                                        @elseif($i == 1 & $tree_item->leg == "RIGHT")
                                                            @php
                                                                $third_tree = $tree_item;
                                                            @endphp
                                                        @endif
                                                    @endif
                                                @endforeach

                                                @if ($third_tree != null)
                                                    <button class="btn btn-network-tree p-0" data-id="{{ $third_tree->id }}">
                                                        <div class="branch">
                                                            <p class="font-bold text-small mx-auto pt-2">{{ $third_tree->username }}</p>
                                                            <p class="phone_number text-small pb-2">{{ $third_tree->phone_number }}</p>
                                                            <p class="package text-small text-muted py-2">{{ explode("_", $third_tree->e_pin)[0] }} Package</p>
                                                            <p class="deposit font-bold text-small font-secondary py-2">${{ number_format($third_tree->deposit, 0, ',', '.') }}</p>
                                                            <div class="row d-flex mx-0">
                                                                <div class="left-leg col-6 p-2">
                                                                    <p class="text-small font-bold">Left</p>
                                                                    <p class="text-small">{{ $third_tree->left_total }}</p>
                                                                </div>
                                                                <div class="col-6 p-2">
                                                                    <p class="text-small font-bold">Right</p>
                                                                    <p class="text-small">{{ $third_tree->right_total }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                @else
                                                    <button class="btn btn-register-tree p-0" data-leg="@if($i%2==0) LEFT @else RIGHT @endif" data-upline-id="{{ $left_upline->id }}" data-upline-name="{{ $left_upline->username }}">
                                                        <div class="branch d-flex">
                                                            <div class="m-auto">
                                                                <p class="font-bold">Add +</p>
                                                                <p>New Member</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                @endif
                                            </li>
                                        @endfor
                                    </ul> 
                                    @else  
                                    <ul>
                                        
                                    </ul>                                 
                                @endif
                            @else
                                <button class="btn btn-register-tree p-0" data-leg="LEFT" data-upline-id="{{ $network_tree[0]->id }}" data-upline-name="{{ $network_tree[0]->username }}">
                                    <div class="branch d-flex">
                                        <div class="m-auto">
                                            <p class="font-bold">Add +</p>
                                            <p>New Member</p>
                                        </div>
                                    </div>
                                </button>
                            @endif
                        @else
                            <button class="btn btn-register-tree p-0" data-leg="LEFT" data-upline-id="{{ $network_tree[0]->id }}" data-upline-name="{{ $network_tree[0]->username }}">
                                <div class="branch d-flex">
                                    <div class="m-auto">
                                        <p class="font-bold">Add +</p>
                                        <p>New Member</p>
                                    </div>
                                </div>
                            </button>
                        @endif
                    </li>

                    <li>
                        @php
                            $right_upline = null;
                        @endphp
                        @if(count($network_tree[1]) > 0)
                            @foreach($network_tree[1] as $idx => $tree)
                                @if($tree->leg == "RIGHT")
                                    @php
                                        $right_upline = $tree;
                                    @endphp
                                    <button class="btn btn-network-tree p-0" data-id="{{ $tree->id }}">
                                        <div class="branch d-flex flex-column">
                                            <p class="font-bold text-small mx-auto pt-2">{{ $tree->username }}</p>
                                            <p class="phone_number text-small pb-2">{{ $tree->phone_number }}</p>
                                            <p class="package text-small text-muted py-2">{{ explode("_", $tree->e_pin)[0] }} Package</p>
                                            <p class="deposit font-bold text-small font-secondary py-2">${{ number_format($tree->deposit, 0, ',', '.') }}</p>
                                            <div class="row d-flex mx-0 flex-grow-1" style="min-height: 100%">
                                                <div class="left-leg col-6 p-2 d-flex">
                                                    <div class="m-auto">
                                                        <p class="text-small font-bold">Left</p>
                                                        <p class="text-small">{{ $tree->left_total }}</p>
                                                    </div>
                                                </div>
                                                <div class="col-6 p-2 d-flex">
                                                    <div class="m-auto">
                                                        <p class="text-small font-bold">Right</p>
                                                        <p class="text-small">{{ $tree->right_total }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </button>
                                @else
                                    @if(count($network_tree[1]) == 1)
                                        <button class="btn btn-register-tree p-0" data-leg="RIGHT" data-upline-id="{{ $network_tree[0]->id }}" data-upline-name="{{ $network_tree[0]->username }}">
                                            <div class="branch d-flex">
                                                <div class="m-auto">
                                                    <p class="font-bold">Add +</p>
                                                    <p>New Member</p>
                                                </div>
                                            </div>
                                        </button>
                                    @endif
                                @endif
                            @endforeach
                            @if ($right_upline != null)
                                <ul class="d-none d-md-block">
                                    @for($i = 0; $i < 2; $i++)
                                        <li>
                                        
                                                @php
                                                    $third_tree = null;
                                                @endphp

                                                @foreach($network_tree[2] as $index => $tree_item)
                                                    @if($tree_item->upline_user_id == $right_upline->id)
                                                        @if($i == 0 & $tree_item->leg == "LEFT")
                                                            @php
                                                                $third_tree = $tree_item;
                                                            @endphp
                                                        @elseif($i == 1 & $tree_item->leg == "RIGHT")
                                                            @php
                                                                $third_tree = $tree_item;
                                                            @endphp
                                                        @endif
                                                    @endif
                                                @endforeach

                                                @if ($third_tree != null)
                                                    <button class="btn btn-network-tree p-0" data-id="{{ $third_tree->id }}">
                                                        <div class="branch">
                                                            <p class="font-bold text-small mx-auto pt-2">{{ $third_tree->username }}</p>
                                                            <p class="phone_number text-small pb-2">{{ $third_tree->phone_number }}</p>
                                                            <p class="package text-small text-muted py-2">{{ explode("_", $third_tree->e_pin)[0] }} Package</p>
                                                            <p class="deposit font-bold text-small font-secondary py-2">${{ number_format($third_tree->deposit, 0, ',', '.') }}</p>
                                                            <div class="row d-flex mx-0">
                                                                <div class="left-leg col-6 p-2">
                                                                    <p class="text-small font-bold">Left</p>
                                                                    <p class="text-small">{{ $third_tree->left_total }}</p>
                                                                </div>
                                                                <div class="col-6 p-2">
                                                                    <p class="text-small font-bold">Right</p>
                                                                    <p class="text-small">{{ $third_tree->right_total }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </button>
                                                @else
                                                    <button class="btn btn-register-tree p-0" data-leg="@if($i%2==0) LEFT @else RIGHT @endif" data-upline-id="{{ $right_upline->id }}" data-upline-name="{{ $right_upline->username }}">
                                                        <div class="branch d-flex">
                                                            <div class="m-auto">
                                                                <p class="font-bold">Add +</p>
                                                                <p>New Member</p>
                                                            </div>
                                                        </div>
                                                    </button>
                                                @endif
                                        </li>
                                    @endfor
                                </ul>
                            @endif  
                        @else
                            <button class="btn btn-register-tree p-0" data-leg="RIGHT" data-upline-id="{{ $network_tree[0]->id }}" data-upline-name="{{ $network_tree[0]->username }}">
                                <div class="branch d-flex">
                                    <div class="m-auto">
                                        <p class="font-bold">Add +</p>
                                        <p>New Member</p>
                                    </div>
                                </div>
                            </button>
                        @endif
                    </li>
                </ul>
            @endif
        </li>
    </ul>
</div>