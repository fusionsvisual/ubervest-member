@foreach($items->data as $idx => $leg)
    <tr>
        <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
        <td>{{ $leg->username }}</td>
        <td>{{ $leg->upline_user->username }}</td>
        <td>@isset($leg->package->package_code){{ $leg->package->package_code }}@endisset</td>
        <td>Level {{ $leg->level - (Session::get('user.data')->level - 1) }}</td>
    </tr>
@endforeach
