@foreach ($items->data as $idx => $sharing)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($sharing->created_at), "d F Y") }}</td>
    <td>{{$sharing->profit_from_user_id->username }}</td>
    <td>{{$sharing->profit }}</td>
</tr>
@endforeach