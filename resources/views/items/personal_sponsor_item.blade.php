@foreach ($items->data as $idx => $member)
<tr>
    <td scope="row">{{ $idx+1 }}</td>
    <td>{{ $member->username }}</td>
    <td>{{ $member->upline_user->username }}</td>
    <td>{{ $member->package->package_code }}</td>
    <td>Level {{ $member->level }}</td>
    <td>
        <div>
            <button class="btn btn-upgrade btn-edit px-4" data-id="{{ $member->id }}" data-username="{{ $member->username }}" @if($member->package->package_code == "PP") disabled style="background:rgba(0,0,0,0.25); color:white;" @endif>
                Upgrade
            </button>
        </div>
    </td>
</tr>
@endforeach