@foreach ($items->data as $idx => $history)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($history->created_at), "d F Y") }}</td>
    <td>{{ $history->invoice_code }}</td>
    <td>Rp{{number_format($history->total, 0, ',', '.')}}</td>
    <td class="font-semiBold" style="@switch($history->status->status_name)
        @case(OrderStatus::PAID)
        color:rgba(32,186,56,1)
        @break
        @case(OrderStatus::UNPAID)
        color:rgba(254,97,0,1)
        @break
        @case(OrderStatus::WAITING)
        color:rgba(255,209,90,1)
        @break
        @default
        color:rgba(255,0,0,1)
        @break
     @endswitch">{{ $history->status->status_name }}</td>
    <td>
        <div class="d-flex">
            <a class="btn btn-primary" href="{{ url('purchase_package/history/'.$history->id) }}">
                <p>Detail</p>
            </a>
        </div>
    </td>
</tr>
@endforeach