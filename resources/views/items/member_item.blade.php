@foreach ($items->data as $idx => $member)
    <tr>
        <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
        <td>{{ $member->username }}</td>
        <td>{{ $member->heir_name }}</td>
        <td>SUP</td>
        <td>Level {{ $member->level }}</td>
    </tr>
@endforeach