@foreach ($items->data as $idx => $withdraw)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($withdraw->created_at), "d F Y") }}</td>
    <td>Rp{{number_format(($withdraw->fast_track_withdraw_amount + $withdraw->link_up_withdraw_amount), 0, ',', '.')}}</td>
    <td>Pending</td>
    <td>
        <div class="d-flex">
            <a class="btn btn-primary mr-2" href="{{ url('withdraw_history/left_pocket/'.$withdraw->id) }}">
                <p>Detail</p>
            </a>
        </div>
    </td>
</tr>
@endforeach