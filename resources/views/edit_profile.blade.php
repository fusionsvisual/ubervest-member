@extends('layouts/dashboard_master')

@section('title')
    EDIT PROFILE
@endsection

@section('dashboard-css')
    <style>
      @media only screen and (max-width: 768px) {
        .page-content{
            padding-top: 100px!important;
        }
      } 
    </style>
@endsection

@section('content')
    <div class="mx-1">

        @if(Session::has('response'))   
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
        @endif
        
        <form action="{{ url('edit_profile') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-12 mb-4">
                    <p class="font-semiBold text-primary">Network Information</p>
                </div>
                <div class="col-12 col-md-6 mb-md-4">
                    <div class="mb-3">
                        <p class="mb-2">Sponsor Member</p>
                        <input class="form-control mb-1" type="text" name="" id="" value="{{ Session::get('user.data')->sponsor_name }}" disabled>
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Upline Member</p>
                        <input class="form-control mb-1" type="text" name="" id="" value="{{ Session::get('user.data')->upline_name }}"  disabled>
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">E-PIN</p>
                        <input class="form-control mb-1" type="text" name="" id="" value="{{ Session::get('user.data')->e_pin }}" disabled>
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-4">
                    <div class="mb-3">
                        <p class="mb-2">No. Hp</p>
                        <input class="form-control mb-1" type="text" name="phone_number" id="" value="{{ Session::get('user.data')->phone_number }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Nama Ahli Waris</p>
                        <input class="form-control mb-1" type="text" name="heir_name" id="" value="{{ Session::get('user.data')->heir_name }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Hub. Ahli Waris</p>
                        <input class="form-control mb-1" type="text" name="heir_relationship" id="" value="{{ Session::get('user.data')->heir_relationship }}">
                    </div>
                </div>
    
                <div class="col-12 col-md-6 mb-4">
                    <p class="font-semiBold text-primary">Personal Information</p>
                </div>
    
                <div class="col-12 col-md-6 mb-4">
                    <p class="font-semiBold text-primary">Bank Information</p>
                </div>
    
                <div class="col-12 col-md-6 mb-3">
                    <div class="mb-3">
                        <p class="mb-2">Email Member</p>
                        <input class="form-control mb-1" type="text" name="email" id="" value="{{ Session::get('user.data')->email }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Jenis Kelamin</p>
                        <select class="form-control custom-select" name="gender_id" id="">
                            <option value="1" @if(Session::get('user.data')->gender_id == 1) selected @endif>Pria</option>
                            <option value="2" @if(Session::get('user.data')->gender_id == 2) selected @endif>Wanita</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Provinsi Member</p>
                        <input class="form-control mb-1" type="text" name="province_name" id="" value="{{ Session::get('user.data')->province_name }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Kota Member</p>
                        <input class="form-control mb-1" type="text" name="city_name" id="" value="{{ Session::get('user.data')->city_name }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Kode Pos Member</p>
                        <input class="form-control mb-1" type="text" name="postal_code" id="" value="{{ Session::get('user.data')->postal_code }}">
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-3">
                    <div class="mb-3">
                        <p class="mb-2">No. Rekening</p>
                        <input class="form-control mb-1" type="text" name="account_number" id="" value="{{ Session::get('user.data')->account_number }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Nama Bank Member</p>
                        <input class="form-control mb-1" type="text" name="account_holder_name" id="" value="{{ Session::get('user.data')->account_holder_name }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">Nama Bank <span class="text-muted">(BCA, BRI, dll)</span></p>
                        <input class="form-control mb-1" type="text" name="bank_code" id="" value="{{ Session::get('user.data')->bank_code }}">
                    </div>
                    <div class="mb-3">
                        <p class="mb-2">NPWP</p>
                        <input class="form-control mb-1" type="text" name="npwp_number" id="" value="{{ Session::get('user.data')->npwp }}">
                    </div>
                </div>
                <div class="col-12 mb-4">
                    <div class="text-right">
                        <input class="form-control btn d-inline-block px-5" style="width:unset;" type="submit" value="Save">
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('dashboard-javascript')
    <script>
        $('select[name="province_id"]').change(function () { 
            $("select[name='city_id']").empty();
            $.ajax({
                type: "POST",
                url: "{{ url('get_cities') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "province_id":$(this).children("option:selected").val()
                },
                dataType: "json",
                success: function (response) {
                    $.each(response, function (index, val) { 
                        $("select[name='city_id']").append(new Option(val.city_name, val.id));
                    });
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        });
    </script>
@endsection

