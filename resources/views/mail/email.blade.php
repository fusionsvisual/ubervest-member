<!DOCTYPE html>
<html>
  <head>
    <title>Please complete your payment</title>
    <style>
        .container{
            text-align: center;
            background:white;
            max-width:30%;
            margin:auto;
            padding: 3rem;
        }
        h1, p{
            color:black;
        }
        @media only screen and (max-width: 768px) {
            .container{
                max-width:100%;
            }
        }
    </style>
  </head>
  <body style="background: #F0F2F5; height: 100vh;">
    <div style="display: flex; height:100vh;">
        <div class="container">
            <img src="https://ubervest.fusionsvisual.id/images/img_logo_png.png" alt="" width="35%">
            <h1>Please complete your payment</h1>
            <p style="margin-bottom: 1.5rem;">Your transaction on {{ date_format(date_create(DateFormatter::formatISO8601($data->purchase_room->created_at)), "d F Y") }}, {{ date_format(date_create(DateFormatter::formatISO8601($data->purchase_room->created_at)), "H:i") }} was successfully created. Please pay to complete the transaction.</p>
            <a href="{{url('payment/'.$data->token)}}" style="background: #0062cc; color: white; display:block; padding: 1.25rem 0px 1.25rem 0px; border-radius: 6px; margin-bottom:1.5rem; text-decoration: none; font-weight: 800">
                Pay Now
            </a>
            <p style="margin-bottom:0.5rem;">Or paste this link into your browser</p>
            <a href="{{url('payment/'.$data->token)}}">{{url('payment/'.$data->token)}}</a>
        </div>   
    </div>       
  </body>
</html>