@extends('layouts/dashboard_master')

@section('title')
NETWORK
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px) {
        .page-content {
            padding-top: 100px !important;
        }

        .right-sidebar-toggler {
            display: block !important;
        }
    }
</style>
@endsection

@section('outside-content')
<div class="upgrade-popup d-none m-auto" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999">
    <div class="upgrade-popup-overlay" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:998; background: rgba(0,0,0,0.75)">
    </div>
    <div class="m-auto px-3 px-md-0" style="width: 500px; z-index:999;">
        <div class="bg-white p-5" style="border-radius: 10px;">
            <form method="POST">
                @csrf
                <div class="upgrade-form mb-4">
                    <p class="mb-2">Username</p>
                    <input class="form-control" type="text" name="username" id="" value="" disabled>
                    <input type="hidden" name="user_id">
                </div>
                <div class="upgrade-form mb-4">
                    <p class="mb-2">E-PIN</p>
                    <select class="custom-select" name="e_pin" id="">
                        @foreach($ePin_codes->data as $ePin)
                        <option value="{{ $ePin->e_pin }}">{{ $ePin->e_pin }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <button class="btn btn-save btn-primary px-3 w-100">Upgrade</button>
                </div>
            </form>
        </div>
        {{-- <div class="d-flex" style="position: relative; padding-top:55%;"> --}}
        {{-- <img class="img-image-popup w-100 h-100" style="position:absolute; top:0; left:0; right:0; bottom: 0; border-radius:10px; object-fit:cover;"> --}}
        {{-- </div> --}}
    </div>
</div>
@endsection

@section('content')
{{-- <form action="{{ url('network/personal_sponsor') }}" method="GET"> --}}
<div class="row">

    @if(Session::has('response'))  
    <div class="col-12"> 
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    </div>
    @endif

    <div class="col-12 col-md-9 mb-3">
        <input class="search-input form-control" type="text" placeholder="Search">
    </div>

    <div class="col-12 col-md-3 mb-4">
        <select class="level-select form-control custom-select" name="" id="">
            <option value="">All Level</option>
            @foreach ($total_level as $level)
            <option value="{{ $level }}">Level {{ $level }}</option>
            @endforeach
        </select>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-2 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Username</th>
                        <th scope="col">Upline</th>
                        <th scope="col">Package</th>
                        <th scope="col">Level (Username)</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($members->data as $idx => $member)
                    <tr>
                        <td scope="row">{{ $idx+1 }}</td>
                        <td>{{ $member->username }}</td>
                        <td>{{ $member->upline_user->username }}</td>
                        <td>{{ $member->package->package_code }}</td>
                        <td>Level {{ $member->level }}</td>
                        <td>
                            <div>
                                <button class="btn btn-upgrade btn-edit px-4" data-id="{{ $member->id }}" data-username="{{ $member->username }}" @if($member->package->package_code == "PP") disabled style="background:rgba(0,0,0,0.25); color:white;" @endif>
                                    Upgrade
                                </button>   
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination-container">
            @include('templates.pagination', ["items" => $members, "page" => $page])
        </div>
    </div>
</div>
{{-- </form> --}}
@endsection

@section('dashboard-javascript')
<script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
<script>
    $url = "{{ url('network/personal_sponsor/filter') }}";
        $token = "{{ csrf_token() }}";
        $totalCount = "{{ $members->total/$members->per_page }}"
        $level_query = "";
        
        $('.search-input').keyup(function (e) { 
            if ($(this).val() != ""){
              $isEmpty = false;
            }

            if (!$isEmpty){
                $search_query = $(this).val();
                $page = 1;
                filter();
            }

            if ($(this).val() == ""){
                $isEmpty = true;
            }
        });

        $('.level-select').change(function (e) { 
            $level_query = $(this).val()
            $page = 1;
            filter()
        });

        function setupViews(){
            $('.btn-upgrade').click(function (e) { 
                $('.upgrade-popup').addClass('d-flex');
                $('.upgrade-popup').removeClass('d-none');

                $('input[name="username"]').val($(this).data('username'));
                $('input[name="user_id"]').val($(this).data('id'));
            });
        }

        function didFilterDone(){
            setupViews()
        }

        $('.upgrade-popup-overlay').click(function (e) { 
            $('.upgrade-popup').removeClass('d-flex');
            $('.upgrade-popup').addClass('d-none');
        });

</script>
@endsection