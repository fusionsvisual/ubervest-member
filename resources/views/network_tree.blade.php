@extends('layouts/dashboard_master')

@section('title')
NETWORK
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px) {
        .page-content {
            padding-top: 100px !important;
        }

        .right-sidebar-toggler {
            display: block !important;
        }
    }

    .branch {
        min-width: 172px;
        min-height: 195px;
        max-width: 172px;
    }

    .package {
        border-bottom: 1px solid rgba(0, 0, 0, 0.25)
    }

</style>
@endsection

@section('content')
@if(Session::has('response'))
    @if(is_array(Session::get('response')->message))
        @foreach(Session::get('response')->message as $message)
            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ $message }}</p>
        @endforeach
    @else
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    @endif
@endif
<div class="network-tree mx-1 p-4 p-md-5 mb-3" style="background:white; border-radius:20px; ">
    <div class="row">
        <div class="col-12 col-md-4 col-xl-6 mb-3 mb-md-0">
            <p class="text-medium">Total Investasi</p>
            <p class="text-extraLarge font-semiBold font-secondary">${{ number_format($user->deposit, 0, ',', '.') }}</p>
        </div>
        <div class="left-leg col-12 col-md-4 col-xl-3 d-flex mb-3 mb-md-0">
            <div class="mx-md-auto">
                <p class="text-medium">Left BV</p>
                <p class="text-extraLarge font-semiBold">Rp.{{ number_format($user->temporary_left_bv*1275000, 0, ',', '.') }}</p>
                <p class="text-medium text-muted">(Rp.{{ number_format($user->left_bv*1275000, 0, ',', '.') }})</p>
            </div>
        </div>
        <div class="right-leg col-12 col-md-4 col-xl-3 d-flex mb-3 mb-md-0">
            <div class="mx-md-auto">
                <p class="text-medium">Right BV</p>
                <p class="text-extraLarge font-semiBold">Rp.{{ number_format($user->temporary_right_bv*1275000, 0, ',', '.') }}</p>
                <p class="text-medium text-muted">(Rp.{{ number_format($user->right_bv*1275000, 0, ',', '.') }})</p>

            </div>
        </div>
    </div>
</div>

<div class="d-flex">
    <button class="btn btn-gotop mr-2 btn-primary">Go to top</button>
    <button class="btn btn-back btn-primary">Back</button>
</div>

<div class="loading-container d-flex">
    <img class="img-loading mx-auto my-5 d-none" src="{{ asset('images/loading.gif') }}" alt="" width="100px" height="100px">
</div>

<div class="tree-container">
    @include('items.network_tree_item', ['network_tree' => $network_tree])
</div>

@endsection

@section('outside-content')
<div class="registration-form-container d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:3;">
    <div style="position: fixed; top:0; left:0; right:0; bottom:0; background:rgba(0,0,0,0.5);"></div>
    <div class="registration-form m-auto px-md-5 px-4 py-4" style="background: white; z-index:4; min-width:50%; max-width:90%; max-height:95%; overflow:scroll">
        <p class="font-bold text-extraLarge mb-3">REGISTRASI</p>
        <p class="message text-danger mb-3 d-none">**message</p>
        <form action="{{ url('register') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="mb-4">
                        <p class="mb-3">Network Information</p>
                        <div class="mb-3">
                            <p class="label mb-2">Sponsor</p>
                            <input class="form-control" type="text" value="{{ Session::get('user.data')->username }}" id="" disabled>
                            <input class="form-control" type="hidden" name="sponsor_id" id="{{ Session::get('user.data')->id }}" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">Upline</p>
                            <input class="form-control" type="text" value="" name="upline_name" id="" disabled>
                            <input class="form-control" type="hidden" name="upline_id" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">E-PIN</p>
                            <select class="custom-select" name="e_pin" id="">
                                @foreach($ePin_codes->data as $ePin)
                                    <option value="{{ $ePin->e_pin }}">{{ $ePin->e_pin }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div>
                        <p class="mb-3">Personal Information</p>
                        <div class="mb-3">
                            <p class="label mb-2">Username</p>
                            <input class="form-control" type="text" name="username" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">Email</p>
                            <input class="form-control" type="email" name="email" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">No. Handphone</p>
                            <input class="form-control" type="text" name="phone_number" id="" required>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="mb-4">
                        <p class="mb-3">Proof of Identity</p>
                        <div class="mb-3">
                            <p class="label mb-2">Provinsi</p>
                            <input class="form-control" type="text" name="province" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">Kota</p>
                            <input class="form-control" type="text" name="city" id="" required>
                        </div>
                        {{-- <div class="mb-3">
                                <p class="label mb-2">Tanggal Lahir</p>
                                <input class="form-control" type="date" name="birthdate" id="" required>
                            </div> --}}

                        <div class="mb-3">
                            <p class="label mb-2">Gender</p>
                            <select class="custom-select" name="gender_id" id="" required>
                                <option value="1" selected>Men</option>
                                <option value="2">Women</option>
                            </select>
                        </div>
                    </div>

                    <div>
                        <p class="mb-3">Account Information</p>
                        <div class="mb-3">
                            <p class="label mb-2">Nama Bank</p>
                            <input class="form-control" type="text" name="bank_code" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">Nama Pemilik Rek.</p>
                            <input class="form-control" type="text" name="account_holder_name" id="" required>
                        </div>
                        <div class="mb-3">
                            <p class="label mb-2">Nomor Rek.</p>
                            <input class="form-control" type="text" name="account_number" id="" required>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="btn-container d-flex">
                        <input type="hidden" name="leg" value="">
                        <button class="btn btn-cancel btn-stroke mr-2 px-3 ml-auto">Cancel</button>
                        <button class="btn btn-save btn-primary px-3">Save</button>
                    </div>
                    <div class="loading-container d-none">
                        <p class="ml-auto font-bold">Creating member...</p>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection

@section('dashboard-javascript')
<script>
    $selected_leg_id = "{{ $network_tree[0]->id }}";
    $last_user_id = ["{{ Session::get('user.data')->id }}"];
    $user_id = "{{ Session::get('user.data')->id }}";

    $(document).ready(function () {
        updateButton()
    });

    function updateTree($method) {
        $('.img-loading').removeClass('d-none');
        $('.tree-container').empty();

        console.log($selected_leg_id);

        $.ajax({
            type: "POST",
            url: "{{ url('/network/network_tree/filter') }}",
            data: {
                "_token": "{{ csrf_token() }}",
                "method": $method,
                "user_id": $selected_leg_id
            },
            dataType: "json",
            success: function (response) {
                $selected_leg_id = response.current_branch.id;

                $('.img-loading').addClass('d-none');
                $('.tree-container').html(response.items);
                updateButton()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.responseText);
                console.log(thrownError);
            }
        });
    }

    function updateButton() {
        $('.btn-register-tree').click(function (e) {
            $('input[name="upline_id"]').val($(this).data('upline-id'));
            $('input[name="upline_name"]').val($(this).data('upline-name'));

            $('input[name="leg"]').val($(this).data('leg'));

            $('.registration-form-container').addClass('d-flex');
            $('.registration-form-container').removeClass('d-none');
        });

        $('.btn-network-tree').click(function (e) {
            $user_id = $(this).data("id");
            $selected_leg_id = $user_id;

            updateTree("NEXT");
        });
    }

    $('.btn-gotop').click(function (e) {
        $selected_leg_id = "{{ Session::get('user.data')->id }}";
        updateTree("GO_TO_TOP");
    });

    $('.btn-back').click(function (e) {
        if ($selected_leg_id != "{{ Session::get('user.data')->id }}") {
            updateTree("BACK");
        }
    });

    $('.btn-cancel').click(function (e) {
        e.preventDefault();
        $('.registration-form-container').removeClass('d-flex');
        $('.registration-form-container').addClass('d-none');

        $('.message').addClass('d-none');
    });

    $('.btn-save').click(function (e) {
        // $('.loading-container').addClass('d-flex');
        // $('.loading-container').removeClass('d-none');

        // $('.btn-container').removeClass('d-flex');
        // $('.btn-container').addClass('d-none');

        // $('.message').addClass('d-none');

        // registration()
    });

    // function registration() {
    //     $.ajax({
    //         type: "POST",
    //         url: "{{ url('register') }}",
    //         data: {
    //             "_token": "{{ csrf_token() }}",
    //             "leg": $selectedLeg,
    //             "upline_id": $('input[name="upline_id"]').val(),
    //             "username": $('input[name="username"]').val(),
    //             "email": $('input[name="email"]').val(),
    //             "e_pin": $('select[name="e_pin"]').val(),
    //             "phone_number": $('input[name="phone_number"]').val(),
    //             "gender_id": $('select[name="gender_id"]').val(),
    //             "province": $('input[name="province"]').val(),
    //             "city": $('input[name="city"]').val(),
    //             "bank_code": $('input[name="bank_code"]').val(),
    //             "account_holder_name": $('input[name="account_holder_name"]').val(),
    //             "account_number": $('input[name="account_number"]').val(),
    //         },
    //         dataType: "json",
    //         success: function (response) {
    //             if (response.success) {
    //                 window.location.replace("{{ url('network/network_tree') }}");
    //             } else {
    //                 $('.loading-container').removeClass('d-flex');
    //                 $('.loading-container').addClass('d-none');

    //                 $('.btn-container').addClass('d-flex');
    //                 $('.btn-container').removeClass('d-none');

    //                 $('.message').removeClass('d-none');
    //                 $('.message').text(response.message);
    //             }
    //         },
    //         error: function (xhr, ajaxOptions, thrownError) {
    //             console.log(xhr);
    //             console.log(thrownError);
    //         }
    //     });
    // }

</script>
@endsection
