@extends('layouts/dashboard_master')

@section('title')
DETAIL HISTORY TRANSAKSI
@endsection

@section('content')
{{-- {{ json_encode($history) }} --}}
{{-- PAYMENT POPUP --}}
<div class="overlay-container @if(!Session::has('is_new')) d-none @endif" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999;">
    <div class="d-flex" style="position: absolute; top:0; left:0; right:0; bottom:0;">
        <div class="overlay w-100 h-100" style="background: rgba(0,0,0,0.5); position: absolute; top:0; left:0; right:0; bottom:0; z-index: 998"></div>
        <div class="m-auto p-4" style="background: white; max-width:90%; width:425px; border-radius: 10px; z-index: 999">
            <form action="{{ url()->current()."/update" }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <p class="text-large font-semiBold">Transfer to:</p>
                <p>BCA | PT. Untung Bersama Investindo</p>
                <p>8630582378</p>
            </div>
            <div class="payment-receipt-container mb-3" style="padding-top: 66.66%; position: relative; overflow:hidden">
                <img id="payment-receipt" class="w-100 h-100" src="{{ asset("images/img_upload_image.png") }}" style="position: absolute; top:0; left:0; right:0; bottom:0; object-fit: cover; border-radius: 10px;">
                <div class="payment-receipt-overlay d-flex" style="background-color: rgba(0,0,0,0.5); position: absolute; top:0; left:0; right:0; bottom:0; border-radius: 10px; opacity:0;">
                    <div class="bg-primary d-inline-block m-auto" style="border-radius: 25px;">
                        <p class="font-white font-semiBold p-3">CHANGE IMAGE</p>
                    </div>
                </div>
                <input type="file" name="payment_receipt" accept="image/png, image/jpeg" style="position: absolute; top:0; left:0; right:0; bottom:0; opacity:0; cursor: pointer;" required>
            </div>
            <div>
                <div class="mb-3">
                    <p class="label mb-2">Bank Name</p>
                    <input class="form-control" type="text" name="bank_name" id="" style="height: 35px !important; box-shadow: none !important; border: 1px solid #999999 !important;" placeholder="e.g., BCA" required>
                </div>
                <div class="mb-3">
                    <p class="label mb-2">Account Name</p>
                    <input class="form-control" type="text" name="bank_account_name" id="" style="height: 35px !important; box-shadow: none !important; border: 1px solid #999999 !important;" placeholder="e.g., John Doe" required>
                </div>
                <div class="mb-4">
                    <p class="label mb-2">Account Number</p>
                    <input class="form-control" type="text" name="bank_account_number" id="" style="height: 35px !important; box-shadow: none !important; border: 1px solid #999999 !important;" placeholder="e.g., 123456789" required>
                </div>
                <button class="btn btn-primary w-100">DONE</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="py-4 px-5 mb-5" style="background: white">
    <div class="row mb-5">
        @if(Session::has('response'))   
            <div class="col-12">
                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
            </div>
        @endif 

        <div class="col-12 col-md-8">
            <h1 class="font-semiBold" style="font-size: 72px;">Invoice</h1>
            <h3 class="font-regular text-extraLarge">{{ $history->invoice_code }}</h3>
        </div>
        <div class="col-12 col-md-4 d-none d-md-block">
            <div class="d-flex w-100 h-100">
                <div class="ml-auto my-auto">
                    <p class="text-md-right text-muted">PT. Untung Bersama Investindo</p>
                    <p class="text-md-right text-muted">Jl. Mayjend Yono Soewoyo Graha Family, Surabaya 60226 (Spazio Building)</p>
                    <p class="text-md-right text-muted">Indonesia</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-md-5 mb-4 mb-md-0">
            <h5 class="font-regular text-muted">Bill to:</h5>
            <h5 class="font-semiBold">{{ $history->email }}</h5>
            <h5 class="font-semiBold">{{ $history->phone_number }}</h5>
        </div>
        <div class="col-12 col-md-4 mb-4 mb-md-0">
            <h5 class="font-regular text-muted">Invoice Date:</h5>
            <h5 class="font-semiBold">{{ date_format(date_create($history->created_at), "d F Y") }}</h5>
        </div>
        <div class="col-12 col-md-3">
            <h5 class="font-regular text-muted">Status:</h5>
            <div class="d-inline-block mb-1" style="border-radius: 10px; @switch($history->status->status_name)
                @case(OrderStatus::PAID)
                background:rgba(32,186,56,1)
                @break
                @case(OrderStatus::UNPAID)
                background:rgba(254,97,0,1)
                @break
                @case(OrderStatus::WAITING)
                background:rgba(255,209,90,1)
                @break
                @default
                background:rgba(236,75,75,1)
                @break
             @endswitch">
                <h5 class="font-semiBold font-white mx-3 my-1">{{ $history->status->status_name }}</h5>
            </div>
            @if ($history->reject_reason != null)
                <p class="font-red">*{{ $history->reject_reason }}</p>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive mb-4 mb-md-4">
                <table class="table outline-table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Package</th>
                            <th scope="col">E-Pin</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody class="right-legs">
                        @foreach ($history->purchase_packages as $idx => $purchase_package)
                        <tr>
                            <td style="@if(count($history->purchase_packages) == 1) border-radius: 8px 0px 0px 8px !important; @endif">{{ $idx + 1 }}</td>
                            <td>{{ $purchase_package->package->package_code }}</td>
                            <td>{{ $purchase_package->e_pin }}</td>
                            <td style="@if(count($history->purchase_packages) == 1) border-radius: 0px 8px 8px 0px !important; @endif">Rp{{number_format($purchase_package->package->package_price, 0, ',', '.')}}</td>
                        </tr>
                        {{-- <tr>
                            <td>{{ $idx + 1 }}</td>
                            <td>{{ $purchase_package->package->package_code }}</td>
                            <td>{{ $purchase_package->e_pin }}</td>
                            <td>Rp{{number_format($purchase_package->package->package_price, 0, ',', '.')}}</td>
                        </tr> --}}
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-9 mb-5 mb-md-0">
            <div class="pr-4">
                <h5 class="font-regular text-muted">Keterangan:</h5>            
                <p class="">@if ($history->description != ""){{ $history->description }}@else No description @endif</p>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="d-flex mb-3">
                <h5 class="font-regular text-muted">Total:</h5>
                <h5 class="font-regular font-semiBold ml-auto">
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($history->purchase_packages as $purchase_package)
                        @php
                            $total = $total + $purchase_package->package->package_price
                        @endphp
                    @endforeach
                    Rp{{number_format($total, 0, ',', '.')}}
                </h5>
            </div>
            @if ($history->status->status_name == OrderStatus::UNPAID)
                <button class="btn btn-pay btn-primary w-100">PAY</button>
            @endif
        </div>
    </div>
</div>
@endsection

@section('dashboard-javascript')
    <script>
        $paymentReceiptImage = null

        $('.btn-pay').click(function (e) { 
            $('.overlay-container').removeClass('d-none');
        });

        $('input[name="payment_receipt"]').change(function (e) {
            var oFReader = new FileReader();
            oFReader.readAsDataURL($(this)[0].files[0]);
            $paymentReceiptImage = $(this)[0].files[0]

            oFReader.onload = function (oFREvent) {
                $('#payment-receipt').attr('src', oFREvent.target.result);
            };
        });

        $('.overlay').click(function (e) { 
            $('.overlay-container').addClass('d-none');
        });

        $('.payment-receipt-container').hover(function () {
                if ($paymentReceiptImage != null){
                    $('.payment-receipt-overlay').css('opacity', 1);
                }
            }, function () {
                $('.payment-receipt-overlay').css('opacity', 0);
            }
        );


    </script>
@endsection