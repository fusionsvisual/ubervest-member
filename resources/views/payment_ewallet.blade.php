@extends('layouts/master')

@section('css')

<style>
    @media (min-width: 1200px){
        .container {
            max-width: 900px !important;
        }
    }

    input{
        box-shadow: none !important;
        padding-left: 16px;
        padding-right: 16px;
        border:1px solid rgba(0,0,0,0.15) !important;
    }

    img.img-bank{
        position: absolute; 
        top:0;
        object-fit: contain;
    }

    button{
        background: none;
    }

    .line{
        height: 2px;
    }
    
</style>
    
@endsection

@section('master-content')
<form action="" method="POST">
    @csrf
    <div class="">
        <h4 class="text-center py-4 bg-primary font-semiBold text-white mb-0">UBERVEST CHECKOUT</h4>
    </div>

    {{-- <div class="py-3" style="color: #e84855; background-color: #fdedee">
        <p class="text-center font-bold">Pay before JANUARY 01, 2021 00:00pm</p>
    </div> --}}

    
    
    <div class="container pb-5">
        <div class="mt-md-5 mt-4 mb-4">
            @if(Session::has('response'))   
                <div>
                    <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                </div>
            @endif 

            <div class="row mb-3">
                <div class="col-12 col-md-9 order-2 order-md-1">
                    <div class="d-flex h-100">
                        <div class="my-auto">
                            <p class="font-semiBold mb-1">TOTAL AMOUNT:</p>
                            <h1 class="font-regular text-primary">Rp{{ number_format($purchase_room->total + $purchase_room->total * (3/197), 0, ',', '.') }}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 order-1 order-md-2 mb-4 mb-md-0">
                    <div class="px-5 px-md-0">
                        <div class="" style="position: relative; padding-top:50%">
                            <div class="d-flex w-100 h-100" style="position: absolute; top:0px; left:0px; right:0px; right:0px;">
                                @if (Request::segment(3) == "ovo")
                                    <img class="w-100 h-100" src="{{ asset('images/bank_icon/ic_ovo.png') }}" alt="" style="position: absolute; top:0px; left:0px; right:0px; right:0px;">
                                @else
                                    <img class="my-auto w-100" src="{{ asset('images/bank_icon/ic_dana.png') }}" alt="" style="">
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                
                    <p class="font-medium mb-2">Mobile Number</p>
                    <input class="w-100" type="text" name="phone_number" id="" placeholder="1234 5678 9101" required>
            </div>
        </div>

        <div class="d-flex flex-column flex-md-row">
            <a class="btn btn-primary-outline px-5 py-2 ml-md-auto mr-md-3 mb-3 mb-md-0" href="{{ url('payment/'.Request::segment(2)) }}">
                Back
            </a>
            <button type="submit" class="btn btn-primary d-block px-5 py-2 mr-md-auto">Pay Now</button>
        </div>
    </div>
</form>

@endsection

@section('javascript')
@endsection