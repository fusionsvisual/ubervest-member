@extends('admin.layouts.admin_dashboard_master')

@section('title')
WITHDRAWAL REQUEST
@endsection

@section('dashboard-css')

<style>
    @media only screen and (max-width: 768px) {
        .page-content {
            padding-top: 150px !important;
        }
    }
</style>

@endsection

@section('content')
<div class="row d-flex">

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Date</p>
            <input class="form-control" type="text" name="" value="{{ date_format(date_create($withdraw->created_at), "d F Y") }}" disabled>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Status</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->status->status_name }}" disabled>
        </div>
    </div>

    <div class="col-12">
        <div class="mb-4">
            <p class="font-semiBold mb-2">Username</p>
            <input class="form-control" type="text" name="" value="{{ $withdraw->user->username }}" disabled>
        </div>
    </div>

    {{-- <div class="col-12">
            <div class="mb-4">
                <p class="font-semiBold mb-2">Amount</p>
                <input class="form-control" type="text" name="" value="Rp{{number_format($withdraw->amount, 0, ',', '.')}}" disabled>
</div>
</div> --}}

<div class="col-12">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Description</p>
        <textarea class="form-control" name="" id="" cols="1" rows="5" disabled>{{ $withdraw->description }}</textarea>
        {{-- <input class="form-control" type="text" name="" value="{{ $withdraw->description }}" disabled> --}}
    </div>
</div>

<div class="col-12">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Bank Code</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->bank_code }}" disabled>
    </div>
</div>

<div class="col-12 col-md-6">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Account Name Holder</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_holder_name }}" disabled>
    </div>
</div>

<div class="col-12 col-md-6">
    <div class="mb-4">
        <p class="font-semiBold mb-2">Account Number</p>
        <input class="form-control" type="text" name="" value="{{ $withdraw->user->account_number }}" disabled>
    </div>
</div>

<div class="col-12">
    <div class="table-responsive mb-4 mb-md-4">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Package</th>
                    <th scope="col">E-Pin</th>
                    <th scope="col">Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Seasonal Withdraw</td>
                    <td></td>
                    <td>${{number_format($withdraw->seasonal_withdraw_amount, 0, ',', '.')}}</td>
                </tr>

                <tr>
                    <td>2</td>
                    <td>Fast Track Withdraw</td>
                    <td></td>
                    <td>Rp{{number_format($withdraw->fast_track_withdraw_amount, 0, ',', '.')}}</td>
                </tr>

                <tr>
                    <td>3</td>
                    <td>Link Up Withdraw</td>
                    <td></td>
                    <td>Rp{{number_format($withdraw->link_up_withdraw_amount, 0, ',', '.')}}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="col-12">
    <div class="d-flex mb-5">
        <p class="ml-auto text-large font-bold">Total: Rp{{number_format(($withdraw->seasonal_withdraw_amount + $withdraw->fast_track_withdraw_amount), 0, ',', '.')}} @if ($withdraw->link_up_withdraw_amount > 0) + {{ $withdraw->link_up_withdraw_amount }}@endif</p>
    </div>
</div>

@if($withdraw->status->status_name != "SEND" && $withdraw->status->status_name != "REJECT" )
<div class="col-12">
    <div class="d-flex mb-5 flex-column flex-md-row">
        <a class="btn btn-danger mr-md-2 ml-md-auto mb-3 mb-md-0 py-3 px-5" href="{{ url('admin/withdrawal_request/left_pocket/reject/'.$withdraw->id) }}">Reject</a>
        <a class="btn btn-primary py-3 px-5" href="{{ url('admin/withdrawal_request/left_pocket/send/'.$withdraw->id) }}">Send</a>
    </div>
</div>
@endif


</div>

@endsection