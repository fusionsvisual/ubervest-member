@extends('admin.layouts.admin_dashboard_master')

@section('title')
WITHDRAWAL REQUEST
@endsection

@section('content')
<form action="{{ url('admin/member_deposit/create') }}" method="POST">
    @csrf
    <div class="row d-flex">
        <div class="col-12">
            <div class="mb-4">
                <p class="font-semiBold mb-2">Username</p>
                <input class="form-control" type="text" name="" id="">
            </div>
    
            <div class="mb-4">
                <p class="font-semiBold mb-2">Meta ID</p>
                <input class="form-control" type="text" name="" id="">
            </div>
    
            <div class="mb-4">
                <p class="font-semiBold mb-2">Amount</p>
                <input class="form-control" type="text" name="" id="">
            </div>
        </div>
        <div class="ml-auto col-12 col-md-2">
            <input class="btn btn-primary w-100" type="submit" value="Save">
        </div>
    </div>
</form>

@endsection