@extends('admin.layouts.admin_dashboard_master')

@section('title')
VERIFICATION REQUEST
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px){
        .verification-image{
            width:300px;
        }
    }
</style>
    
@endsection

@section('outside-content')
<div class="image-popup d-none m-auto" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999">
    <div class="image-popup-overlay" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:998; background: rgba(0,0,0,0.75)">
    </div>
    <div class="m-auto px-3 px-md-0" style="width: 500px; z-index:999;">
        <div class="d-flex" style="position: relative; padding-top:55%;">
            <img class="img-image-popup w-100 h-100" style="position:absolute; top:0; left:0; right:0; bottom: 0; border-radius:10px; object-fit:cover;">
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    
    @if(Session::has('response'))   
    <div class="col-12">
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    </div>
    @endif 
    
    <div class="col-12 mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search">
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Date</th>
                        <th scope="col">Username</th>
                        <th scope="col">Identity Card</th>
                        <th scope="col">Proof of Address</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($requests->data as $idx => $request)
                    <tr>
                        <td scope="row" style="vertical-align:top!important;">{{ $idx + 1 }}
                        <td scope="row" style="vertical-align:top!important;">{{ date_format(date_create($request->created_at), "d F Y") }}</td>
                        <td style="vertical-align:top!important;">{{ $request->user->username }}</td>
                        <td>
                            <div class="verification-image" style="position: relative; padding-top:55%; width:250px">
                                <img class="w-100 h-100" src="{{ Network::ASSET_URL.$request->identity_card }}" alt=""
                                    style="position: absolute; top:0; left:0; background:grey; border-radius:10px; object-fit:cover; cursor: pointer;">
                            </div>
                        </td>
                        <td>
                            <div class="verification-image" style="position: relative; padding-top:55%; width:250px">
                                <img class="w-100 h-100" src="{{ Network::ASSET_URL.$request->proof_of_address }}" alt=""
                                    style="position: absolute; top:0; left:0; background:grey; border-radius:10px; object-fit:cover; cursor: pointer;">
                            </div>
                        </td>
                        <td style="vertical-align:top!important;">{{ $request->status->status_name }}</td>

                        
                            <td class=""  style="vertical-align:top!important;">
                                @if($request->status->status_name != "RELEASE" && $request->status->status_name != "REJECT")
                                <div class="d-flex">
                                    <form method="POST">
                                        @csrf
                                        <button class="btn d-inline-block btn-release px-4 mr-2">
                                            Release
                                        </button>
                                        <input type="hidden" name="identity_id" value="{{ $request->id }}">
                                        <input type="hidden" name="action" value="release">
                                    </form>
                                    <form method="POST">
                                        @csrf
                                        <button class="btn d-inline-block btn-baning px-4">
                                            Reject
                                        </button>
                                        <input type="hidden" name="identity_id" value="{{ $request->id }}">
                                        <input type="hidden" name="action" value="reject">
                                    </form>
                                </div>
                                @endif
                            </td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            @include('templates.pagination', ["items" => $requests, "page" => $page])
        </div>
    </div>


</div>
@endsection

@section('dashboard-javascript')
<script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
<script>
    $url = "{{ url('admin/verification_request/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $requests->total/$requests->per_page }}"

    setupViews()

    $('input[name="search"]').keyup(function (e) { 
        if ($(this).val() != ""){
          $isEmpty = false;
        }

        if (!$isEmpty){
            $search_query = $(this).val();
            $page = 1;
            filter();
        }

        if ($(this).val() == ""){
            $isEmpty = true;
        }
    });

    $('.image-popup-overlay').click(function (e) { 
        $('.image-popup').removeClass('d-flex');
        $('.image-popup').addClass('d-none');        
    });

    function setupViews(){
        $('.verification-image img').click(function (e) { 
            $('.image-popup').addClass('d-flex');
            $('.image-popup').removeClass('d-none'); 
            
            $('.img-image-popup').attr('src', $(this).attr('src'));          
        });
    }

    // $('.verification-image img').click(function (e) { 
    //     $('.image-popup').addClass('d-flex');
    //     $('.image-popup').removeClass('d-none'); 
        
    //     $('.img-image-popup').attr('src', $(this).attr('src'));          
    // });

    function didFilterDone(){
        setupViews()
    }
</script>
@endsection