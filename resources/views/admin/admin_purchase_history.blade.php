@extends('admin.layouts.admin_dashboard_master')

@section('title')
PURCHASE PACKAGE
@endsection

@section('content')
<div class="row">
  <div class="col-12 col-md-8 mb-3">
    <input class="form-control" type="text" name="search" placeholder="Search invoice">
  </div>

  <div class="col-12 col-md-4 mb-4">
    <div class="row d-flex flex-wrap">
      <div class="col-5">
        <input class="form-control" type="text" name="start_date">
      </div>
      <div class="col-2 d-flex px-0">
        <p class="m-auto text-muted">sampai</p>
      </div>
      <div class="col-5">
        <input class="form-control" type="text" name="end_date">
      </div>
    </div>
  </div>

  <div class="col-12">
    <div class="table-responsive mb-4 mb-md-4">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Tgl. Request</th>
            <th scope="col">Invoice ID</th>
            <th scope="col">Total Payment</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody class="item-list">
          @foreach ($histories->data as $idx => $history)
          <tr>
            <td scope="row">{{ $idx+1 }}</td>
            <td>{{ date_format(date_create($history->created_at), "d F Y") }}</td>
            <td>{{ $history->invoice_code }}</td>
            <td>Rp{{number_format($history->total, 0, ',', '.')}}</td>
            <td class="font-semiBold" style="@switch($history->status->status_name)
              @case(OrderStatus::PAID)
              color:rgba(32,186,56,1)
              @break
              @case(OrderStatus::UNPAID)
              color:rgba(254,97,0,1)
              @break
              @case(OrderStatus::WAITING)
              color:rgba(255,209,90,1)
              @break
              @default
              color:rgba(255,0,0,1)
              @break
           @endswitch">{{ $history->status->status_name }}</td>
            <td>
              <div class="d-flex">
                <a class="btn btn-primary mr-2" href="{{ url('admin/purchase_history/'.$history->id) }}">
                  <p>Detail</p>
                </a>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <div class="pagination-container">
      @include('templates.pagination', ["items" => $histories, "page" => $page])
    </div>


</div>
@endsection

@section('dashboard-javascript')
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
    $url = "{{ url('admin/purchase_history/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $histories->total/$histories->per_page }}"

    $(document).ready(function(){

        // setupButton();

        $('input[name="start_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $startDate = $(this).val();
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth(), 1));

        $('input[name="end_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top right",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $endDate = $(this).val();
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0));
    });

    $('input[name="search"]').keyup(function (e) { 
        if ($(this).val() != ""){
            $isEmpty = false;
        }

        if (!$isEmpty){
            $page = 1
            $search_query = $(this).val();
            filter();
        }

        if ($(this).val() == ""){
              $isEmpty = true;
        }
    });
</script>
@endsection