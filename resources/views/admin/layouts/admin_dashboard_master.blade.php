@extends('layouts/master')
@section('css')
<style>
    body {
        background: #F0F1F3;
    }

    @media only screen and (max-width: 768px){
        .page-content{
            padding-top: 100px!important;
        }
    }

    /* svg{
        fill: red!important;
    } */
</style>
@yield('dashboard-css')

@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>

@yield('dashboard-javascript')

<script>

    updateViews()

    $(window).resize(function(){
        updateViews()
    })

    function updateViews(){
        if (window.innerWidth < 1366) {
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        } else {
            $('#sidebar').addClass('show');
            $('#page-content-wrapper').addClass('wrapped')
        }
    }
    
    function onMenuClicked(){
        if ($('#sidebar').hasClass('show')){
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        }else{
            $('#sidebar').addClass('show');
            $('#overlay').addClass('show');
            $('#page-content-wrapper').addClass('wrapped')
        }

        // if ($('#right-sidebar').hasClass('show')){
        //     $('#sidebar').removeClass('show');
        //     $('#overlay').removeClass('show');
        //     $('#page-content-wrapper').removeClass('wrapped')
        // }else{
        //     $('#sidebar').addClass('show');
        //     $('#overlay').addClass('show');
        //     $('#page-content-wrapper').addClass('wrapped')
        // }
    }

    $('#overlay').click(function(){
        if ($('#sidebar').hasClass('show')){
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        }

        if ($('#right-sidebar').hasClass('show')){
            $('#right-sidebar').removeClass('show');
            $('#overlay').removeClass('show');
        }
    })

    $('.btn-edit-profile').click(function (e) { 
        // $('.change-password-overlay').removeClass('d-none');
        $('.change-password').removeClass('d-none');
        $('.change-password').addClass('d-flex');
    });

    $('.change-password .btn-cancel').click(function (e) { 
        $('.change-password').addClass('d-none');
        $('.change-password').removeClass('d-flex');

        $('input[name="old_password"]').val("");
        $('input[name="new_password"]').val("");

        $('.change-password .message').addClass('d-none');
    });

    function rightSidebarToggler(){
        $('#right-sidebar').addClass('show');
        $('#overlay').addClass('show');
    }

    $('.btn-change-password').click(function (e) { 
        $('.change-password .message').addClass('d-none');
        
        if($('input[name="old_password"]').val() != "" && $('input[name="new_password"]').val() != ""){
            $('.change-password .buttons').removeClass('d-flex');
            $('.change-password .buttons').addClass('d-none');
            $('.change-password .loading').removeClass('d-none');

            $.ajax({
                type: "POST",
                url: "{{ url('admin/change_password') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "old_password": $('input[name="old_password"]').val(),
                    "new_password": $('input[name="new_password"]').val()
                },
                dataType: "json",
                success: function (response) {
                    $('input[name="old_password"]').val("");
                    $('input[name="new_password"]').val("");

                    $('.change-password .loading').addClass('d-none');
                    $('.change-password .buttons').removeClass('d-none');
                    $('.change-password .buttons').addClass('d-flex');

                    if (response.success){
                        $('.change-password').addClass('d-none');
                        $('.change-password').removeClass('d-flex');
                    }else{
                        $('.change-password .message').text("**" + response.message);
                        $('.change-password .message').removeClass('d-none');
                    }                
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        }else{
            $('.change-password .message').text("**Please fill the blanks");
            $('.change-password .message').removeClass('d-none');
        }
    });

</script>
@endsection
@section('master-content')
<div class="d-flex" style="overflow-x:hidden;">

    <div id="overlay">
    </div>

    @yield('outside-content')

    <div class="change-password m-auto d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999">
        <div class="change-password-overlay" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:998; background: rgba(0,0,0,0.5)">
        </div>
        <div class="m-auto py-5" style="background: white; width:450px; max-width:90%; border-radius:10px;; z-index:999">
            {{-- <form method="POST" action="{{ url('admin/change_password') }}">
                @csrf --}}
                <p class="text-center text-large font-semiBold mb-3">Make a new password</p>
                <div class="mx-3 mx-md-5">
                    <p class="message text-danger d-none">**message</p>
                    <div class="input-container mb-3" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input name="old_password" type="password" placeholder="Old Password" class="d-inline form-control p-3">
                    </div>
                    <div class="input-container mb-3" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input name="new_password" type="password" placeholder="New Password" class="d-inline form-control p-3"">
                    </div>
                    <p class="text-center loading d-none">Please wait...</p>
                    {{-- <div class=""></div> --}}
                    <div class="d-flex buttons">
                        <button class="btn btn-cancel btn-danger w-100 mr-2 flex-grow-1">Cancel</button>
                        <button class="btn btn-change-password btn-primary w-100 ml-2 flex-grow-1">Confirm</button>
                    </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
    

    <div id="sidebar" class="show d-flex">
        <div class="d-flex flex-column" style="overflow-y:auto; height:100vh">
            <div class="header px-4 px-md-5 py-4 py-md-5">
                <div class="row">
                    <div class="col-12 mb-2">
                        <h3 class="username font-medium">UberVest Admin</h3>
                        <h5 class="achievement">Admin</h5>
                    </div>
    
                    <div class="col-12">
                        <button class="btn btn-edit-profile text-center m-0 font-medium d-block py-1 w-100">
                            Change Password
                        </button>
                    </div>
                </div>
            </div>
            <div class="d-flex flex-column h-100">
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pt-3 pb-4 py-md-3 mt-md-3 {{ (Request::segment(2) == 'purchase_history') ? 'active' : '' }}" href="{{ url('admin/purchase_history') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'purchase_history')
                                <img src="{{ asset('images/ic_purchase_history_active.png') }}" width="24px" height="24px" class="mr-3">
                            @else
                                <img src="{{ asset('images/ic_purchase_history_muted.png') }}" width="24px" height="24px" class="mr-3">
                            @endif
                            Purchase History
                        </p>
                        @if (Request::segment(2) == 'purchase_history')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                        
                    </div>
                </a>
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 {{ Request::segment(2) == ('verification_request') ? 'active' : '' }}" href="{{ url('admin/verification_request') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'verification_request')
                                <img src="{{ asset('images/ic_verification_request_active.png') }}" width="24px" height="24px" class="mr-3">
                            @else
                                <img src="{{ asset('images/ic_verification_request_muted.png') }}" width="24px" height="24px" class="mr-3">
                            @endif
                            
                            Verification Request
                        </p>
                        @if (Request::segment(2) == 'verification_request')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                {{-- <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='member_deposit') active @endif" href="{{ url('admin/member_deposit') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'member_deposit')
                                <object data="{{ asset('images/ic_member_deposit_active.svg') }}" width="24px" height="24px" class="mr-3 my-auto" type="image/svg+xml" ></object>
                            @else
                                <object data="{{ asset('images/ic_member_deposit_muted.svg') }}" width="24px" height="24px" class="mr-3 my-auto" type="image/svg+xml" ></object>
                            @endif
                            Member Deposit
                        </p>
                        @if (Request::segment(2)=='member_deposit')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a> --}}
        
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='member_sharing') active @endif" href="{{ url('admin/member_sharing/profit') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'member_sharing')
                                <img src="{{ asset('images/ic_member_sharing_active.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @else
                                <img src="{{ asset('images/ic_member_sharing_muted.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @endif
                            Member Sharing
                        </p>
                        @if (Request::segment(2)=='member_sharing')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='claimed_reward') active @endif" href="{{ url('admin/claimed_reward') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'claimed_reward')
                                <img src="{{ asset('images/ic_claimed_reward_active.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @else
                                <img src="{{ asset('images/ic_claimed_reward_muted.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @endif
                            Claimed Reward
                        </p>
                        @if (Request::segment(2)=='claimed_reward')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='member_list') active @endif" href="{{ url('admin/member_list') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'member_list')
                                <img src="{{ asset('images/ic_member_list_active.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @else
                                <img src="{{ asset('images/ic_member_list_muted.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @endif
                            Member List
                        </p>
                        @if (Request::segment(2)=='member_list')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='withdrawal_request') active @endif" href="{{ url('admin/withdrawal_request/left_pocket') }}">
                    <div class="d-flex justify-content-between">
                        <p class="my-auto d-flex">
                            @if (Request::segment(2) == 'withdrawal_request')
                                <img src="{{ asset('images/ic_withdrawal_request_active.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @else
                                <img src="{{ asset('images/ic_withdrawal_request_muted.png') }}" class="mr-3" width="24px" height="24px" alt="">
                            @endif
                            Withdrawal Request
                        </p>
                        @if (Request::segment(2)=='withdrawal_request')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                <a class="d-block pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(2)=='epin_list') active @endif" href="{{ url('admin/epin_list') }}">
                    <div class="d-flex justify-content-between">
                        <div class="my-auto d-flex">
                            <div class="d-flex mr-3" style="width:24px; height:24px;">
                                @if (Request::segment(2) == 'epin_list')
                                    <i class="fas fa-receipt ml-auto my-auto" style="font-size: 21px;"></i>
                                @else
                                    <i class="fas fa-receipt ml-auto my-auto" style="font-size: 21px; color:#999999"></i>
                                @endif
                            </div>
                            <p>E-Pin List</p>
                        </div>
                        @if (Request::segment(2) == 'epin_list')
                            <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                        @endif
                    </div>
                </a>
        
                <a class="btn-logout d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 w-100 mt-auto mb-2" href="{{ url('admin/logout') }}">
                    <div class="d-flex justify-content-between">
                        <div class="my-auto d-flex">
                            <div class="d-flex mr-3" style="width:24px; height:24px;">
                                <i class="fas fa-sign-out-alt ml-auto my-auto" style="font-size: 21px; color:#EC4B4B"></i>
                            </div>
                            <p>Logout</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        

        
    </div>

    <div id="page-content-wrapper" class="wrapped">
        <nav class="navbar navbar-expand-lg fixed-top navbar-light px-0 py-0 d-block">
            <div class="row h-100 d-flex">
                <div class="col-12 col-md-8 @if(Request::segment(1)=='network') col-lg-3 @else col-lg-6 @endif col-xl-5 w-100 d-flex my-3 my-lg-0" style="border-bottom:5px solid white; position:relative;">
                    <button class="btn my-auto p-0 mx-3 mx-lg-0 h-100" id="menu-toggle" onclick="onMenuClicked()" style="position: absolute">
                        <i class="fas fa-bars"></i>
                    </button>
                    <h4 class="title px-md-5 py-2 py-md-4 my-auto font-primary font-bold mb-0 mx-auto mx-lg-0">@yield('title')</h4>
                </div>
                    @if (Request::segment(2)=='withdrawal_request')
                    <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                        <div class="row h-100 mr-lg-4">
                            <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(3)=='left_pocket') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('admin/withdrawal_request/left_pocket') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(3)=='left_pocket') text-primary @else text-muted @endif">Kantong Kiri</p>
                                    </div>
                                </a>   
                            </div>
                            
                            <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(3)=='right_pocket') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('admin/withdrawal_request/right_pocket') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(3)=='right_pocket') text-primary @else text-muted @endif">Kantong Kanan</p>
                                    </div>
                                </a>   
                            </div>
                        </div>
                    </div>
                    @endif

                    @if (Request::segment(2)=='member_sharing')
                    <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                        <div class="row h-100 mr-lg-4">
                            <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(3)=='profit') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('admin/member_sharing/profit') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(3)=='profit') text-primary @else text-muted @endif">Profit</p>
                                    </div>
                                </a>   
                            </div>
                            
                            <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(3)=='commission') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('admin/member_sharing/commission') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(3)=='commission') text-primary @else text-muted @endif">Commission</p>
                                    </div>
                                </a>   
                            </div>
                        </div>
                    </div>
                    @endif
            </div>
        </nav>
        <div class="page-content px-3 px-md-5">
            @yield('content')
        </div>
    </div>

</div>
@endsection