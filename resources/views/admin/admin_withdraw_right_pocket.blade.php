@extends('admin.layouts.admin_dashboard_master')

@section('title')
WITHDRAWAL REQUEST
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px){
        .page-content{
            padding-top: 150px!important;
        }
    }
</style>
@endsection

@section('content')
<div class="row">

    @if(Session::has('response'))   
        <div class="col-12">
            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
        </div>
    @endif  

    <div class="col-12 col-md-8 mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search">
    </div>

    <div class="col-12 col-md-4 mb-4">
        <div class="row d-flex flex-wrap">
            <div class="col-5">
                <input class="form-control" type="text" name="start_date">
            </div>
            <div class="col-2 d-flex px-0">
                <p class="m-auto text-muted">sampai</p>
            </div>
            <div class="col-5">
                <input class="form-control" type="text" name="end_date">
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Date</th>
                  <th scope="col">Username</th>
                  <th scope="col">Type</th>
                  <th scope="col">Amount</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody class="item-list">
                  @foreach ($withdraws->data as $idx => $withdraw)
                  <tr>
                    <td scope="row">{{ $idx + 1 }}</td>
                    <td>{{ date_format(date_create($withdraw->created_at), "d F Y") }}</td>
                    <td>
                        {{ $withdraw->user->username }}
                    </td>
                    <td>
                        {{ strtoupper($withdraw->type) }}
                    </td>
                    <td>
                        Rp{{number_format(($withdraw->amount), 0, ',', '.')}}
                    </td>
                    <td>{{ $withdraw->status->status_name }}</td>
                    <td class="">
                        <div class="d-flex">
                            <a class="btn btn-primary mr-3 px-4" href="{{ url('/admin/withdrawal_request/right_pocket/'.$withdraw->id) }}">
                                Detail
                            </a>
                            @if($withdraw->status->status_name != "SEND" && $withdraw->status->status_name != "REJECT" )
                                <form class="mr-3" method="POST">
                                    @csrf
                                    <button class="btn btn-send btn-edit px-4">
                                        Send
                                    </button>
                                    <input type="hidden" name="withdraw_id" value="{{ $withdraw->id }}">
                                    <input type="hidden" name="action" value="send">
                                </form>

                                <form method="POST">
                                    @csrf
                                    <button class="btn btn-reject btn-baning px-4" data-id="{{ $withdraw->id }}">
                                        Reject
                                    </button>
                                    <input type="hidden" name="withdraw_id" value="{{ $withdraw->id }}">
                                    <input type="hidden" name="action" value="reject">
                                </form>
                            @endif
                        </div>
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>

          <div class="pagination-container">
            @include('templates.pagination', ["items" => $withdraws, "page" => $page])
        </div>
    </div>

    
</div>
@endsection

@section('dashboard-javascript')
  <script>
    $url = "{{ url('admin/withdrawal_request/right_pocket/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $withdraws->total/$withdraws->per_page }}"

    $(document).ready(function(){
        $('input[name="start_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $startDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth(), 1));

        $('input[name="end_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top right",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $endDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0));
    });

    $('input[name="search"]').keyup(function (e) { 
          if ($(this).val() != ""){
              $isEmpty = false;
          }

          if (!$isEmpty){
              $search_query = $(this).val();
              $page = 1;
              filter();
          }

          if ($(this).val() == ""){
              $isEmpty = true;
          }
      });
  </script>
@endsection