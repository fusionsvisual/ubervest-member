@extends('admin.layouts.admin_dashboard_master')

@section('title')
MEMBER SHARING
@endsection

@section('content')
<div class="row mb-4">

    @if(Session::has('response'))   
    <div class="col-12">
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    </div>
    @endif 

    <div class="col-12 col-md-9 col-xl-10  mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search username">
    </div>

    <div class="col-12 col-md-3 col-xl-2 mb-3">
        <form class="csv-form h-100" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="d-flex btn btn-review h-100 w-100 p-0" href="" style="background: #C4EAFF; border-color: #C4EAFF">
                <label class="d-flex h-100 w-100" for="csv">
                    <div class="m-auto d-flex">
                        <div class="csv-placeholder d-flex">
                            <object data="{{ asset('images/ic_upload_csv.svg') }}" width="24px" height="24px" class="d-inline-block mr-2 my-auto" type="image/svg+xml" ></object>
                            <p class="d-inline-block font-bold">
                                Upload CSV
                            </p>
                        </div>
                        <p class="d-none csv-loading font-bold">Uploading..</p>
                    </div>
                </label>
                <input id="csv" class="d-none form-control" type="file" name="csv" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
            </div>
        </form>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width:5%">No</th>
                        <th scope="col" style="width:10%">Last Modified</th>
                        <th scope="col" style="width:25%">Username</th>
                        <th scope="col" style="width:20%">Profit</th>
                        <th scope="col" style="width:25%">Action</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($sharings->data as $idx => $sharing)
                    <tr>
                        <td scope="row">{{ $idx + 1 }}</td>
                        <td>{{ date_format(date_create($sharing->updated_at), "d F Y") }}</td>
                        <td>{{ $sharing->username }}</td>
                        <td>Rp{{number_format($sharing->profit_sharing, 0, ',', '.')}}</td>
                        <td class=""  style="vertical-align:top!important;">
                            <div class="">
                                <a class="d-inline-block btn btn-edit px-4" href="{{ url('/admin/member_sharing/profit/'.$sharing->id) }}">
                                    Edit
                                </a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            @include('templates.pagination', ["items" => $sharings, "page" => $page])
          </div>
    </div>


</div>
@endsection

@section('dashboard-javascript')
  <script>
    $url = "{{ url('admin/member_sharing/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $sharings->total/$sharings->per_page }}"

    $('input[name="search"]').keyup(function (e) { 
          if ($(this).val() != ""){
              $isEmpty = false;
          }

          if (!$isEmpty){
              $search_query = $(this).val();
              $page = 1;
              filter();
          }

          if ($(this).val() == ""){
              $isEmpty = true;
          }
      });

      $("input[name='csv']").change(function (e) { 
          $('.csv-placeholder').addClass('d-none');
          $('.csv-placeholder').removeClass('d-flex');

          $('.csv-loading').addClass('d-block');
          $('.csv-loading').removeClass('d-none');
          $('.csv-form').submit();
      });
  </script>
@endsection