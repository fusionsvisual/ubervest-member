@extends('admin.layouts.admin_dashboard_master')

@section('title')
MEMBER LIST
@endsection

@section('dashboard-css')
    <style>
        .member-card{
            
        }
    </style>
@endsection

@section('outside-content')
    <div class="select-sponsor-popup d-none" style="position: fixed; top:0; left:0; right:0; bottom:0;; z-index:5;">
        <div style="position:fixed; top:0; left:0; right:0; bottom:0; background:rgba(0,0,0,0.5)"></div>
        <div class="m-auto" style="background: white; width: 414px; z-index:6; border-radius:10px;">
            <div class="d-flex px-4 py-3">
                <h5 class="font-bold  m-0">Select Sponsor</h5>
                <button class="btn btn-close p-0 ml-auto" style="width:24px; width:24px; background:none;"><i class="fas fa-times"></i></button>
            </div>
            <hr class="m-0">
            <div class="" style="overflow: scroll; max-height:350px; ">
                @foreach ($users as $sponsor)
                    <button class="btn btn-sponsor-item d-block w-100 p-0 text-left p-3 px-4" data-id="{{ $sponsor->id }}" style="background: none">
                        {{ $sponsor->username }}
                    </button>
                    <hr class="m-0">
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('content')

@if(Session::has('response'))   
    <div class="col-12">
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    </div>
@endif 

<form method="POST">
    @csrf
    <div class="row d-flex">

        <div class="col-12 col-md-4 mb-4">
            <div style="position: relative; padding-top:60%">
                <div class="bonus-container" style="position: absolute; top:0 ;left: 0; right: 0; bottom:0; background: url('../../images/bg_seasonal_bonus.png');">
                    <div class="d-flex flex-column p-4 h-100">
                        <p class="font-medium text-white font-white mb-2">Seasonal Bonus</p>
                        <h1 class="font-bold text-white">${{ number_format($user->seasonal_bonus, 0, ',', '.') }}</h1>
                        <h3 class="text-white mt-auto">${{ number_format($user->total_seasonal_bonus, 0, ',', '.') }}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 mb-4">
            <div style="position: relative; padding-top:60%">
                <div class="bonus-container" style="position: absolute; top:0 ;left: 0; right: 0; bottom:0; background: url('../../images/bg_fast_track.png');">
                    <div class="d-flex flex-column p-4 h-100">
                        <p class="font-medium text-white font-white mb-2">Fast Track Bonus</p>
                        <h1 class="font-bold text-white">Rp{{ number_format($user->fast_track_bonus, 0, ',', '.') }}</h1>
                        <h3 class="text-white mt-auto">Rp{{ number_format($user->total_fast_track_bonus, 0, ',', '.') }}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 mb-4">
            <div style="position: relative; padding-top:60%">
                <div class="bonus-container" style="position: absolute; top:0 ;left: 0; right: 0; bottom:0; background: url('../../images/bg_link_up.png');">
                    <div class="d-flex flex-column p-4 h-100">
                        <p class="font-medium text-white font-white mb-2">Link Up Bonus</p>
                        <h1 class="font-bold text-white">Rp{{ number_format($user->link_up_bonus, 0, ',', '.') }}</h1>
                        <h3 class="text-white mt-auto">Rp{{ number_format($user->total_link_up_bonus    , 0, ',', '.') }}</h3>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <div class="member-card" style="position: relative; padding-top:60%;">
                <div class="bonus-container d-flex flex-column p-4 p-xl-5 text-white" style="position: absolute; top:0; left:0; right:0; bottom:0; background:url('../../images/img_bg_member_card_left_leg.png');">
                    <div class="d-flex justify-content-between">
                        <h1 class="text-white m-0 font-regular">Profit Sharing</h1>
                    </div>
    
                    <h1 class="text-white font-bold mt-auto">Rp{{number_format($user->profit_sharing, 0, ',', '.')}}</h1>
                    <h3 class="text-white">Rp{{number_format($user->total_profit_sharing, 0, ',', '.')}}</h3>
                </div>
            </div>
            
        </div>
        <div class="col-12 col-md-6 mb-4">
            <div class="member-card" style="position: relative; padding-top:60%;">
                <div class="bonus-container d-flex flex-column p-4 p-xl-5 text-white" style="position: absolute; top:0; left:0; right:0; bottom:0; background:url('../../images/img_bg_member_card_right_leg.png');">
                    <div class="d-flex justify-content-between">
                        <h1 class="text-white m-0 font-regular">Commission Sharing</h1>
                    </div>
    
                    <h1 class="text-white font-bold mt-auto">Rp{{number_format($user->commision_sharing, 0, ',', '.')}}</h1>
                    <h3 class="text-white">Rp{{number_format($user->total_commision_sharing, 0, ',', '.')}}</h3>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Username</p>
            <input class="form-control" type="text" name="username" id="" value="{{ $user->username }}">
        </div>  

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Meta ID</p>
            <input class="form-control" type="text" name="meta_id" id="" value="{{ $user->meta_id }}">
        </div>  

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Email</p>
            <input class="form-control" type="email" name="email" id="" value="{{ $user->email }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Phone Number</p>
            <input class="form-control" type="text" name="phone_number" id="" value="{{ $user->phone_number }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Province</p>
            <input class="form-control" type="text" name="province_name" id="" value="{{ $user->province_name }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">City</p>
            <input class="form-control" type="text" name="city_name" id="" value="{{ $user->city_name }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Bank Name</p>
            <input class="form-control" type="text" name="bank_code" id="" value="{{ $user->bank_code }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Bank Account Name</p>
            <input class="form-control" type="text" name="account_holder_name" id="" value="{{ $user->account_holder_name }}">
        </div>

        <div class="col-12 mb-4">
            <p class="font-semiBold mb-2">Bank Account Number</p>
            <input class="form-control" type="text" name="account_number" id="" value="{{ $user->account_number }}">
        </div>

        <div class="col-12 mb-4">
            <p class="font-semiBold mb-2">Deposit</p>
            <input class="form-control" type="text" name="deposit" id="" value="{{ $user->deposit }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Seasonal Bonus (Withdrawable)</p>
            <input class="form-control" type="text" name="seasonal_bonus" id="" value="{{ $user->seasonal_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Total Seasonal Bonus</p>
            <input class="form-control" type="text" name="total_seasonal_bonus" id="" value="{{ $user->total_seasonal_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Fast Track Bonus (Withdrawable)</p>
            <input class="form-control" type="text" name="fast_track_bonus" id="" value="{{ $user->fast_track_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Total Fast Track Bonus</p>
            <input class="form-control" type="text" name="total_fast_track_bonus" id="" value="{{ $user->total_fast_track_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Link Up Bonus (Withdrawable)</p>
            <input class="form-control" type="text" name="link_up_bonus" id="" value="{{ $user->link_up_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Total Link Up Bonus</p>
            <input class="form-control" type="text" name="total_link_up_bonus" id="" value="{{ $user->total_link_up_bonus }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Profile Sharing (Withdrawable)</p>
            <input class="form-control" type="text" name="profit_sharing" id="" value="{{ $user->profit_sharing }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Total Profile Sharing</p>
            <input class="form-control" type="text" name="total_profit_sharing" id="" value="{{ $user->total_profit_sharing }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Commision Sharing (Withdrawable)</p>
            <input class="form-control" type="text" name="commision_sharing" id="" value="{{ $user->commision_sharing }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Total Commision Sharing</p>
            <input class="form-control" type="text" name="total_commision_sharing" id="" value="{{ $user->total_commision_sharing }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Left BV (Current)</p>
            <input class="form-control" type="text" name="temporary_left_bv" id="" value="{{ $user->temporary_left_bv }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Left BV (Total)</p>
            <input class="form-control" type="text" name="left_bv" id="" value="{{ $user->left_bv }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Right BV (Current)</p>
            <input class="form-control" type="text" name="temporary_right_bv" id="" value="{{ $user->temporary_right_bv }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Right BV (Total)</p>
            <input class="form-control" type="text" name="right_bv" id="" value="{{ $user->right_bv }}">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">E-Pin</p>
            <input class="form-control" type="text" name="e_pin" id="" value="{{ $user->e_pin }}" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Badge</p>
            <input class="form-control" type="text" name="e_pin" id="" value="@if($user->badge != null){{ $user->badge->reward_title }}@else No Badge @endif" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Sponsor</p>
            <button class="form-control btn-sponsor-input text-left" style="height: 50px; border-radius: 6px; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1); border:0px;">@isset($user->sponsor_user->username){{ $user->sponsor_user->username }}@endisset</button>
            <input type="hidden" name="sponsor_id" value="@isset($user->sponsor_user->id){{ $user->sponsor_user->id }}@endisset">
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Upline</p>
            <input class="form-control" type="text" name="upline_name" id="" value="@isset($user->upline_user->username){{ $user->upline_user->username }} @endisset" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Leg</p>
            <input class="form-control" type="text" name="leg" id="" value="{{ $user->leg }}" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Level</p>
            <input class="form-control" type="text" name="upline_name" id="" value="Level {{ $user->level }}" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Left Leg</p>
            <input class="form-control" type="text" name="left_total" id="" value="{{ $user->left_total }}" disabled>
        </div>

        <div class="col-12 col-md-6 mb-4">
            <p class="font-semiBold mb-2">Right Leg</p>
            <input class="form-control" type="text" name="right_total" id="" value="{{ $user->right_total }}" disabled>
        </div>

        <div class="ml-auto col-12 col-md-2 mb-4">
            <input class="btn btn-primary w-100" type="submit" value="Save">
        </div>
    </div>
</form>

@endsection

@section('dashboard-javascript')
    <script>
        $('.btn-sponsor-input').click(function (e) { 
            e.preventDefault();
            showSelectSponsor();
        });

        $('.btn-close').click(function (e) { 
            $('.select-sponsor-popup').addClass('d-none');
            $('.select-sponsor-popup').removeClass('d-flex');
        });

        $('.btn-sponsor-item').click(function (e) { 
            $('.btn-sponsor-input').text($(this).text());
            $('input[name="sponsor_id"]').val($(this).data('id'));
            hideSelectSponsor();
        });

        function hideSelectSponsor(){
            $('.select-sponsor-popup').addClass('d-none');
            $('.select-sponsor-popup').removeClass('d-flex');
        }

        function showSelectSponsor(){
            $('.select-sponsor-popup').addClass('d-flex');
            $('.select-sponsor-popup').removeClass('d-none');
        }
    </script>
@endsection