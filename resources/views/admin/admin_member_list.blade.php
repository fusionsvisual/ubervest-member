@extends('admin.layouts.admin_dashboard_master')

@section('title')
MEMBER LIST
@endsection

@section('content')
<div class="row">
  
    @if(Session::has('response'))   
      <div class="col-12">
          <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
      </div>
    @endif 

    <div class="col-12 mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search username">
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Username</th>
                  <th scope="col">Meta ID</th>
                  <th scope="col">Deposit</th>
                  <th scope="col">Left Leg</th>
                  <th scope="col">Right Leg</th>
                  <th scope="col">Password</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody class="item-list">
                @foreach ($users->data as  $idx => $user)
                  <tr>
                    <td scope="row">{{ $idx + 1 }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->meta_id }}</td>
                    <td>${{number_format($user->deposit, 0, ',', '.')}}</td>
                    <td>{{ $user->left_total }}</td>
                    <td>{{ $user->right_total }}</td>
                    <td>{{ $user->first_password }}</td>
                    <td>
                      @if($user->is_banned)
                          BANNED
                      @else
                          ACTIVE
                      @endif
                  </td>
                    <td class="">
                        <div class="">
                          <a class="d-inline-block btn btn-edit px-4" href="{{ url('/admin/member_list/'.$user->id) }}">
                              Edit
                          </a>
                          @if($user->is_banned)
                          <a class="d-inline-block btn btn-review px-4" href="{{ url('/admin/member_list/unban/'.$user->id) }}">
                              Unban
                          </a>
                          @else
                          <a class="d-inline-block btn btn-baning px-4" href="{{ url('/admin/member_list/ban/'.$user->id) }}">
                            Ban
                          </a>
                          @endif
                          <a class="d-inline-block btn btn-danger px-4" href="{{ url('/admin/member_list/delete/'.$user->id) }}">
                            Delete
                          </a>
                        </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="pagination-container">
            @include('templates.pagination', ["items" => $users, "page" => $page])
          </div>
    </div>

    
</div>
@endsection

@section('dashboard-javascript')
  <script>
    $url = "{{ url('admin/member_list/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $users->total/$users->per_page }}"

    $('input[name="search"]').keyup(function (e) { 
          if ($(this).val() != ""){
              $isEmpty = false;
          }

          if (!$isEmpty){
              $search_query = $(this).val();
              $page = 1;
              filter();
          }

          if ($(this).val() == ""){
              $isEmpty = true;
          }
      });
  </script>
@endsection