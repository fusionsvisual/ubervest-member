@extends('admin.layouts.admin_dashboard_master')

@section('title')
DETAIL HISTORY TRANSAKSI
@endsection

@section('dashboard-css')
<style>
    /* body{} */
</style>
@endsection

@section('content')

{{-- REJECT REASON POPUP --}}
<div class="overlay-container d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999;">
    <div class="d-flex" style="position: absolute; top:0; left:0; right:0; bottom:0;">
        <div class="overlay w-100 h-100" style="background: rgba(0,0,0,0.5); position: absolute; top:0; left:0; right:0; bottom:0; z-index: 998"></div>
        <div class="m-auto p-4" style="background: white; max-width:90%; width:425px; border-radius: 10px; z-index: 999">
        <form action="{{ url()->current()."/update" }}" method="POST">
            @csrf
            <textarea class="form-control mb-3" type="text" name="rejected_reason" id="" style="!important; box-shadow: none !important; border: 1px solid #999999 !important;" rows="4" placeholder="Write reject reasons" required></textarea>
            <button class="btn btn-reject w-100">REJECT</button>
            <input type="hidden" name="status" value="REJECT" id="">
        </form>
        </div>
    </div>
</div>

<div class="receipt-overlay-container d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999;">
    <div class="d-flex" style="position: absolute; top:0; left:0; right:0; bottom:0;">
        <div class="overlay w-100 h-100" style="background: rgba(0,0,0,0.5); position: absolute; top:0; left:0; right:0; bottom:0; z-index: 998"></div>
        <div class="m-auto p-4" style="background: white; max-width:75%; width:425px; border-radius: 10px; z-index: 999">
        <img class="w-100" src="{{ Network::ASSET_URL.$history->payment_receipt }}" alt="" style="object-fit: contain">
        </div>
    </div>
</div>

<div class="py-4 px-5 mb-5" style="background: white">
    <div class="row mb-5">
        @if(Session::has('response'))   
            <div class="col-12">
                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
            </div>
        @endif 
        <div class="col-12 col-md-8">
            <h1 class="font-semiBold" style="font-size: 72px;">Invoice</h1>
            <h3 class="font-regular text-extraLarge">{{ $history->invoice_code }}</h3>
        </div>
        <div class="col-12 col-md-4 d-none d-md-block">
            <div class="d-flex w-100 h-100">
                <div class="ml-auto my-auto">
                    <p class="text-md-right text-muted">PT. Untung Bersama Investindo</p>
                    <p class="text-md-right text-muted">Jl. Mayjend Yono Soewoyo Graha Family, Surabaya 60226 (Spazio Building)</p>
                    <p class="text-md-right text-muted">Indonesia</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-12 col-md-5 mb-4 mb-md-0">
            <h5 class="font-regular text-muted">Bill to:</h5>
            <h5 class="font-semiBold">{{ $history->email }}</h5>
            <h5 class="font-semiBold">{{ $history->phone_number }}</h5>
        </div>
        <div class="col-12 col-md-4 mb-4 mb-md-0">
            <h5 class="font-regular text-muted">Invoice Date:</h5>
            <h5 class="font-semiBold">{{ date_format(date_create($history->created_at), "d F Y") }}</h5>
        </div>
        <div class="col-12 col-md-3">
            <h5 class="font-regular text-muted">Status:</h5>
            <div class="d-inline-block mb-1" style="border-radius: 10px; @switch($history->status->status_name)
                @case(OrderStatus::PAID)
                background:rgba(32,186,56,1)
                @break
                @case(OrderStatus::UNPAID)
                background:rgba(254,97,0,1)
                @break
                @case(OrderStatus::WAITING)
                background:rgba(255,209,90,1)
                @break
                @default
                background:rgba(236,75,75,1)
                @break
             @endswitch">
                <h5 class="font-semiBold font-white mx-3 my-1">{{ $history->status->status_name }}</h5>
            </div>
            @if ($history->reject_reason != null)
                <p class="font-red">*{{ $history->reject_reason }}</p>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="table-responsive mb-4 mb-md-4">
                <table class="table outline-table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Package</th>
                            <th scope="col">E-Pin</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody class="right-legs">
                        @foreach ($history->purchase_packages as $idx => $purchase_package)
                        <tr>
                            <td style="@if(count($history->purchase_packages) == 1) border-radius: 8px 0px 0px 8px !important; @endif">{{ $idx + 1 }}</td>
                            <td>{{ $purchase_package->package->package_code }}</td>
                            <td>{{ $purchase_package->e_pin }}</td>
                            <td style="@if(count($history->purchase_packages) == 1) border-radius: 0px 8px 8px 0px !important; @endif">Rp{{number_format($purchase_package->package->package_price, 0, ',', '.')}}</td>
                        </tr>
                        {{-- <tr>
                            <td>{{ $idx + 1 }}</td>
                            <td>{{ $purchase_package->package->package_code }}</td>
                            <td>{{ $purchase_package->e_pin }}</td>
                            <td>Rp{{number_format($purchase_package->package->package_price, 0, ',', '.')}}</td>
                        </tr> --}}
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-8 mb-5 mb-md-0">
            <div class="pr-4">
                <h5 class="font-regular text-muted">Keterangan:</h5>            
                <p class="">@if ($history->description != ""){{ $history->description }}@else No description @endif</p>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="d-flex mb-3">
                <h5 class="font-regular text-muted">Total:</h5>
                <h5 class="font-regular font-semiBold ml-auto">
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($history->purchase_packages as $purchase_package)
                        @php
                            $total = $total + $purchase_package->package->package_price
                        @endphp
                    @endforeach
                    Rp{{number_format($total, 0, ',', '.')}}
                </h5>
            </div>
            @if ($history->status->status_name != OrderStatus::UNPAID)
                <div class="row">
                    <div class="col-12 mb-3">
                        <button class="btn btn-primary w-100" style="height:50px" onclick="didSeeReceipt(this)">SEE PAYMENT RECEIPT</button>
                    </div>
                    <div class="col-12 col-md-6 pr-md-1">
                        <form action="{{ url()->current()."/update" }}" method="POST">
                            @csrf
                            <button class="btn btn-done mb-3 w-100">RELEASE</button>
                            <input type="hidden" name="status" value="RELEASE" id="">
                        </form>
                    </div>
                    <div class="col-12 col-md-6 pl-md-1">
                        <button class="btn btn-reject w-100" onclick="didRejectTapped(this)">REJECT</button>
                    </div>                    
                </div>
            @endif
        </div>
    </div>
</div>

{{-- <div class="row">

    @if(Session::has('response'))   
        <div class="col-12">
            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
        </div>
    @endif 

    <div class="col-12 col-md-8 mb-4 mb-md-0">
        <div class="d-flex flex-column h-100">
            <div>
                <h1 class="font-semiBold" style="@switch($history->status->status_name)
                    @case(OrderStatus::PAID)
                    color:rgba(32,186,56,1)
                    @break
                    @case(OrderStatus::UNPAID)
                    color:rgba(254,97,0,1)
                    @break
                    @case(OrderStatus::WAITING)
                    color:rgba(255,209,90,1)
                    @break
                    @default
                    color:rgba(255,0,0,1)
                    @break
                 @endswitch">
                    {{ $history->status->status_name }}
                </h1>
                <h3 class="font-semiBold">{{ $history->invoice_code }}</h3>
            </div>
            
            <div class="mt-3 mt-md-auto">
                <p class="text-muted mb">Bill to:</p>
                <p class="mb-2">{{ $history->email }} - {{ $history->phone_number }}</p>

                <p class="text-muted">Time request:</p>
                <p>{{ date_format(date_create($history->created_at), "d F Y") }}</p>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-4">
        <div class="d-flex ml-md-auto" style="position: relative;background: rgba(217, 217, 217, 1); max-width: 350px; height: 233px; border-radius: 10px; overflow:hidden;">
            <img class="w-100 h-100" src="@if($history->payment_receipt){{ Network::ASSET_URL.$history->payment_receipt }}@endif" alt="" style="position: absolute; top:0; left:0; right:0; bottom:0; object-fit: cover;">
            <p class="m-auto" style="color: rgba(106,106,106,1);">No Payment Receipt</p>
        </div>
    </div>

    <div class="col-12 mt-4">
        <div class="table-responsive mb-3 mb-md-3">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Package</th>
                        <th scope="col">E-Pin</th>
                        <th scope="col">Amount</th>
                    </tr>
                </thead>
                <tbody class="right-legs">
                    @foreach ($history->purchase_packages as $idx => $purchase_package)
                    <tr>
                        <td>{{ $idx + 1 }}</td>
                        <td>{{ $purchase_package->package->package_code }}</td>
                        <td>{{ $purchase_package->e_pin }}</td>
                        <td>Rp{{number_format($purchase_package->package->package_price, 0, ',', '.')}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-12 col-md-9 order-1 order-md-0 mb-3 d-none d-md-block">
        <div class="pr-4">
            <p class="mr-4 text-muted">Keterangan:</p>
            <p class="">@if($history->description != ""){{ $history->description }} @else No description @endif</p>
        </div>
    </div>

    <div class="col-12 col-md-3 order-0 order-md-1 pl-md-5 mb-3">
        <div class="mb-3">
            <div class="ml-md-auto d-flex">
                <p class="mr-4 text-muted">Total:</p>
                <p class="font-bold ml-auto">
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($history->purchase_packages as $purchase_package)
                        @php
                            $total = $total + $purchase_package->package->package_price
                        @endphp
                    @endforeach
                    Rp{{number_format($total, 0, ',', '.')}}
                </p>
            </div>
        </div>

        <div class="d-md-none mb-3">
            <p class="mr-4 text-muted">Keterangan:</p>
            <p class="">@if($history->description != ""){{ $history->description }} @else No description @endif</p>
        </div>
        
        @if ($history->status->status_name != OrderStatus::UNPAID)
            <div class="">
                <form action="{{ url()->current()."/update" }}" method="POST">
                    @csrf
                    <button class="btn btn-done mb-3 w-100">Release</button>
                    <input type="hidden" name="status" value="RELEASE" id="">
                </form>
                <button class="btn btn-reject w-100" onclick="didRejectTapped(this)">Reject</button>
            </div>
        @endif
    </div>
    
</div> --}}
    
@endsection

@section('dashboard-javascript')
    <script>
        $paymentReceiptImage = null

        function didRejectTapped(e){
            $('.overlay-container').removeClass('d-none');
        }

        function didSeeReceipt(e){
            $('.receipt-overlay-container').removeClass('d-none');
        }

        $('.overlay').click(function (e) { 
            $('.overlay-container').addClass('d-none');
            $('.receipt-overlay-container').addClass('d-none');
        });

    </script>
@endsection