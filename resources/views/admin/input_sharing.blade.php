@extends('admin.layouts.admin_dashboard_master')

@section('title')
WITHDRAWAL REQUEST
@endsection

@section('content')
<form method="POST">
    @csrf
    <div class="row d-flex">

        @if(Session::has('response'))   
            <div class="col-12">
                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
            </div>
        @endif 
        
        <div class="col-12">
            <div class="mb-4">
                <p class="font-semiBold mb-2">Username</p>
                <input class="form-control" type="text" name="username" id="" value="{{ $sharing->username }}" disabled>
            </div>
    
            <div class="mb-4">
                <p class="font-semiBold mb-2">Meta ID</p>
                <input class="form-control" type="text" name="meta_id" value="{{ $sharing->meta_id }}">
            </div>
    
            <div class="mb-4">
                <p class="font-semiBold mb-2">Profit Sharing</p>
                <input class="form-control" type="text" name="profit_sharing_amount" value="{{ $sharing->commision_sharing }}">
            </div>

            <div class="mb-4">
                <p class="font-semiBold mb-2">Commission Sharing</p>
                <input class="form-control" type="text" name="commission_sharing_amount" value="{{ $sharing->profit_sharing }}">
            </div>
        </div>
        <div class="ml-auto col-12 col-md-2">
            <input class="btn btn-primary w-100" type="submit" value="Save">
        </div>
    </div>
</form>

@endsection