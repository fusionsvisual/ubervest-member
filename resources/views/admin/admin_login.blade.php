@extends('layouts/master')
@section('master-content')
<div id="login" class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <form class="col-12 d-flex" method="POST">
            @csrf
            <div class=""></div>
            <div class="d-inline-block mx-auto" style="width:500px">
                <div class="mb-5" style="height: 75px">
                    <object data="{{ asset('images/ic_logo_original.svg') }}" class="m-auto" type="image/svg+xml" ></object>
                </div>
                <div class="bg-white p-md-5">
                    <p class="text-large font-semiBold muli-bold text-center">ADMIN</p>
                    <div class="mb-3">
                        <h4 class="muli-bold text-center mb-4">Sign in to your account</h4>
                        @if(Session::has('response'))   
                            <div class="col-12">
                                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
                            </div>
                        @endif 
                        <p class="empty-state my-2 d-none" style="color:red">Please fill the blanks.</p>
                    </div>
                    
                    <div class="input-container mb-3" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_profile.svg') }}" alt="">
                        <input name="username" type="text" placeholder="Username" class="d-inline form-control p-3">
                    </div>
                    <div class="input-container" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input name="password" type="password" placeholder="Password" class="d-inline form-control p-3">
                    </div>
                    <input type="submit" class="btn muli-bolder d-block w-100 mt-4 d-block text-center" value="Login" style="border-radius: 20px">
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
