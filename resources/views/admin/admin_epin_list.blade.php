@extends('admin.layouts.admin_dashboard_master')

@section('title')
E-PIN LIST
@endsection

@section('outside-content')
    <div class="create-epin-form d-none" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:99">
      <div class="create-epin-overlay" style="position: absolute; top:0; right:0; left:0; bottom:0; background:rgba(0,0,0,0.5);"></div>
      <div class="m-auto p-5" style="background:white; z-index: 99; width:500px; max-width:90%; border-radius:10px">
        <form action="{{ url('admin/member_list/create_epin') }}" method="POST">
          @csrf
          <h3 class="font-bold mb-3">Select Package</h3>
          <select class="custom-select mb-3" name="package_id" id="" style="height:50px!important;">
            <option value="1">Start Up Pack</option>
            <option value="2">Pro Up Pack</option>
          </select>
          <button class="btn btn-primary w-100" style="height: 50px!important;">Done</button>
        </form>
      </div>
    </div>
@endsection

@section('content')
<div class="row">
  
    @if(Session::has('response'))   
      <div class="col-12">
          <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
      </div>
    @endif 

    <div class="col-12 col-md-9 mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search username">
    </div>

    <div class="col-12 col-md-3 mb-3">
      <button class="btn btn-primary btn-create-epin w-100" style="height: 50px">Create E-Pin</button>
  </div>
  {{-- {{ json_encode($e_pins ) }} --}}

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">Created Date</th>
                  <th scope="col">Owner</th>
                  <th scope="col">E-Pin</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody class="item-list">
                @foreach ($e_pins->data as $idx => $epin)
                <tr>
                  <td scope="row">{{ $idx + 1 }}</td>
                  <td>{{ date_format(date_create($epin->created_at), "d F Y") }}</td>
                  <td>{{ $epin->purchase_room->sponsor_user->username }}</td>
                  <td>{{ $epin->e_pin }}</td>
                  <td>
                    <a class="d-inline-block btn btn-danger px-4" href="{{ url('/admin/epin_list/delete/'.$epin->id) }}">
                      Delete
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>

          <div class="pagination-container">
            @include('templates.pagination', ["items" => $e_pins, "page" => $page])
          </div>
    </div>

    
</div>
@endsection

@section('dashboard-javascript')
  <script>

    $url = "{{ url('admin/epin_list/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $e_pins->total/$e_pins->per_page }}"

    $('input[name="search"]').keyup(function (e) { 
          if ($(this).val() != ""){
              $isEmpty = false;
          }

          if (!$isEmpty){
              $search_query = $(this).val();
              $page = 1;
              filter();
          }

          if ($(this).val() == ""){
              $isEmpty = true;
          }
      });

      $('.btn-create-epin').click(function (e) { 
        $('.create-epin-form').addClass("d-flex");
        $('.create-epin-form').removeClass("d-none");
      });
      
      $('.create-epin-overlay').click(function (e) { 
        $('.create-epin-form').removeClass("d-flex");
        $('.create-epin-form').addClass("d-none");
      });
  </script>
@endsection