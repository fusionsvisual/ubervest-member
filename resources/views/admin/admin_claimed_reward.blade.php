@extends('admin.layouts.admin_dashboard_master')

@section('title')
CLAIMED REWARD
@endsection

@section('content')
<div class="row mb-4">
    
    @if(Session::has('response'))   
        <div class="col-12">
            <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
        </div>
    @endif 

    <div class="col-12  mb-3">
        <input class="form-control" type="text" name="search" placeholder="Search">
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Date</th>
                        <th scope="col">Username</th>
                        <th scope="col">Reward</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($claimed_rewards->data as $idx => $claimed_reward)
                        <tr>
                            <td scope="row">{{ $idx + 1 }}</td>
                            <td>{{ date_format(date_create($claimed_reward->created_at), "d F Y") }}</td>
                            <td>{{ $claimed_reward->user->username }}</td>
                            <td>{{ $claimed_reward->reward->reward_title }}</td>
                            <td>{{ $claimed_reward->status->status_name }}</td>
                            <td class=""  style="vertical-align:top!important;">
                                <div class="">
                                    <form method="POST">
                                        @csrf
                                        <button class="d-inline-block btn btn-review px-4">
                                            Review
                                        </button>
                                        <input type="hidden" name="claimed_reward_id" value="{{ $claimed_reward->id }}">
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            @include('templates.pagination', ["items" => $claimed_rewards, "page" => $page])
        </div>
    </div>


</div>
@endsection

@section('dashboard-javascript')
  <script>
    $url = "{{ url('admin/claimed_reward/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $claimed_rewards->total/$claimed_rewards->per_page }}"

    $('input[name="search"]').keyup(function (e) { 
          if ($(this).val() != ""){
              $isEmpty = false;
          }

          if (!$isEmpty){
              $search_query = $(this).val();
              $page = 1;
              filter();
          }

          if ($(this).val() == ""){
              $isEmpty = true;
          }
      });
  </script>
@endsection