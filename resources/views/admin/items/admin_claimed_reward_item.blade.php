@foreach ($items->data as $idx => $claimed_reward)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($claimed_reward->created_at), "d F Y") }}</td>
    <td>{{ $claimed_reward->user->username }}</td>
    <td>{{ $claimed_reward->reward->reward_title }}</td>
    <td>{{ $claimed_reward->status->status_name }}</td>
    <td class=""  style="vertical-align:top!important;">
        <div class="">
            <form method="POST">
                @csrf
                <button class="d-inline-block btn btn-review px-4">
                    Review
                </button>
                <input type="hidden" name="claimed_reward_id" value="{{ $claimed_reward->id }}">
            </form>
        </div>
    </td>
</tr>
@endforeach