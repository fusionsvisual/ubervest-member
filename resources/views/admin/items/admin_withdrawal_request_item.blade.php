@foreach ($items->data as $idx => $withdraw)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($withdraw->created_at), "d F Y") }}</td>
    <td>{{ $withdraw->user->username }}</td>
    <td>Rp{{number_format(($withdraw->fast_track_withdraw_amount + $withdraw->link_up_withdraw_amount), 0, ',', '.')}} 
        @if ($withdraw->seasonal_withdraw_amount > 0)
        + ${{ $withdraw->seasonal_withdraw_amount }}
        @endif
       </td>
    <td>{{ $withdraw->status->status_name }}</td>
    <td class="">
        <div class="d-flex">
            <a class="btn btn-primary mr-3 px-4" href="{{ url('/admin/withdrawal_request/left_pocket/'.$withdraw->id) }}">
                Detail
            </a>
            @if($withdraw->status->status_name != "SEND" && $withdraw->status->status_name != "REJECT" )
                <form class="mr-3" method="POST">
                    @csrf
                    <button class="btn btn-send btn-edit px-4">
                        Send
                    </button>
                    <input type="hidden" name="withdraw_id" value="{{ $withdraw->id }}">
                    <input type="hidden" name="action" value="send">
                </form>

                <form method="POST">
                    @csrf
                    <button class="btn btn-reject btn-baning px-4" data-id="{{ $withdraw->id }}">
                        Reject
                    </button>
                    <input type="hidden" name="withdraw_id" value="{{ $withdraw->id }}">
                    <input type="hidden" name="action" value="reject">
                </form>
            @endif
        </div>
    </td>
</tr>
@endforeach