@foreach ($items->data as $idx => $epin)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($epin->created_at), "d F Y") }}</td>
    <td>{{ $epin->purchase_room->sponsor_user->username }}</td>
    <td>{{ $epin->e_pin }}</td>
    <td>
    <a class="d-inline-block btn btn-danger px-4" href="{{ url('/admin/epin_list/delete/'.$epin->id) }}">
        Delete
    </a>
    </td>
</tr>
@endforeach