@foreach ($items->data as $idx => $sharing)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ date_format(date_create($sharing->updated_at), "d F Y") }}</td>
    <td>{{ $sharing->username }}</td>
    <td>Rp{{ number_format($sharing->profit_sharing, 0, ',', '.') }}</td>
    <td class="" style="vertical-align:top!important;">
        <div class="">
            <a class="d-inline-block btn btn-edit px-4" href="{{ url('/admin/member_sharing/profit/'.$sharing->id) }}">
                Edit
            </a>
        </div>
    </td>
</tr>
@endforeach
