@foreach ($items->data as $idx => $request)
<tr>
    <td scope="row" style="vertical-align:top!important;">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td scope="row" style="vertical-align:top!important;">{{ date_format(date_create($request->created_at), "d F Y") }}</td>
    <td style="vertical-align:top!important;">{{ $request->user->username }}</td>
    <td>
        <div class="verification-image" style="position: relative; padding-top:55%; width:250px">
            <img class="w-100 h-100" src="{{ Network::ASSET_URL.$request->identity_card }}" alt="" style="position: absolute; top:0; left:0; background:grey; border-radius:10px; object-fit:cover; cursor: pointer;">
        </div>
    </td>
    <td>
        <div class="verification-image" style="position: relative; padding-top:55%; width:250px">
            <img class="w-100 h-100" src="{{ Network::ASSET_URL.$request->proof_of_address }}" alt="" style="position: absolute; top:0; left:0; background:grey; border-radius:10px; object-fit:cover; cursor: pointer;">
        </div>
    </td>
    <td style="vertical-align:top!important;">{{ $request->status->status_name }}</td>
    <td class="" style="vertical-align:top!important;">
        <div class="d-flex">
            <form method="POST">
                @csrf
                <button class="btn d-inline-block btn-release px-4 mr-2">
                    Release
                </button>
                <input type="hidden" name="identity_id" value="{{ $request->id }}">
                <input type="hidden" name="action" value="release">
            </form>
            <form method="POST">
                @csrf
                <button class="btn d-inline-block btn-baning px-4">
                    Reject
                </button>
                <input type="hidden" name="identity_id" value="{{ $request->id }}">
                <input type="hidden" name="action" value="reject">
            </form>
        </div>
    </td>
</tr>
@endforeach