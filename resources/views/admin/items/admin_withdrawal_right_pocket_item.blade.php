@foreach ($items->data as $idx => $withdraw)
<tr>
    <td scope="row">{{ $idx + 1 }}</td>
    <td>{{ date_format(date_create($withdraw->created_at), "d F Y") }}</td>
    <td>
        {{ $withdraw->user->username }}
    </td>
    <td>
        {{ strtoupper($withdraw->type) }}
    </td>
    <td>
        Rp{{number_format(($withdraw->amount), 0, ',', '.')}}
    </td>
    <td>{{ $withdraw->status->status_name }}</td>
    <td class="">
        <div class="d-flex">
            <a class="btn btn-primary mr-3 px-4" href="{{ url('/admin/withdrawal_request/right_pocket/'.$withdraw->id) }}">
                Detail
            </a>
            <a class="btn btn-edit mr-3 px-4" href="{{ url('/admin/withdrawal_request/right_pocket/send/'.$withdraw->id) }}">
                Send
            </a>
            <a class="btn btn-baning mr-3 px-4" href="{{ url('/admin/withdrawal_request/right_pocket/reject/'.$withdraw->id) }}">
                Reject
            </a>
        </div>
    </td>
</tr>
@endforeach