@foreach ($items->data as $idx => $user)
<tr>
    <td scope="row">{{ ($paginate * ($page-1)) + $idx + 1 }}</td>
    <td>{{ $user->username }}</td>
    <td>{{ $user->meta_id }}</td>
    <td>Rp{{number_format($user->deposit, 0, ',', '.')}}</td>
    <td>{{ $user->left_total }}</td>
    <td>{{ $user->right_total }}</td>
    <td>{{ $user->first_password }}</td>
    <td>
        @if($user->is_banned)
            BANNED
        @else
            ACTIVE
        @endif
    </td>
    <td class="">
        <div class="">
          <a class="d-inline-block btn btn-edit px-4" href="{{ url('/admin/member_list/'.$user->id) }}">
              Edit
          </a>
          
          @if($user->is_banned)
          <a class="d-inline-block btn btn-review px-4" href="{{ url('/admin/member_list/unban/'.$user->id) }}">
              Unban
          </a>
          @else
          <a class="d-inline-block btn btn-baning px-4" href="{{ url('/admin/member_list/ban/'.$user->id) }}">
            Ban
          </a>
          @endif
            <a class="d-inline-block btn btn-danger px-4" href="{{ url('/admin/member_list/delete/'.$user->id) }}">
                Delete
            </a>
        </div>
    </td>
  </tr>
</tr>
@endforeach