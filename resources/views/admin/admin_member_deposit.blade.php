@extends('admin.layouts.admin_dashboard_master')

@section('title')
MEMBER DEPOSIT
@endsection

@section('content')
<div class="row mb-4">
    <div class="col-12 col-md-9 col-xl-10 mb-3">
        <input class="form-control" type="text" placeholder="Search">
    </div>

    <div class="col-12 col-md-3 col-xl-2 mb-3">
        <a class="d-flex btn btn-primary h-100 w-100" href="{{ url('/admin/member_deposit/create') }}">
            <p class="m-auto">+ New Deposit</p>
        </a>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-4 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width:5%">No</th>
                        <th scope="col" style="width:10%">Last Modified</th>
                        <th scope="col" style="width:25%">Username</th>
                        <th scope="col" style="width:20%">Account ID</th>
                        <th scope="col" style="width:20%">Total Deposit</th>
                        <th scope="col" style="width:25%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row">1</td>
                        <td>01 Januari 2020</td>
                        <td>Filbert</td>
                        <td>123456</td>
                        <td>IDR 5.000.000</td>
                        <td class=""  style="vertical-align:top!important;">
                            <div class="">
                                <a class="d-inline-block btn btn-edit px-4" href="{{ url('admin/member_deposit/create') }}">
                                    Edit
                                </a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        @include('templates.pagination')
    </div>


</div>
@endsection