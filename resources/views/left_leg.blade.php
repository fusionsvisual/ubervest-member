@extends('layouts/dashboard_master')

@section('title')
NETWORK
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px) {
        .page-content {
            padding-top: 100px !important;
        }

        .right-sidebar-toggler {
            display: block !important;
        }
    }
</style>
@endsection

@section('content')
{{-- {{ json_encode(Session::get('user')) }} --}}

<div class="row">

    <div class="col-12 mb-3">
        <div class="" style="overflow: scroll; white-space: nowrap;">
            @foreach ($available_legs as $available_leg)
            <a class="p-0 mr-2" href="{{ url('network/left_leg/'.$available_leg->level) }}">
                <div class="d-inline-block" style="width: 200px; height:100px; background:#0075FF; border-radius:10px;">
                    <div class="d-flex h-100">
                        <div class="my-auto mx-3">
                            <h5 class="m-0 text-white font-bold">Level {{ $available_leg->level - (Session::get('user.data')->level - 1) }}</h5>
                            <p class="text-white">Available: {{ $available_leg->available }}</p>
                        </div>
                    </div>
                </div>
            </a>
            @endforeach
            
        </div>
    </div>

    <div class="col-12 col-md-9 mb-3">
        <input class="search-input form-control" type="text" placeholder="Search">
    </div>

    <div class="col-12 col-md-3 mb-4">
        <select class="level-select form-control custom-select" name="" id="">
            <option value="">All Level</option>
            @foreach ($total_level as $level)
                <option value="{{ $level }}">Level {{ $level-(Session::get('user.data')->level - 1) }}</option>
            @endforeach
        </select>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-2 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width:10%">No</th>
                        <th scope="col" style="width:20%">Username</th>
                        <th scope="col" style="width:30%">Upline</th>
                        <th scope="col" style="width:20%">Package</th>
                        <th scope="col" style="width:20%">Level</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($legs->data as $idx => $leg)
                    <tr>
                        <td scope="row">{{ $idx+1 }}</td>
                        <td>{{ $leg->username }}</td>
                        <td>{{ $leg->upline_user->username }}</td>
                        <td>@isset($leg->package->package_code){{ $leg->package->package_code }}@endisset</td>
                        <td>Level {{ $leg->level - (Session::get('user.data')->level - 1) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            @include('templates.pagination', ["items" => $legs, "page" => $page])
        </div>
    </div>
</div>
@endsection

@section('dashboard-javascript')
    <script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
    <script>
        $url = "{{ url('network/left_leg/filter') }}";
        $token = "{{ csrf_token() }}";
        $totalCount = "{{ $legs->total/$legs->per_page }}"
        $level_query = "";

        $('.search-input').keyup(function (e) { 
            if ($(this).val() != ""){
              $isEmpty = false;
            }

            if (!$isEmpty){
                $search_query = $(this).val();
                $page = 1;
                filter();
            }

            if ($(this).val() == ""){
                $isEmpty = true;
            }
        });

        $('.level-select').change(function (e) { 
            $level_query = $(this).val();
            $page = 1;
            filter();            
        });
    </script>
@endsection