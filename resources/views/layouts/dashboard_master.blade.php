@extends('layouts/master')
@section('css')
<style>
    body {
        background: #F0F1F3;
    }

    div.privacy-policy ol {
        counter-reset: item;
        margin-bottom: 1rem;
    }
    div.privacy-policy ol li {
        display: block;
        position: relative;
        margin-bottom:1rem;
    }

    div.privacy-policy ol li p {
        margin-bottom:1rem;
    }
    div.privacy-policy ol li:before {
        content: counters(item, ".")".";
        counter-increment: item;
        position: absolute;
        margin-right: 100%;
        right: 10px; /* space between number and text */
    }
    li.no-number{
        margin-left: -16;
    }
    li.no-number::before{
        display: none;
        margin-left: -16;
    }
</style>
@yield('dashboard-css')
@endsection

@section('javascript')

@yield('dashboard-javascript')

<script>

    updateViews()

    $(window).resize(function(){
        updateViews()
    })

    function updateViews(){
        if (window.innerWidth < 1366) {
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        } else {
            $('#sidebar').addClass('show');
            $('#page-content-wrapper').addClass('wrapped')
        }
    }
    
    function onMenuClicked(){
        if ($('#sidebar').hasClass('show')){
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        }else{
            $('#sidebar').addClass('show');
            $('#overlay').addClass('show');
            $('#page-content-wrapper').addClass('wrapped')
        }
    }

    $('.btn-radio').click(function (e) {     
        if($('input[name="is_checked"]').val() == 'true'){
            $('.btn-save-change-password').prop('disabled', true);
            $('input[name="is_checked"]').val(false);
            $('.radio-outer').css("background", "white");
        }else{
            $('.btn-save-change-password').prop('disabled', false);
            $('input[name="is_checked"]').val(true);
            $('.radio-outer').css("background", "#0075FF");
        }
    });

    $('#overlay').click(function(){
        if ($('#sidebar').hasClass('show')){
            $('#sidebar').removeClass('show');
            $('#overlay').removeClass('show');
            $('#page-content-wrapper').removeClass('wrapped')
        }

        if ($('#right-sidebar').hasClass('show')){
            $('#right-sidebar').removeClass('show');
            $('#overlay').removeClass('show');
        }

    })

    function rightSidebarToggler(){
        $('#right-sidebar').addClass('show');
        $('#overlay').addClass('show');
    }

    $('.btn-code-ethics').click(function (e) { 
        $('.privacy-policy-popup').removeClass('d-none');
        $('.privacy-policy-popup').addClass('d-flex');
    });

    $('.btn-terms-condition').click(function (e) { 
        $('.terms_condition-popup').removeClass('d-none');
        $('.terms_condition-popup').addClass('d-flex');
    });

    $('.btn-terms-condition-close').click(function (e) { 
        $('.terms_condition-popup').addClass('d-none');
        $('.terms_condition-popup').removeClass('d-flex');
    });

    $('.btn-privacy-policy-close').click(function (e) { 
        $('.privacy-policy-popup').removeClass('d-flex');
        $('.privacy-policy-popup').addClass('d-none');
    });

    $('.change-password-form').submit(function (e) { 
        e.preventDefault();

        $('.btn-save-change-password').addClass('d-none');
        $('.btn-save-change-password').removeClass('d-block');

        $('.change-password-loading-img').addClass('d-block')
        $('.change-password-loading-img').removeClass('d-none')
        
        $.ajax({
        type: "POST",
        url: "{{ url('change_password') }}",
        data: {
            "_token": "{{ csrf_token() }}",
            "old_password": $('input[name="old_password"]').val(),
            "new_password": $('input[name="password"]').val()
        },
        dataType: "json",
        success: function (response) {
            console.log(response);

            if (response.success){
                location.reload()
            }else{
                $('.error-label').removeClass('d-none')
                $('.error-label').text("**"+response.message);

                $('.btn-save-change-password').removeClass('d-none');
                $('.btn-save-change-password').addClass('d-block');
                $('.btn-save-change-password').prop('disabled', false);

                $('.change-password-loading-img').addClass('d-none')
                $('.change-password-loading-img').removeClass('d-block')

            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(thrownError);
        }
    });

    });

</script>
@endsection
@section('master-content')

<div class="d-flex" style="overflow-x:hidden;">


    <div id="overlay">
    </div>

    @yield('outside-content')

    @if (Session::get('user.data')->first_password != null)
        @include('templates.privacy_policy')
        @include('templates.terms_condition')

        <div class="d-flex" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:4;">
            <div style="position: fixed; top:0; left:0; right:0; bottom:0; background:rgba(0,0,0,0.5);"></div>
            <div class="m-auto p-4 p-md-5" style="z-index:5; background: white; border-radius:10px; max-width:90%;">
                <p class="error-label d-none text-danger mb-2"></p>
                <form class="change-password-form" action="{{ url('change_password') }}" method="POST">
                    @csrf
                    <p class="text-large font-semiBold mb-3 text-center">Make a new password</p>
                    <div class="input-container mb-3" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input type="password" name="old_password" placeholder="Old Password" class="d-inline form-control p-3" required>
                    </div>

                    <div class="input-container" style="border:0px;">
                        <img class="d-inline" src="{{ asset('images/ic_password.svg') }}" alt="">
                        <input type="password" name="password" placeholder="Password" class="d-inline form-control p-3" required    >
                    </div>
                    
                    <div class="d-flex">
                        <img class="change-password-loading-img mx-auto my-3 d-none" src="{{ asset('images/loading.gif') }}" width="50px" height="50px" alt="">
                    </div>

                    <input type="submit" class="btn btn-save-change-password muli-bolder d-block w-100 mt-4 d-block text-center mb-3" value="Save" style="border-radius: 20px" disabled>
                    <div class="d-flex flex-row">
                        <div class="btn-radio my-auto mr-2" style="height: 20px; width:20px; cursor: pointer;">
                            <div class="radio-outer d-flex p-1 m-auto" style="height: 20px; width:20px; background:white; border-radius:50%; box-shadow: 0 0 2px rgba(0,0,0,0.50)">
                                <div class="radio-inside m-auto" style="height: 8px; width:8px; background:white; border-radius:50%">
                                </div>
                            </div>
                        </div>
                        <p class="text-medium">I agree to <span class="text-primary btn-code-ethics" style="cursor: pointer">Code of Ethics Rule</span> and <span class="btn-terms-condition text-primary" style="cursor: pointer">Terms & Condition</span></p>
                        <input type="hidden" name="is_checked" value="false">
                    </div>
                </form>
            </div>
        </div>
    @endif
    
    <div id="sidebar" class="show">
        <div class="header px-4 px-md-5 py-4 py-md-5">
            <div class="row">
                <div class="col-12 mb-2">
                    <h3 class="username font-medium">{{ Session::get('user.data')->username }}</h3>
                    <h5 class="achievement">@if(Session::get('user.data')->badge != null) {{ Session::get('user.data')->badge->reward_title }} @else No Badge @endif</h5>
                </div>

                <div class="col-12">
                    <a class="btn-edit-profile text-center m-0 font-medium d-block py-1" href="{{ url('edit_profile') }}">
                        Edit Profile
                    </a>
                </div>
            </div>
        </div>

        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pt-3 pb-4 py-md-3 mt-md-3 {{ Request::segment(1)=='purchase_package' ? 'active' : '' }}" href="{{ url('purchase_package/buy') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    {{-- <svg fill = "#FFFFFF" href = ""></svg> --}}
                    <svg class="mr-3 my-auto" width="20px" height="20px" viewBox="0 0 26 26" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M6.91076 21.5333C7.967 21.5333 8.83235 22.417 8.83235 23.5088C8.83235 24.5875 7.967 25.4713 6.91076 25.4713C5.8418 25.4713 4.97645 24.5875 4.97645 23.5088C4.97645 22.417 5.8418 21.5333 6.91076 21.5333ZM21.2272 21.5333C22.2834 21.5333 23.1488 22.417 23.1488 23.5088C23.1488 24.5875 22.2834 25.4713 21.2272 25.4713C20.1582 25.4713 19.2929 24.5875 19.2929 23.5088C19.2929 22.417 20.1582 21.5333 21.2272 21.5333ZM1.12081 0.0111288L4.1559 0.477716C4.58857 0.556996 4.90671 0.919608 4.94489 1.3615L5.18668 4.27279C5.22486 4.68999 5.55573 5.00192 5.96295 5.00192H23.149C23.9253 5.00192 24.4343 5.27485 24.9434 5.8727C25.4524 6.47056 25.5415 7.32835 25.427 8.10686L24.218 16.6328C23.9889 18.2717 22.6146 19.4791 20.9984 19.4791H7.11463C5.42211 19.4791 4.02227 18.1547 3.88229 16.4391L2.71153 2.27128L0.78994 1.93336C0.280911 1.84238 -0.0754099 1.33551 0.0136702 0.815633C0.10275 0.284062 0.599054 -0.068152 1.12081 0.0111288ZM18.9623 9.80945H15.4373C14.9028 9.80945 14.4828 10.2383 14.4828 10.7842C14.4828 11.3171 14.9028 11.759 15.4373 11.759H18.9623C19.4968 11.759 19.9167 11.3171 19.9167 10.7842C19.9167 10.2383 19.4968 9.80945 18.9623 9.80945Z"
                            fill="black" />
                    </svg>
                    Purchase Package
                </p>
                @if (Request::segment(1)=='purchase_package')
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
                
            </div>
        </a>
        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 {{ Request::is('upload_data') ? 'active' : '' }}" href="{{ url('upload_data') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20px" height="20px" viewBox="0 0 26 26" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M21.4266 20.2735C22.1023 20.2735 22.6519 20.8302 22.6519 21.5147C22.6519 22.2006 22.1023 22.756 21.4266 22.756H14.5007C13.825 22.756 13.2754 22.2006 13.2754 21.5147C13.2754 20.8302 13.825 20.2735 14.5007 20.2735H21.4266ZM16.6235 1.77137L18.4126 3.19257C19.1462 3.76627 19.6353 4.5225 19.8026 5.31785C19.9957 6.19274 19.7898 7.05198 19.2106 7.79518L8.55339 21.5769C8.06429 22.2028 7.34352 22.5548 6.57126 22.5679L2.32383 22.62C2.09216 22.62 1.89909 22.4635 1.84761 22.2419L0.882285 18.0565C0.714962 17.2872 0.882285 16.4919 1.37138 15.8791L8.92665 6.10017C9.05536 5.94371 9.28703 5.91893 9.44149 6.03497L12.6206 8.56445C12.8266 8.73395 13.1097 8.82522 13.4057 8.78611C14.0364 8.70788 14.4612 8.13418 14.3968 7.52137C14.3582 7.20844 14.2037 6.94767 13.9978 6.7521C13.9335 6.69994 10.9088 4.27477 10.9088 4.27477C10.7157 4.11831 10.6771 3.83146 10.8316 3.63719L12.0286 2.08429C13.1355 0.663093 15.0661 0.532707 16.6235 1.77137Z"
                            fill="black" />
                    </svg>
                    Upload Data
                </p>
                @if (Request::is('upload_data'))
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
            </div>
        </a>

        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(1)=='network') active @endif" href="{{ url('network/network_tree') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20" height="20" viewBox="0 0 27 20" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M13.2803 13.0242C17.442 13.0242 20.9956 13.684 20.9956 16.3242C20.9956 18.9634 17.4648 19.6472 13.2803 19.6472C9.11854 19.6472 5.56498 18.9874 5.56498 16.3482C5.56498 13.708 9.09573 13.0242 13.2803 13.0242ZM19.9344 11.4344C21.5255 11.4049 23.236 11.6233 23.8681 11.7785C25.2072 12.0417 26.088 12.5792 26.4529 13.3602C26.7613 14.0014 26.7613 14.7453 26.4529 15.3854C25.8947 16.5969 24.0951 16.9857 23.3957 17.0862C23.2512 17.1081 23.135 16.9825 23.1502 16.8372C23.5075 13.4804 20.6653 11.8888 19.9301 11.5228C19.8986 11.5065 19.8921 11.4813 19.8953 11.466C19.8975 11.4551 19.9105 11.4376 19.9344 11.4344ZM6.75062 11.4348C6.77451 11.4381 6.78646 11.4556 6.78863 11.4654C6.79189 11.4818 6.78537 11.5058 6.75496 11.5233C6.01862 11.8892 3.17643 13.4808 3.53374 16.8365C3.54894 16.9829 3.43382 17.1074 3.28938 17.0867C2.58996 16.9862 0.790374 16.5973 0.232143 15.3859C-0.0773811 14.7447 -0.0773811 14.0018 0.232143 13.3606C0.597056 12.5796 1.47676 12.0421 2.81586 11.7778C3.44903 11.6238 5.15847 11.4053 6.75062 11.4348ZM13.2803 0.240479C16.1138 0.240479 18.3858 2.52352 18.3858 5.37458C18.3858 8.22456 16.1138 10.5098 13.2803 10.5098C10.4468 10.5098 8.17476 8.22456 8.17476 5.37458C8.17476 2.52352 10.4468 0.240479 13.2803 0.240479ZM20.2114 1.09667C22.9482 1.09667 25.0975 3.68666 24.3655 6.5716C23.8713 8.51382 22.0826 9.8039 20.0897 9.75146C19.8899 9.746 19.6933 9.72743 19.5032 9.69466C19.3653 9.67063 19.2958 9.51442 19.374 9.39863C20.1342 8.2735 20.5676 6.92006 20.5676 5.46722C20.5676 3.95102 20.0941 2.5375 19.2719 1.3785C19.2459 1.34245 19.2263 1.28674 19.2524 1.24523C19.2741 1.21137 19.3143 1.19389 19.3523 1.18515C19.6292 1.12835 19.9138 1.09667 20.2114 1.09667ZM6.47205 1.09656C6.76963 1.09656 7.05417 1.12824 7.3322 1.18504C7.36913 1.19378 7.4104 1.21235 7.43212 1.24512C7.4571 1.28663 7.43863 1.34234 7.41257 1.37839C6.59043 2.53739 6.11691 3.95091 6.11691 5.46711C6.11691 6.91995 6.55024 8.27339 7.31048 9.39852C7.38868 9.51431 7.31917 9.67052 7.18124 9.69455C6.99009 9.72842 6.79461 9.74589 6.59477 9.75136C4.60187 9.80379 2.81314 8.51371 2.31899 6.57149C1.58591 3.68656 3.7352 1.09656 6.47205 1.09656Z"
                            fill="black" />
                    </svg>
                    Network
                </p>
                @if (Request::segment(1)=='network')
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
            </div>
        </a>

        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(1)=='left_bonus') active @endif" href="{{ url('left_bonus/bonus') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M9.88239 1.33269C11.231 -0.0261548 13.4327 -0.0261547 14.7928 1.32232L15.6343 2.16252C15.9571 2.48523 16.3951 2.67079 16.8562 2.67079H18.0434C19.9569 2.67079 21.513 4.22556 21.513 6.13878V7.32705C21.513 7.78691 21.6975 8.22487 22.0202 8.54758L22.8502 9.37741C23.5072 10.0228 23.8645 10.8988 23.8645 11.8208C23.8761 12.7428 23.5187 13.6199 22.8732 14.2757L22.0202 15.1286C21.6975 15.4513 21.513 15.8893 21.513 16.3514V17.5374C21.513 19.4506 19.9569 21.0077 18.0434 21.0077H16.8562C16.3951 21.0077 15.9571 21.1909 15.6343 21.5137L14.8044 22.3435C14.1243 23.0246 13.2367 23.3577 12.3491 23.3577C11.4616 23.3577 10.574 23.0246 9.89391 22.3562L9.05245 21.5137C8.72969 21.1909 8.29167 21.0077 7.8306 21.0077H6.64332C4.72986 21.0077 3.17373 19.4506 3.17373 17.5374V16.3514C3.17373 15.8893 2.98929 15.4513 2.66654 15.1171L1.8366 14.2988C0.487956 12.9514 0.476429 10.7489 1.82508 9.39009L2.66654 8.54758C2.98929 8.22487 3.17373 7.78691 3.17373 7.31437V6.13878C3.17373 4.22556 4.72986 2.67079 6.64332 2.67079H7.8306C8.29167 2.67079 8.72969 2.48523 9.05245 2.16252L9.88239 1.33269ZM15.3116 13.7916C14.7467 13.7916 14.2972 14.2423 14.2972 14.7943C14.2972 15.3591 14.7467 15.8086 15.3116 15.8086C15.8649 15.8086 16.3144 15.3591 16.3144 14.7943C16.3144 14.2423 15.8649 13.7916 15.3116 13.7916ZM16.0147 8.15572C15.6228 7.76501 14.9888 7.76501 14.5969 8.15572L8.67206 14.0798C8.28015 14.4716 8.28015 15.1171 8.67206 15.5089C8.85649 15.7049 9.11008 15.8086 9.3752 15.8086C9.65185 15.8086 9.90544 15.7049 10.0899 15.5089L16.0147 9.58602C16.4066 9.19416 16.4066 8.54758 16.0147 8.15572ZM9.38673 7.86759C8.83344 7.86759 8.37236 8.31708 8.37236 8.8703C8.37236 9.43619 8.83344 9.88453 9.38673 9.88453C9.94002 9.88453 10.3896 9.43619 10.3896 8.8703C10.3896 8.31708 9.94002 7.86759 9.38673 7.86759Z"
                            fill="black" />
                    </svg>
                    Submine Bonus
                </p>
                @if (Request::segment(1)=='left_bonus')
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
            </div>
        </a>

        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(1)=='right_bonus') active @endif" href="{{ url('right_bonus/profit_sharing') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20" height="20" viewBox="0 0 26 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M18.6612 0.73291C23.0687 0.73291 25.6701 3.26912 25.6701 7.61057H20.2629V7.65488C17.7534 7.65488 15.7191 9.63828 15.7191 12.0849C15.7191 14.5316 17.7534 16.515 20.2629 16.515H25.6701V16.9137C25.6701 21.1998 23.0687 23.736 18.6612 23.736H7.11992C2.71242 23.736 0.111084 21.1998 0.111084 16.9137V7.5552C0.111084 3.26912 2.71242 0.73291 7.11992 0.73291H18.6612ZM24.7159 9.5155C25.2428 9.5155 25.6701 9.93201 25.6701 10.4458V13.6798C25.6639 14.1911 25.2403 14.6041 24.7159 14.6101H20.3651C19.0947 14.6267 17.9838 13.7787 17.6957 12.5722C17.5513 11.8233 17.7539 11.0505 18.2491 10.4609C18.7443 9.87135 19.4814 9.52529 20.2629 9.5155H24.7159ZM20.8763 11.0106H20.456C20.198 11.0077 19.9495 11.1056 19.7659 11.2824C19.5824 11.4593 19.4791 11.7004 19.4791 11.952C19.4791 12.4799 19.9147 12.9095 20.456 12.9156H20.8763C21.4159 12.9156 21.8532 12.4891 21.8532 11.9631C21.8532 11.4371 21.4159 11.0106 20.8763 11.0106ZM13.379 5.70565H6.16572C5.63059 5.70562 5.19502 6.12534 5.1888 6.64704C5.1888 7.17489 5.62435 7.60451 6.16572 7.61057H13.379C13.9186 7.61057 14.3559 7.18414 14.3559 6.65811C14.3559 6.13208 13.9186 5.70565 13.379 5.70565Z"
                            fill="black" />
                    </svg>
                    Profit Sharing
                </p>
                @if (Request::segment(1)=='right_bonus')
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
            </div>
        </a>

        <a class="d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 @if(Request::segment(1)=='withdraw_history') active @endif" href="{{ url('withdraw_history/left_pocket') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20" height="20" viewBox="0 0 24 24" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M11.7356 0.307617C18.1077 0.307617 23.2584 5.4698 23.2584 11.8303C23.2584 18.2024 18.1077 23.3531 11.7356 23.3531C5.37507 23.3531 0.212891 18.2024 0.212891 11.8303C0.212891 5.4698 5.37507 0.307617 11.7356 0.307617ZM11.3323 5.98832C10.8599 5.98832 10.4681 6.36857 10.4681 6.85253V12.6715C10.4681 12.9711 10.6294 13.2476 10.8945 13.409L15.4114 16.1053C15.5496 16.1859 15.6994 16.232 15.8608 16.232C16.1488 16.232 16.4369 16.0822 16.5982 15.8057C16.8402 15.4024 16.7134 14.8723 16.2986 14.6188L12.1965 12.176V6.85253C12.1965 6.36857 11.8048 5.98832 11.3323 5.98832Z"
                            fill="black" />
                    </svg>
                    Withdraw History
                </p>
                @if (Request::segment(1)=='withdraw_history')
                    <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p>
                @endif
            </div>
        </a>

        <a class="btn-logout d-block text-muted pl-4 pl-md-5 pr-4 pb-4 py-md-3 w-100" href="{{ url('logout') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex">
                    <svg class="mr-3 my-auto" width="20" height="20" viewBox="0 0 27 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.4829 0.0131836C15.4941 0.0131836 17.9499 2.42689 17.9499 5.39854V11.2084H10.5454C10.0148 11.2084 9.59517 11.6208 9.59517 12.1424C9.59517 12.6518 10.0148 13.0763 10.5454 13.0763H17.9499V18.8741C17.9499 21.8457 15.4941 24.2716 12.4582 24.2716H6.44828C3.4248 24.2716 0.968994 21.8579 0.968994 18.8862V5.41067C0.968994 2.42689 3.43714 0.0131836 6.46063 0.0131836H12.4829ZM21.0309 7.95805C21.3948 7.58204 21.9891 7.58204 22.353 7.94592L25.8947 11.4755C26.0767 11.6574 26.1737 11.8879 26.1737 12.1426C26.1737 12.3852 26.0767 12.6278 25.8947 12.7976L22.353 16.3272C22.1711 16.5091 21.9285 16.6062 21.698 16.6062C21.4554 16.6062 21.2129 16.5091 21.0309 16.3272C20.667 15.9633 20.667 15.369 21.0309 15.0051L22.9716 13.0766L17.9499 13.0763V11.2084L22.9716 11.2087L21.0309 9.28013C20.667 8.91625 20.667 8.32192 21.0309 7.95805Z" fill="#EC4B4B"/>
                    </svg>
                    Logout
                </p>
                {{-- <p class="circle-icon my-auto"><i class="fas fa-circle"></i></p> --}}
            </div>
        </a>
    </div>

    <div id="right-sidebar" class="px-4" style="right:0px; left:unset; width:250px;">
        <div class="mt-3">
            <button class="btn p-0" style="background: transparent" onclick="$('#right-sidebar').removeClass('show');$('#overlay').removeClass('show');">
                <i class="fas fa-times"></i>
            </button>
            <hr class="my-3">
        </div>
        
        <a class="d-block text-muted pt-3 pb-4 py-md-3 mt-md-3 {{ Request::segment(2)=='network_tree' ? 'active' : '' }}" href="{{ url('network/network_tree') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex font-medium">
                    Network Tree
                </p>
            </div>
        </a>
        <a class="d-block text-muted pb-4 py-md-3 {{ Request::segment(2)=='left_leg' ? 'active' : '' }}" href="{{ url('network/left_leg') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex font-medium">
                    Left Leg
                </p>
            </div>
        </a>
        <a class="d-block text-muted pb-4 py-md-3 {{ Request::segment(2)=='right_leg' ? 'active' : '' }}" href="{{ url('network/right_leg') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex font-medium">
                    Right Leg
                </p>
            </div>
        </a>
        <a class="d-block text-muted pb-4 py-md-3 {{ Request::segment(2)=='personal_sponsor' ? 'active' : '' }}" href="{{ url('network/personal_sponsor') }}">
            <div class="d-flex justify-content-between">
                <p class="my-auto d-flex font-medium">
                    Personal Sponsor
                </p>
            </div>
        </a>
    </div>

    <div id="page-content-wrapper" class="wrapped">
        <nav class="navbar navbar-expand-lg fixed-top navbar-light px-0 py-0 d-block">
            <div class="row h-100 d-flex">
                <div class="col-12 col-md-8 @if(Request::segment(1)=='network') col-lg-3 @else col-lg-6 @endif col-xl-5 w-100 d-flex my-3 my-lg-0" style="border-bottom:5px solid white; position:relative;">
                    <button class="btn my-auto p-0 mx-3 mx-lg-0 h-100" id="menu-toggle" onclick="onMenuClicked()" style="position: absolute">
                        <i class="fas fa-bars"></i>
                    </button>
                    <h4 class="title px-md-5 py-2 py-md-4 my-auto font-primary font-bold mb-0 mx-auto mx-lg-0">@yield('title')</h4>

                    <div class="right-sidebar-toggler h-100 d-none d-md-none" style="position: absolute; right:32px; top:0;">
                        <button class="my-auto btn p-0" style="height: 24px; width:24px;" onclick="rightSidebarToggler()">
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                    {{-- <div class="col-12">
                        <button>
                            <i class="fas fa-ellipsis-v"></i>
                        </button>
                    </div> --}}
                </div>

                    @if (Request::segment(1)=='purchase_package')
                            <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                                <div class="row h-100 mr-lg-4">
                                    <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='buy') #0075FF @else white @endif;">
                                        <a class="d-flex h-100" href="{{ url('purchase_package/buy') }}">
                                            <div class="my-auto mx-auto">
                                                <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='buy') text-primary @else text-muted @endif">Buy</p>
                                            </div>
                                        </a>   
                                    </div>
                                    
                                    <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='history') #0075FF @else white @endif;">
                                        <a class="d-flex h-100" href="{{ url('purchase_package/history') }}">
                                            <div class="my-auto mx-auto">
                                                <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='history') text-primary @else text-muted @endif">History</p>
                                            </div>
                                        </a>   
                                    </div>
                                </div>
                            </div>
                        @endif  
                
                    @if (Request::segment(1)=='left_bonus')
                    <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                        <div class="row h-100 mr-lg-4">
                            {{-- TEMPORARY TURNED OFF --}}

                            {{-- <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='reward') white @else #0075FF @endif;">
                                <a class="d-flex h-100" href="{{ url('left_bonus/bonus') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='reward') text-muted @else text-primary @endif">Bonus</p>
                                    </div>
                                </a>   
                            </div> --}}
                            
                            {{-- <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='reward') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('left_bonus/reward') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='reward') text-primary @else text-muted @endif">Reward</p>
                                    </div>
                                </a>   
                            </div> --}}

                            {{-- END --}}
                        </div>
                    </div>
                    @elseif(Request::segment(1)=='right_bonus')
                    <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                        <div class="row h-100 mr-lg-4">
                            {{-- TEMPORARY TURNED OFF --}}

                            {{-- <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='profit_sharing')  #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('right_bonus/profit_sharing') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='profit_sharing') text-primary @else text-muted @endif">Profit Sharing</p>
                                    </div>
                                </a>   
                            </div>
                            
                            <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='commision_sharing') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('right_bonus/commision_sharing') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='commision_sharing') text-primary @else text-muted @endif">Commision Sharing</p>
                                    </div>
                                </a>   
                            </div> --}}

                            {{-- END --}}
                        </div>
                    </div>
                    @elseif(Request::segment(1)=='withdraw_history')
                    <div class="col-12 col-md-4 col-lg-6 col-xl-5 w-100 ml-auto">
                        <div class="row h-100 mr-lg-4">
                            <div class="col-6 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='right_pocket') white @else #0075FF @endif;">
                                <a class="d-flex h-100" href="{{ url('withdraw_history/left_pocket') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0  text-medium @if(Request::segment(2)=='right_pocket') text-muted @else text-primary @endif">Submine Bonus</p>
                                    </div>
                                </a>   
                            </div>
                            
                            <div class="col-6 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='right_pocket') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('withdraw_history/right_pocket') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0  text-medium @if(Request::segment(2)=='right_pocket') text-primary @else text-muted @endif">Profit Sharing</p>
                                    </div>
                                </a>   
                            </div>
                        </div>
                    </div>
                    @elseif(Request::segment(1)=='network')
                    <div class="col-12 col-lg-9 col-xl-7 w-100 d-none d-md-block">
                        <div class="row h-100 mr-lg-4">

                            <div class="col-3 pr-0 pr-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='network_tree') #0075FF  @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('network/network_tree') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='network_tree') text-primary @else text-muted @endif">Network Tree</p>
                                    </div>
                                </a>   
                            </div>
                            
                            <div class="col-3 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='left_leg') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('network/left_leg') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='left_leg') text-primary @else text-muted @endif">Left Leg</p>
                                    </div>
                                </a>   
                            </div>

                            <div class="col-3 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='right_leg') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('network/right_leg') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='right_leg') text-primary @else text-muted @endif">Right Leg</p>
                                    </div>
                                </a>   
                            </div>

                            <div class="col-3 pl-0 pl-lg-3" style="position: relative; border-bottom:5px solid @if(Request::segment(2)=='personal_sponsor') #0075FF @else white @endif;">
                                <a class="d-flex h-100" href="{{ url('network/personal_sponsor') }}">
                                    <div class="my-auto mx-auto">
                                        <p class="my-auto text-center my-auto w-100 font-bold pb-3 pb-lg-0 text-medium @if(Request::segment(2)=='personal_sponsor') text-primary @else text-muted @endif">Personal Sponsor</p>
                                    </div>
                                </a>   
                            </div>

                        </div>
                    </div>
                    @endif
            </div>
        </nav>
        <div class="page-content px-3 px-md-5">
            @yield('content')
        </div>
    </div>

</div>
@endsection