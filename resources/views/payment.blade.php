@extends('layouts/master')

@section('css')

<style>
    @media (min-width: 1200px){
        .container {
            max-width: 900px !important;
        }
    }

    input{
        box-shadow: none !important;
        padding-left: 16px;
        padding-right: 16px;
        border:1px solid rgba(0,0,0,0.15) !important;
    }
    
    button{
        background: none;
    }

    button.btn-payment-method{
        padding-left: 1.25rem;
        padding-right: 1.25rem;
    }

    .payment-subview{
        padding-left: 1.25rem;
        padding-right: 1.25rem;
    }

    .rotateimg180 {
        -webkit-transform:rotate(-180deg);
        -moz-transform: rotate(-180deg);
        -ms-transform: rotate(-180deg);
        -o-transform: rotate(-180deg);
        transform: rotate(-180deg);
        transition: transform 150ms ease;
    }
</style>
    
@endsection

@section('master-content')
    <div class="3ds-container d-none" style="position: fixed; top:0; left:0; right:0; bottom:0;background:rgba(0,0,0,0.5); z-index:99">
        <iframe id="3ds-frame" name="3ds-frame" class="m-auto" src="" frameborder="0" width= "450px" height= "350px"  style="background:white; max-width: 90%"></iframe>
    </div>

    <div class="">
        <h4 class="text-center py-4 bg-primary font-semiBold text-white mb-0">UBERVEST CHECKOUT</h4>
    </div>

    <div class="text-center">
        <div class="my-5">
            <p class="font-semiBold mb-1">TOTAL AMOUNT: </p>
            <h1 class="font-regular text-primary">Rp{{ number_format($purchase_room->total, 0, ',', '.') }}</h1>
        </div>
    </div>
    
    <div class="container pb-5">
        @if(Session::has('response'))   
            <div class="col-12">
                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
            </div>
         @endif 
        <ul class="list-group">
            <li class="list-group-item py-0 px-0">
                <button class="btn btn-payment-method w-100">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="text-medium font-semiBold my-3">Virtual Account</p>
                        <i class="fas fa-chevron-down my-auto" style="font-size: 17px;"></i>
                    </div>
                </button>
                <div class="payment-subview d-none">
                    <div class="row px-2">
                        @for ($i = 0; $i < 5; $i++)
                        <div class="col-12 col-md-4 mb-3 px-2">
                            <a class="btn p-0 w-100" href="
                            @switch($i)
                                @case(0)
                                    {{ url('payment/'.$purchase_room->id.'/mandiri') }}
                                    @break
                                @case(1)
                                    {{ url('payment/'.$purchase_room->id.'/bri') }}
                                    @break
                                @case(2)
                                    {{ url('payment/'.$purchase_room->id.'/bni') }}
                                    @break
                                @case(3)
                                    {{ url('payment/'.$purchase_room->id.'/permata') }}
                                    @break
                                @case(4)
                                    {{ url('payment/'.$purchase_room->id.'/bca') }}
                                    @break
                                @default
                            @endswitch">
                                <div class="provider-container">
                                    <div class="provider-view d-flex w-100">
                                        @switch($i)
                                            @case(0)
                                                <img src="{{ asset('images/bank_icon/ic_mandiri.png') }}" alt="" class="m-auto">
                                                @break
                                            @case(1)
                                                <img src="{{ asset('images/bank_icon/ic_bri.png') }}" alt="" class="m-auto">
                                                @break
                                            @case(2)
                                                <img src="{{ asset('images/bank_icon/ic_bni.png') }}" alt="" class="m-auto">
                                                @break
                                            @case(3)
                                                <img src="{{ asset('images/bank_icon/ic_permata.png') }}" alt="" class="m-auto">
                                                @break
                                            @case(4)
                                                <img src="{{ asset('images/bank_icon/ic_bca.png') }}" alt="" class="m-auto">
                                                @break
                                            @default
                                        @endswitch
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endfor
                    </div>
                </div>
            </li>

            <li class="list-group-item py-0 px-0">
                <button class="btn btn-payment-method w-100">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="text-medium font-semiBold my-3">Credit Card</p>
                        <i class="fas fa-chevron-down my-auto" style="font-size: 17px;"></i>
                    </div>
                </button>
                <div class="payment-subview d-none">
                    {{-- <form action=""> --}}
                        <div class="mb-3">
                            <p class="mb-2">Card Number</p>
                            <input class="w-100" type="text" name="card_number" id="" placeholder="1234 5678 9101">
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-12 col-md-4 mb-3">
                                    <p class="mb-2">Valid Month</p>
                                    <input class="w-100" type="text" name="valid_month" id="" placeholder="1">
                                </div>

                                <div class="col-12 col-md-4 mb-3">
                                    <p class="mb-2">Valid Year</p>
                                    <input class="w-100" type="text" name="valid_year" id="" placeholder="2021">
                                </div>

                                <div class="col-12 col-md-4 mb-3">
                                    <p class="mb-2">CVV</p>
                                    <input class="w-100" type="text" name="cvv" id="" placeholder="123">
                                </div>
                            </div>
                        </div>
                        <div class="mb-3 d-flex">
                            <button class="btn btn-primary btn-paynow mx-auto px-5">Pay Now</button>
                        </div>
                    {{-- </form> --}}
                </div>
            </li>

            <li class="list-group-item py-0 px-0">
                <button class="btn btn-payment-method w-100">
                    <div class="d-flex w-100 justify-content-between">
                        <p class="text-medium font-semiBold my-3">E-Wallet</p>
                        <i class="fas fa-chevron-down my-auto" style="font-size: 17px;"></i>
                    </div>
                </button>
                <div class="payment-subview d-none">
                    <div class="row px-2">
                        @for ($i = 0; $i < 2; $i++)
                        <div class="col-12 col-md-4 mb-3 px-2">
                            <a class="btn p-0 w-100" href="
                            @switch($i)
                                @case(0)
                                    {{ url('payment/'.$purchase_room->id.'/ovo') }}
                                    @break
                                @case(1)
                                    {{ url('payment/'.$purchase_room->id.'/dana') }}
                                    @break
                                @default
                            @endswitch">
                                <div class="provider-container">
                                    <div class="provider-view d-flex w-100">
                                        @switch($i)
                                            @case(0)
                                                <img src="{{ asset('images/bank_icon/ic_ovo.png') }}" alt="" class="m-auto">
                                                @break
                                            @case(1)
                                                <img src="{{ asset('images/bank_icon/ic_dana.png') }}" alt="" class="m-auto">
                                                @break
                                            @default
                                        @endswitch
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endfor
                    </div>
                </div>
            </li>
        </ul>
    </div>
    
@endsection

@section('javascript')
    <script src="https://js.xendit.co/v1/xendit.min.js"></script>
    <script>        
        $('.btn-payment-method').click(function (e) { 
            $payment_subview = $(this).parent().find('.payment-subview');
            if ($payment_subview.hasClass('d-none')){
                $(this).find('.fa-chevron-down').addClass('rotateimg180')
                $payment_subview.removeClass('d-none');
            }else{
                $(this).find('.fa-chevron-down').removeClass('rotateimg180')
                $payment_subview.addClass('d-none');
            }
        });

        function validate(){
            if ($('input[name="valid_month"]').val() == ""){
                return false
            }

            if ($('input[name="valid_year"]').val() == ""){
                return false
            }

            if ($('input[name="cvv"]').val() == ""){
                return false
            }

            return true
        }

        $('.btn-paynow').click(function (e) {

            if (validate()){

                $('.3ds-container').addClass('d-flex');
                $('.3ds-container').removeClass('d-none');
                
                Xendit.setPublishableKey('xnd_public_development_aWcI2qXlJRHgJ5OEbWHfuYqeTZfTaoVYicCtao8DIFLAYPpyKrUsPkMT9PTBYA');
                
                $valid_month =  $('input[name="valid_month"]').val()
                if ($valid_month.length == 1){
                    $valid_month = "0" + $valid_month
                }

                var tokenData = {
                    amount: 1000,
                    card_number: $('input[name="card_number"]').val(),
                    card_exp_month: $valid_month,
                    card_exp_year: $('input[name="valid_year"]').val(),
                    card_cvn: $('input[name="cvv"]').val(),
                    is_multiple_use: false,
                    currency: "IDR",
                    should_authenticate: true
                };

                Xendit.card.createToken(tokenData, function (err, data) {

                    if (err) {
                        $('.3ds-container').addClass('d-none');
                        $('.3ds-container').removeClass('d-flex');
                        alert(err.message)
                    }else{

                        window.open(data.payer_authentication_url, '3ds-frame');  
                    
                        if (data.status === 'VERIFIED') {

                            $('.3ds-container').addClass('d-none');
                            $('.3ds-container').removeClass('d-flex');

                            $form = $('<form action="{{ url("payment/".Request::segment(2)."/credit_card") }}" method="POST" class="credit_card_form"></form>')
                            $form.append($('<input type="hidden" name="_token" value="{{ csrf_token() }}">'));
                            $form.append($('<input type="hidden" name="invoice_code" value="{{ $purchase_room->invoice_code }}">'));
                            $form.append($('<input type="hidden" name="amount" value="{{ $purchase_room->total }}">'));
                            $form.append($('<input type="hidden" name="token_id" value="'+ data.id +'">'));
                            $('body').append($form);
                            $form.submit()

                        } else if (data.status === 'IN_REVIEW') {
                            
                        } else if (data.status === 'FAILED') {
                            $('.3ds-container').addClass('d-flex');
                            $('.3ds-container').removeClass('d-none');
                        }
                    }
                });
            }else{
                alert("Please fill the blanks.")
            }
        });
        
        
        
    </script>
@endsection