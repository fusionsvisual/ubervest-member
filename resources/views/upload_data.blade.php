@extends('layouts/dashboard_master')

@section('title')
    UPLOAD DATA
@endsection

@section('dashboard-css')
    <style>
      @media only screen and (max-width: 768px) {
        .page-content{
            padding-top: 100px!important;
        }
      } 
    </style>
@endsection

@section('outside-content')
    <div class="popup d-none m-auto" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:999">
        <div class="popup-overlay" style="position: fixed; top:0; left:0; right:0; bottom:0; z-index:996; background: rgba(0,0,0,0.5)">
        </div>
        <div class="m-auto px-4 py-4" style="background: white; width:350px; max-width:90%; border-radius:10px; z-index:997">
            <p class="font-semiBold font-primary mb-3">Bukti alamat salah satu dari dokumen di bawah ini: </p>
            <div class="mb-4">
                <p class="font-primary">A. Bukti tagihan</p>
                <p class="ml-3">Tagihan PBB, koran air, listrik, internet, pulsa, dan tagihan kartu kredit penerbitan max 180 hari mundur.</p>
            </div>
            
            <div class="mb-4">
                <p class="font-primary">B. Bukti kartu keluarga cetak</p>
                <p class="ml-3">Maksimum 4 tahun mundur.</p>
            </div>

            <div>
                <p class="font-primary">C. Buku tabungan ada nama</p>
                <p class="ml-3">Alamat, dicetak maksimum 1 tahun mundur.</p>
            </div>
        </div>
    </div>
@endsection

@section('content')

    @if(Session::has('response'))   
        <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
    @endif    

    @if(Session::get('user.data')->identity_card  == null)
    <form action="" method="POST" enctype="multipart/form-data"> 
        @csrf
        <h4 class="font-medium text-large mb-4">Upload Your ID</h4>
        <p class="identity_card_uploading d-none">Uploading...</p>
        <div class="mb-4">
            <p class="d-inline-block mr-2 font-medium mb-3 mb-md-0">Upload your KTP/ SIM/ Passport:</p>
            <input type="file" name="identity_card" accept="image/png, image/jpeg">
        </div>
    
        <p class="proof_of_address_uploading d-none">Uploading...</p>
        <div class="mb-4">
            <p class="d-inline-block mr-2 font-medium mb-3 mb-md-0">Upload your Proof of Address:</p>
            <input type="file" name="proof_of_address" accept="image/png, image/jpeg" style="width:225px">
            <button type="button" class="btn btn-popup btn-secondary">
                <i class="fas fa-info-circle"></i>
            </button>
        </div>
    
        <div class="mb-4">
            <p class="d-inline-block mr-2 font-medium mb-3 mb-md-0">No. Handphone untuk Broker:</p>
            <input class="mb-2 px-2" type="text" name="tickmill_phone" style="height: 35px" required>
        </div>
    
        <button class="submit btn btn-primary" style="width:150px;" disabled>Save</button>
      </form>
    @else
    <div class="row">
        <div class="col-12 mb-5">
            <h5 class="mb-3">No. Telephone untuk Broker</h5>
            <input class="form-control" type="text" name="" value="081332221998" disabled>
        </div>
        <div class="col-12 col-md-6 mb-5 mb-md-0">
            <h5 class="mb-3 text-md-center">Identity Card</h5>
            <div style="position: relative; padding-top:50%; border-radius:10px; overflow:hidden;">
                <img class="w-100 h-100" src="{{ Network::ASSET_URL.Session::get('user.data')->identity_card }}" alt="" style="position: absolute; top:0; left:0; right:0; bottom:0; object-fit:cover;">
            </div>
        </div>
        <div class="col-12 col-md-6 mb-5">
            <h5 class="mb-3 text-md-center">Proof of Address</h5>
            <div style="position: relative; padding-top:50%;">
                <img class="w-100 h-100" src="{{ Network::ASSET_URL.Session::get('user.data')->proof_of_address }}" alt="" style="position: absolute; top:0; left:0; right:0; bottom:0; object-fit:cover;">
            </div>
        </div>
    </div>
    @endif

    
@endsection

@section('dashboard-javascript')
    <script>
        $identity_card_link = "";
        $proof_of_address_link = "";

        $("input[name='identity_card']").change(function (e) { 

            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('image', $(this)[0].files[0]);
            formData.append('type', 'identity_card');

            $('.identity_card_uploading').removeClass('d-none');

            uploadImage(formData);
        });

        $("input[name='proof_of_address']").change(function (e) { 
            var formData = new FormData();
            formData.append('_token', '{{ csrf_token() }}');
            formData.append('image', $(this)[0].files[0]);
            formData.append('type', 'proof_of_address');

            $('.proof_of_address_uploading').removeClass('d-none');

            uploadImage(formData);
        });

        $('form').submit(function (e) { 
            if ($identity_card_link == "" || $proof_of_address_link == ""){
                e.preventDefault();
                $('.message').removeClass('d-none');
                $('.message').text("Please fill the blanks!");
            }else{
                $(this).submit();
            }
        });

        function uploadImage(formData){
            $.ajax({
                type: "POST",
                url: "{{ url('upload_data/upload') }}",
                data: formData,
                enctype: 'multipart/form-data',
                dataType: "json",
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    console.log(response);
                    if (formData.get("type") == "identity_card"){
                        $identity_card_link = response.data
                        $('.identity_card_uploading').addClass('d-none');
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'identity_card_link',
                            value: $identity_card_link
                        }).appendTo('form');
                    }else{
                        $proof_of_address_link = response.data
                        $('.proof_of_address_uploading').addClass('d-none');
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'proof_of_address_link',
                            value: $proof_of_address_link
                        }).appendTo('form');
                    }
                    if ($identity_card_link != "" && $proof_of_address_link != ""){
                        $('button.submit').prop('disabled', false);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    console.log(xhr);
                    console.log(thrownError);
                }
            });
        }

        $(function () {
            $('[data-toggle="popover"]').popover()
        })

        $('.btn-popup').click(function (e) { 
            $('.popup').addClass('d-flex');
            $('.popup').removeClass('d-none');
        });

        $('.popup-overlay').click(function (e) { 
            $('.popup').removeClass('d-flex');
            $('.popup').addClass('d-none');
        });
    </script>
@endsection