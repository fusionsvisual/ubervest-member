@extends('layouts/dashboard_master')

@section('title')
NETWORK
@endsection

@section('dashboard-css')
<style>
    @media only screen and (max-width: 768px) {
        .page-content {
            padding-top: 100px !important;
        }

        .right-sidebar-toggler {
            display: block !important;
        }
    }
</style>
@endsection

@section('content')
<div class="row">
    
    <div class="col-12 col-md mb-3">
        <div class="d-flex flex-column flex-md-row">
            <a class="d-flex mb-3 mb-md-0" href="{{ url('network/right_leg') }}">
                <div class="btn btn-primary d-flex" style="height: 50px;">
                    <span class="d-flex my-auto">
                        <i class="fas fa-chevron-left" style="font-size:24px;"></i>
                        <p class="my-auto ml-2">Right Leg List</p>
                    </span>
                </div>
            </a>
            <input class="search-input form-control ml-md-3" type="text" placeholder="Search">
        </div>
    </div>

    <div class="col-12">
        <div class="table-responsive mb-2 mb-md-4">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col" style="width:10%">No</th>
                        <th scope="col" style="width:30%">Upline Name</th>
                        <th scope="col" style="width:20%">Upline Package</th>
                        <th scope="col" style="width:20%">Level</th>
                        <th scope="col" style="width:20%">Action</th>
                    </tr>
                </thead>
                <tbody class="item-list">
                    @foreach ($available_legs as $idx => $available_leg)
                        <tr>
                            <td scope="row">{{ $idx+1 }}</td>
                            <td>{{ $available_leg->upline->username }}</td>
                            <td>{{ $available_leg->upline->package->package_name }}</td>
                            <td>Level {{ $available_leg->level - (Session::get('user.data')->level - 1) }}</td>
                            <td>
                                <form action="{{ url('network/network_tree') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-primary">Add Member</button>
                                    <input type="hidden" name="upline_id" value="{{ $available_leg->upline->id }}">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="pagination-container">
            {{-- @include('templates.pagination', ["items" => $legs, "page" => $page]) --}}
        </div>
    </div>
</div>
@endsection

@section('dashboard-javascript')
    <script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
    <script>
        $url = "{{ url('network/right_leg/filter') }}";
        $token = "{{ csrf_token() }}";
        $totalCount = ""
        $level_query = "";

        $('.search-input').keyup(function (e) { 
            if ($(this).val() != ""){
              $isEmpty = false;
            }

            if (!$isEmpty){
                $search_query = $(this).val();
                $page = 1;
                filter();
            }

            if ($(this).val() == ""){
                $isEmpty = true;
            }
        });

        $('.level-select').change(function (e) { 
            $level_query = $(this).val();
            $page = 1;
            filter();            
        });
    </script>
@endsection