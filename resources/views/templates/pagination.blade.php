<ul class="pagination d-md-flex mb-5 d-none" style="height: 35px">
    <li class="page-item ml-auto mr-3 d-flex">
        <button class="btn btn-prev page-link d-flex my-auto" style="border-width:0px; border-radius: 50%;">
            <img class="m-auto" src="{{ asset('images/ic_chevron_left.svg') }}"></object>
        </button>
    </li>
    <li class="h-100 d-flex" style="border-radius:30px; background:white;">
        <div class="my-auto">
            <ul class="pagination px-3">
                
                @if(ceil($items->total/$items->per_page) > 4)
                    @if($page != $items->total)
                        @if($page < 4) <li class="page-item page-number-item page-number-item @if($page == 1) active @endif mx-1" data-page="1" >
                            <button class="btn btn-page page-link px-2 p-0" data-page="1" style="border:0px; border-radius:8px;">1</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == 2) active @endif" data-page="2">
        <button class="btn btn-page page-link px-2 p-0" data-page="2" style="border:0px; border-radius:8px;">2</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == 3) active @endif" data-page="3">
        <button class="btn btn-page page-link px-2 p-0" data-page="3" style="border:0px; border-radius:8px;">3</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == 4) active @endif" data-page="4">
        <button class="btn btn-page page-link px-2 p-0" data-page="4" style="border:0px; border-radius:8px;">4</button>
    </li>
    <li class="page-item disabled mx-1">
        <a class="page-link px-2 p-0" style="border:0px; border-radius:8px;">...</a>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ ceil($items->total/$items->per_page) }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page) }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page) }}</button>
    </li>
@elseif($page > ceil($items->total/$items->per_page) - 3)
    <li class="page-item page-number-item page-number-item mx-1" data-page="1">
        <button class="btn btn-page page-link px-2 p-0" data-page="1" style="border:0px; border-radius:8px;">1</button>
    </li>
    <li class="page-item disabled mx-1">
        <a class="page-link px-2 p-0" style="border:0px; border-radius:8px;">...</a>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == ceil($items->total/$items->per_page)-3) active @endif" data-page="{{ ceil($items->total/$items->per_page)-3 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page)-3 }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page)-3 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == ceil($items->total/$items->per_page)-2) active @endif" data-page="{{ ceil($items->total/$items->per_page)-2 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page)-2 }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page)-2 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == ceil($items->total/$items->per_page)-1) active @endif" data-page="{{ $items->total-1 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page)-1 }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page)-1 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 @if($page == ceil($items->total/$items->per_page)) active @endif"  data-page="{{ ceil($items->total/$items->per_page) }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page) }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page) }}</button>
    </li>
@else
    <li class="page-item page-number-item page-number-item @if($page == 1) active @endif mx-1" data-page="1">
        <button class="btn btn-page page-link px-2 p-0" data-page="1" style="border:0px; border-radius:8px;">1</button>
    </li>
    <li class="page-item disabled mx-1">
        <a class="page-link px-2 p-0" style="border:0px; border-radius:8px;">...</a>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ $page-1 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page-1 }}" style="border:0px; border-radius:8px;">{{ $page-1 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 active" data-page="{{ $page }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page }}" style="border:0px; border-radius:8px;">{{ $page }}</button>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ $page+1 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page+1 }}" style="border:0px; border-radius:8px;">{{ $page+1 }}</button>
    </li>
    <li class="page-item disabled mx-1">
        <a class="page-link px-2 p-0" style="border:0px; border-radius:8px;">...</a>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ ceil($items->total/$items->per_page) }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page) }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page) }}</button>
    </li>
    @endif
@else
    <li class="page-item page-number-item page-number-item @if($page == 1) active @endif mx-1" data-page="1">
        <button class="btn btn-page page-link px-2 p-0" data-page="1" style="border:0px; border-radius:8px;">1</button>
    </li>
    <li class="page-item disabled mx-1">
        <a class="page-link px-2 p-0" style="border:0px; border-radius:8px;">...</a>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ $page-3 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page-3 }}" style="border:0px; border-radius:8px;">{{ $page-3 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 " data-page="{{ $page-2 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page-2 }}" style="border:0px; border-radius:8px;">{{ $page-2 }}</button>
    </li>
    <li class="page-item page-number-item mx-1" data-page="{{ $page-1 }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ $page-1 }}" style="border:0px; border-radius:8px;">{{ $page-1 }}</button>
    </li>
    <li class="page-item page-number-item mx-1 active" data-page="{{ ceil($items->total/$items->per_page) }}">
        <button class="btn btn-page page-link px-2 p-0" data-page="{{ ceil($items->total/$items->per_page) }}" style="border:0px; border-radius:8px;">{{ ceil($items->total/$items->per_page) }}</button>
    </li>
    @endif

@else
    @if($items->total == 0)
        <li class="page-item page-number-item active mx-1" data-page="1">
            <button class="btn btn-page page-link px-2 p-0" data-page="1" style="border:0px; border-radius:8px;">1</button>
        </li>
    @else
        @for($i = 1; $i <= ceil($items->total/$items->per_page); $i++)
            <li class="page-item page-number-item @if($i == $page) active @endif mx-1" data-page="{{ $i }}">
                <button class="btn btn-page page-link px-2 p-0" data-page="{{ $i }}" style="border:0px; border-radius:8px;">{{ $i }}</button>
            </li>
        @endfor
    @endif

    @endif
</ul>
<li class="page-item ml-3 mr-auto mr-md-0 d-flex">
    <button class="btn btn-next page-link d-flex my-auto" style="border-width:0px; border-radius: 50%;">
        <img class="m-auto" src="{{ asset('images/ic_chevron_right.svg') }}"></object>
    </button>
</li>
</ul>

<ul class="pagination d-flex d-md-none mb-5" style="height: 35px">
    <li class="page-item ml-auto mr-3 d-flex">
        <button class="btn btn-prev page-link d-flex my-auto" style="border-width:0px; border-radius: 50%;">
            <img class="m-auto" src="{{ asset('images/ic_chevron_left.svg') }}"></object>
        </button>
    </li>
    <li class="h-100 d-flex" style="border-radius:30px; background:white;">
        <div class="my-auto">
            <ul class="pagination px-3">
                <li class="page-item page-number-item page-number-item mx-1">
                    <p class="btn btn-page px-2 p-0" style="border:0px; border-radius:8px;">
                        {{ $page }}</p>
            </ul>
        </div>
    </li>

    <li class="page-item ml-3 mr-auto mr-md-0 d-flex">
        <button class="btn btn-next page-link d-flex my-auto" style="border-width:0px; border-radius: 50%;">
            <img class="m-auto" src="{{ asset('images/ic_chevron_right.svg') }}"></object>
        </button>
    </li>
</ul>
