<div class="terms_condition-popup d-none flex-column m-auto" style="position:fixed; top:0; left:0; right:0; bottom:0; background:white; z-index:5; max-width:90%; max-height:90%; border-radius: 6px; overflow:hidden;">
    <div style="border-bottom: 2px solid rgba(0, 0, 0, 0.25);">
        <h2 class="px-5 py-3">TERMS AND CONDITION</h2>
    </div>
    <div class="privacy-policy h-100 px-2 px-md-5 py-4" style="overflow: scroll;">
        <p class="mb-3">Dengan ini memberikan persetujuan kepada PT.Untung Bersama Investindo yang juga dikenal dengan sebutan lain yakni Ubervest, untuk hal-hal seperti disebutkan dibawah ini:</p>
        <ol class="mb-5">
            <li>Memberikan kewenangan kepada Ubervest untuk mendaftarkan dan membuat akun trading derivatif forex di broker yang dikehendaki oleh klien dan atau Ubervest. Termasuk mengisi segala informasi yang diperlukan pada saat pendaftaran akun di broker tersebut.</li>
            <li>Memberikan kewenangan kepada Ubervest untuk membuat alamat email baru atas nama klien yang akan digunakan pada proses pendaftaran akun trading di broker.</li>
            <li>Memberikan kewenangan kepada Ubervest untuk mengelola sepenuhnya akun yang sudah terdaftar di broker, termasuk namun tidak terbatas pada mendaftarkan IB (introductory Broker), melakukan proses withdrawal (penarikan atas nama kllien), dan sebagainya.</li>
            <li>Melepaskan hak untuk menyimpan informasi login baik username maupun password akun klien area broker sekaligus memberikan hak kepada Ubervest untuk menggunakan username dan password untuk keperluan login ke akun tersebut.</li>
            <li>Memberikan kewenangan penuh kepada Ubervest untuk melakukan aktifitas trading dan mengambil keputusan trading pada akun trading yang sudah aktif di broker yang telah ditunjuk.</li>
            <li>Mengikuti segala ketentuan dan perubahan-perubahan yang ditentukan oleh Ubervest, namun tidak termasuk di dalamnya hak klien untuk melakukan penarikan uang, memberhentikan aktifitas trading dan menutup akun trading di broker dengan sebelumnya menyelesaikan proses administrasi sesuai ketentuan Ubervest.</li>
        </ol>
        <p class="mb-3">Bersama dengan ini pula menyatakan bahwa:</p>
        <ol>
            <li>Klien telah mengerti akan segala resiko yang mungkin terjadi dalam trading derivatif forex, termasuk resiko atas kerugian yang mungkin timbul.</li>
            <li>Ubervest tidak menanggung resiko atas kerugian apapun yang terjadi dan timbul baik yang disebabkan dari aktifitas trading maupun oleh sebab-sebab lain di dalam dan di luar Ubervest atau yang biasanya dikenal dengan istilah force majeur dan menyadari bahwa resiko kerugian akibat force majeur tersebut menjadi tanggungan klien sepenuhnya.</li>
            <li>Klien telah memahami bahwa Ubervest tidak melakukan penghimpunan dana untuk trading, melainkan penyetoran dana adalah langsung dari pihak klien ke pihak broker dengan atau tanpa perantara pihak ketiga.</li>
            <li>Klien telah mengerti bahwa return/profit/hasil yang diinformasikan pada saat presentasi adalah berupa estimasi dan bukan suatu kepastian.</li>
            <li>Klien telah memahami seluruh ketentuan dan informasi yang berkaitan dengan produk dan fasilitas yang disediakan oleh Ubervest.</li>
            <li>Klien telah memahami akan perihal adanya pembagian keuntungan, dan biaya-biaya lainnya yang mungkin timbul dengan menggunakan produk dan jasa Ubervest.</li>
            <li>Saya memberikan pernyataan yang benar dan sesungguhnya yakni uang yang digunakan untuk melakukan aktifitas trading dengan menggunakan jasa Ubervest adalah bukan dari hasil perbuatan melawan hukum dan sebagai bentuk pencucian uang.</li>
            <li>Dengan menandatangani / menyetujui dokumen ini secara elektronik, klien menyatakan telah memberikan persetujuan dan pernyataan yang sah dan mengikat secara hukum sebagaimana apabila dilakukan pembubuhan tanda tangan diatas materai senilai Rp.6000,- seperti pada umumnya</li>
        </ol>
    </div>
    <div class="w-100" style="background:white; height:100px;"></div>
    <div class="" style="position: absolute; left:0; right:0; bottom:0; background:white; height:100px; border-top: 2px solid rgba(0, 0, 0, 0.25)">
        <div class="d-flex p-4">
            <button class="btn btn-terms-condition-close btn-primary mx-auto">Saya Mengerti</button>
        </div>
    </div>
</div>