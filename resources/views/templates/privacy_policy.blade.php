<div class="privacy-policy-popup d-none flex-column m-auto" style="position:fixed; top:0; left:0; right:0; bottom:0; background:white; z-index:5; max-width:90%; max-height:90%; border-radius: 6px; overflow:hidden;">
    <div style="border-bottom: 2px solid rgba(0, 0, 0, 0.25);">
        <h2 class="px-5 py-3">PERATURAN DAN KODE ETIK UBERVEST</h2>
    </div>
    <div class="privacy-policy h-100 px-2 px-md-5 py-4" style="overflow: scroll;">
            <ol>
                <li class="mb-5 no-number">
                    <h4>BAB I. PENDAHULUAN</h4>
                    <ol>
                        <li>
                            <p>Peraturan  dan Kode Etik Perusahaan ini dibuat sebagai peraturan dan pedoman bagi semua pihak yang terlibat dalam  bisnis Untung Bersama Investindo (UBERVEST) dengan tujuan dan maksud  sebagai berikut:</p>
                            <ol>
                                <li>Menjelaskan  dan  mengatur  hubungan  antara Perusahaan dengan Mitra Usaha Untung Bersama Investindo (Ubervest).</li>
                                <li>Menjaga dan melindungi kepentingan Perusahaan dan Mitra Usaha Ubervest.</li>
                                <li>Menjelaskan  mengenai  peraturan dan etika yang harus ditaati dalam  menjalankan  bisnis Untung Bersama Investindo.</li>
                                <li>Menyatakan dan  mengatur hak, kewajiban, tugas dan tanggung jawab Mitra Usaha Untung Bersama Investindo dalam  menjalankan bisnis Ubervest.</li>
                                <li>Mengatur dan melindungi kepentingan hukum Perusahaan dan Mitra Usaha Untung Bersama Investindo, dari tindakan Mitra Usaha Untung Bersama Investindo yang dapat menyebabkan dan atau berpotensi menyebabkan  timbulnya kerugian terhadap Perusahaan dan/atau terhadap Mitra Usaha Untung Bersama Investindo lain. Semua  Mitra Usaha Untung Bersama Investindo secara otomatis terikat dengan  segala ketentuan yang ada dalam kode etik dan peraturan perusahaan Untung Bersama Investindo. Selanjutnya Kode Etik dan Peraturan Perusahaan ini akan digunakan sebagai peraturan dan pedoman  MitraUsaha Untung Bersama Investindo dalam  menjalankan  bisnis Untung Bersama Investindo.</li>
                            </ol>
                        </li>
                        
                        <li>
                            <p>Untuk memberikan pengertian  dalam  memahami Peraturan dan Kode Etik Untung Bersama Investindo, perlu diberikan penjelasan tentang  istilah yang dipergunakan  sebagai  berikut:</p>
                            <ol>
                                <li>Perusahaan Untung Bersama Investindo disebut UBERVEST.</li>
                                <li>Mitra usaha UBERVEST adalah individu yang telah mendaftarkan keanggotaannya dengan cara menyerahkan formulir pendaftaran yang sah dan telah disetujui oleh perusahaan.</li>
                                <li>Upline adalah Mitra Usaha UBERVEST yang berada pada satu garis lurus ke atas.</li>
                                <li>Downline adalah Mitra Usaha UBERVEST yang berada pada satu garis lurus ke bawah.</li>
                                <li>Sponsor adalah upline yang memperkenalkan bisnis UBERVEST kepada calon Mitra Usaha UBERVEST, dan menempatkan  calon  Mitra Usaha UBERVEST tersebut sebagai downlinenya.</li>
                                <li>Konsumen adalah pembeli dari  produk-produk UBERVEST dengan tujuan  untuk dipakai sendiri.</li>
                                <li>Crossline adalah  mitra usaha UBERVEST yang berbeda garis sponsor.</li>
                                <li>Jaringan adalah  beberapa mitra usaha UBERVEST  yang berada dalam kelompok anggota yang bersangkutan.</li>
                                <li>Username dan Password adalah tanda pengenal yang diberikan oleh perusahaan kepada Mitra Usaha UBERVEST sebagai bukti keanggotaan dan dipakai sebagai alat untuk berhubungan dengan Perusahaan, Mitra Usaha UBERVEST serta konsumen dalam menjalankan  aktivitas bisnis UBERVEST.</li>
                                <li>Produk adalah barang atau jasa yang dipasarkan oleh  perusahaan  kepada para konsumen dan/atau Mitra usaha UBERVEST.</li>
                            </ol>
                        </li>
                    </ol>
                </li>
                
                <li class="mb-5 no-number">
                    <h4>BAB II. PENDAFTARAN MENJADI MITRA USAHA UNTUNG BERSAMA INVESTINDO (UBERVEST)</h4>
                    <ol>
                        <li>Setiap calon Mitra Usaha UBERVEST telah berusia minimal 17 tahun dengan kewarganegaraan yang jelas yang dibuktikan dengan data-data dan/atau bukti identitas diri (KTP) yang masih berlaku dan diakui keabsahannya. Untuk menjadi Mitra Usaha UBERVEST, seorang pemohon diwajibkan mengisi, melengkapi dan menandatangani formulir pendaftaran secara jujur dan benar, serta berjanji dan mengikatkan diri untuk tunduk pada semua syarat-syarat, peraturan sebagai Mitra Usaha UBERVEST,serta kode etik program pemasaran dan kebijakan-kebijakan dan peraturan-peraturan lainnya yang ditetapkan oleh Perusahaan baik secara tertulis atau secara tidak tertulis baik yang telah ada sekarang dan/atau tidak terbatas terhadap perubahan-perubahan, modifikasi dan/atau variasi. Formulir pendaftaran yang telah diisi dan ditanda tangani oleh calon Mitra Usaha UBERVEST harus dikembalikan ke UBERVEST dan harus mendapat persetujuan dari Perusahaan. Perusahaan memiliki kewenangan penuh untuk menyetujui atau  menolak keanggotaan seseorang untuk menjadi Mitra Usaha UBERVEST.
                        </li>
                        
                        <li>Seorang calon Mitra Usaha UBERVEST dilarang memberikan keterangan atau informasi yang tidak benar atau tidak akurat kepada perusahaan. Seorang Mitra Usaha UBERVEST berkewajiban mengisi/memasukkan data keanggotannya dengan benar pada Pihak Perusahaan. Apabila terdapat perubahan atau kesalahan dalam  mengisi/memasukkan data di kemudian hari  UBERVEST berhak untuk menarik biaya administrasi atas perubahan data tersebut.
                        </li>
    
                        <li>Perusahaan mempunyai  hak untuk segera menghentikan status keanggotaan sebagai Mitra Usaha UBERVEST apabila yang bersangkutan telah secara sengaja memberikan informasi yang salah atau tidak akurat. Segala kerugian yang timbul terhadap Mitra Usaha UBERVEST itu sendiri maupun terhadap Perusahaan ataupun Pihak-pihak lainnya yang disebabkan karena kesalahan, kelalaian dan/atau kesengajaan dalam  mengisi dan memberikan data kenggotaan menjadi tanggung jawab Mitra Usaha UBERVEST itu sendiri secara pribadi.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB III. MASA KEANGGOTAAN SEUMUR HIDUP DAN BERSYARAT</h4>
                    <p>Seorang Mitra Usaha PT. Untung Bersama Investindo dapat diberhentikan keanggotaannya sebagai Mitra Usaha UBERVEST oleh perusahaan secara sepihak apabila yang bersangkutan secara langsung atau tidak langsung terlibat dalam tindakan seperti:</p>
                    <ol>
                        <li>Mensponsori  Mitra Usaha UBERVEST lain secara tidak benar, yaitu tidak mematuhi ketentuan-ketentuan yang tercantum dalam Peraturan dan Kode Etik ini.</li>
                        <li>Merubah harga jual produk UBERVEST tanpa mendapat persetujuan lebih dahulu dari perusahaan.</li>
                        <li>Melanggar dan/atau tidak mematuhi kode etik sebagai Mitra Usaha UBERVEST serta program pemasaran  atau kebijakan  lainnya  yang telah ditetapkan oleh UBERVEST</li>
                        <li>Melakukan kegiatan yang secara langsung  atau  tidak langsung  akan membawa pengaruh negatif kepada produk, program  pemasaran, maupun  perusahaan UBERVEST.</li>
                        <li>Menyampaikan  informasi yang  salah  tentang  produk UBERVEST dan  tata cara pemasaran  perusahaan.</li>
                        <li>Melakukan tindakan atau perbuatan yang bertentangan dengan Hukum yang berlaku yang secara langsung  atau  tidak langsung akan membawa pengaruh negatif kepada produk maupun UBERVEST</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB IV. BIAYA PENDAFTARAN DAN KETENTUAN MENJADI MITRA USAHA UNTUNG BERSAMA INVESTINDO</h4>
                    <ol>
                        <li>Biaya pendaftaran keanggotaan sebagai Mitra Usaha UBERVEST ditetapkan oleh perusahaan dan dapat berubah / mengalami penyesuaian di kemudian hari. Member yang sudah melunasi tidak akan dikenakan selisih daripada penyesuaian harga tersebut.</li>
                        <li>Besaran biaya dapat berubah  setiap waktu. Perubahan atas biaya pendaftaran tersebut akan diumumkan dan berlaku efektif setelah pengumuman atau pemberitahuan resmi dikeluarkan dari UBERVEST.</li>
                        <li>Mitra Usaha UBERVEST akan  mendapatkan Panduan Bisnis (Staterkit) serta Paket Produk sesuai dengan yang dipesan secara elektronik.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB V. AHLI WARIS DAN PENGHIBAHAN KEANGGOTAAN</h4>
                    <ol>
                        <li>Seorang Mitra Usaha UBERVEST dapat menunjuk siapa saja yang mempunyai kewarganegaraan yang sama menjadi ahli warisnya. Jika nama ahli waris tidak tercantum dalam formulir permohonan  Keanggotaan  maka yang  menjadi ahli warisnya adalah saudara yang terdekat. Apabila terjadi perselisihan tentang siapa yang menjadi ahli waris yang sah setelah seorang Mitra Usaha UBERVEST meninggal dunia, Perusahaan hanya mengakui ahli waris berdasarkan  Keputusan atau penetapan  lembaga  peradilan  sesuai dengan kewarganegaraan  yang  bersangkutan. UBERVEST berhak menahan/membekukan keuntungan apa saja termasuk bonus sampai diperoleh keputusan  pengadilan  yang  berkekuatan hukum.</li>
                        <li>Seorang  Mitra Usaha UBERVEST dilarang  mengalihkan, menghibahkan atau mengalihkan hak lainnya sebagai seorang Mitra Usaha UBERVEST kepada orang lain tanpa  persetujuan tertulis dari UBERVEST. Seorang Mitra Usaha UBERVEST dapat mendelegasikan tanggung jawabnya tetapi  tetap bertanggung jawab untuk memastikan semua tindakan yang diambil sesuai dengan ketentuan perundang-undangan yang berlaku.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB VI. PENGALIHAN KEANGGOTAAN</h4>
                    <p>Pengalihan keanggotaan dapat dibagi  menjadi 2 kategori:</p>
                    <ol>
                        <li>Jika seorang Mitra Usaha UBERVEST meninggal dunia, maka ahliwarisnya secara otomatis akan mengambil alih keanggotaannya. Jika nama ahliwaris tidak disebutkan, pengalihan dimaksud akan ditentukan berdasarkan Putusan atau Penetapan Lembaga Peradilan yang berlaku di Negara setempat. Secara bersamaan, UBERVEST berhak menahan/membekukan seluruh keuntungan termasuk tetapi tidak terbatas pada bonus, komisi sampai kasusnya diputuskan.</li>
                        <li>Seorang Mitra Usaha UBERVEST yang telah mencapai usia 65 tahun atau tidak mampu menjalankan bisnis UBERVEST karena kondisi kesehatannya tidak memungkinkan, jika disetujui oleh UBERVEST maka Mitra Usaha UBERVEST yang bersangkutan diijinkan mengalihkan keanggotannya kepada siapa saja yang dia inginkan menjadi ahli warisnya.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB VII. PENDAFTARAN GANDA  ATAU LEBIH & PERUBAHAN DATA/EDIT PROFILE</h4>
                    <ol>
                        <li>Setiap calon Mitra Usaha jika ingin mendaftar diwajibkan membeli salah satu produk dari UBERVEST dan menjadi syarat utama setelah itu pihak UBERVEST akan memberikan akses ke dashboar member.</li>
                        <li>Adapun apabila mitra usaha berkeinginan menambah ID lagi maka kewajiban berlaku seperti semula yakni melakukan pembelian produk sesuai yang dipesan. Selanjutnya perusahaan akan memberikan akses atas ID yang ke 2.</li>
                        <li>Seseorang Mitra Usaha UBERVEST yang hendak melakukan perubahan data/edit profile keatas nama mitra lain, harus mengajukan permohonan kepada UBERVEST dan persetujuan dari yang bersangkutan dengan membayar biaya perubahan data yang ditetapkan.</li>
                        <li>Seorang Mitra UBERVEST dilarang terlibat secara langsung atau  tidak langsung untuk meyakinkan, mempengaruhi atau membantu Mitra Usaha UBERVEST lain agar megalihkan statusnya sebagai Mitra Usaha UBERVEST di jaringannya atau  mengalihkan kepada sponsor lain. Dalam hal ini termasuk tindakan  menawarkan  bantuan  keuangan atau insentif  yang terlihat maupun  tidak terlihat atau  memberikan bagian keuntungan untuk mempengaruhi seorang Mitra Usaha UBERVEST agar membatalkan statusnya sebagai Mitra Usaha UBERVEST dan kemudian mengajukan permohonan  ulang  menjadi  Mitra Usaha UBERVEST di bawah sponsor yang berbeda. Seorang Mitra Usaha UBERVEST yang ditemukan terlibat praktek dimaksud dapa tmengakibatkan statusnya sebagai Mitra Usaha UBERVEST akan dibekukan atau dibatalkan.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB VIII. PENSPONSORAN YANG TIDAK SAH DAN SANKSINYA</h4>
                    <ol>
                        <li>Pensponsoran yang tidak sah dalam konteks berikut  adalah  mensponsori seorang Mitra Usaha UBERVEST yang sudah  menjadi  Mitra Usaha UBERVEST pada jaringan/group lain kecuali atas persetujuan dan atas kemauan mitra yang bersangkutan.</li>
                        <li>Perusahaan berhak untuk mengambil tindakan terhadap Mitra Usaha UBERVEST yang melakukan pensponsoran  yang tidak sah, sebagai berikut:
                            <ol>
                                <li>Status sebagai Mitra Usaha UBERVEST tersebut akan dicabut atau dibatalkan, baik yang mensponsori atau yang disponsori</li>
                                <li>Mengeluarkan surat teguran dan/atau menjatuhkan sanksi kepada semua pihak yang terlibat dan tidak membayarkan bonus yang telah muncul.</li>
                            </ol>
                        </li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB IX. PEMBELIAN BARANG DAN HARGA</h4>
                    <ol>
                        <li>Pembelian produk dilakukan secara tunai atau pembayaran dengan cara lain sesuai yang ditentukan Perusahaan.</li>
                        <li>Seorang  Mitra Usaha UBERVEST berhak mendapatkan  harga yang sama yaitu sesuai dengan harga anggota yang telah diberlakukan oleh perusahaan untuk masing-masing wilayah.</li>
                        <li>Harga anggota yang  tercantum  dalam  daftar  harga yang  dikeluarkan  perusahaan  belum/sudah termasuk Pajak Pertambahan Nilai dimana informasi akan dicantumkan di price list yang tertera.</li>
                        <li>Mitra Usaha UBERVEST yang telah melakukan pembelian produk, tidak dapat mengembalikan  atau  menukar  dengan barang yang lain.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB IX. PERHITUNGAN, PENGIRIMAN BONUS</h4>
                    <ol>
                        <li>Perhitungan Bonus Sponsor adalah sesuai dengan marketing plan termutakhir dari perusahaan yang diakui secara sah oleh perusahaan.</li>
                        <li>Pembayaran Bonus akan dibayar maksimal 1 x 24 jam setelah permintaan withdrawal. Pada saat pengisian data menjadi Mitra Usaha UBERVEST segala  biaya  yang dibebankan  oleh bank terhadap pengiriman bonus menjadi beban dan tanggung jawab Mitra Usaha UBERVEST yang bersangkutan.</li>
                        <li>Mitra Usaha UBERVEST yang mencapai promo atau reward  tidak dapat diganti sesuai dengan janji perusahaan.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XI. PENUNDAAN & PEMBATALAN BONUS</h4>
                    <ol>
                        <li>UBERVEST mempunyai hak penuh dan mutlak setiap saat untuk menahan atau membatalkan keuntungan yang diterima seorang Mitra Usaha PT Untung Bersama Investindo termasuk atas bonus dan hak-hak lainnya dalam  hal:</li>
                        <li>Kepada seorang Mitra Usaha UBERVEST yang telah dikeluarkan surat bukti pelanggaran karena melanggar peraturan yang telah ditetapkan UBERVEST, termasuk kode etik, program pemasaran (system Marketing Plan) atau kebijakan perusahaan lainnya maka UBERVEST berhak menunda atau membatalkan bonus member UBERVEST</li>
                        <li>Seorang Mitra Usaha UBERVEST yang sedang dalam  penyelidikan  yang dilakukan UBERVEST atas dugaan pelanggaran terhadap peraturan Keanggotaan, kode etik, program pemasaran (system Marketing Plan) atau kebijakan perusahaan lainnya.</li>
                        <li>Seorang  Mitra Usaha UBERVEST yang telah terbukti bertanggung  jawab atas pelanggaran peraturan Keanggotaan,  kode etik, program pemasaran (system Marketing Plan) atau kebijakan perusahaan lainnya.</li>
                        <li>Penundaan penyelesaian pemindahan hak Mitra Usaha UBERVEST kepada ahli waris.</li>
                        <li>Alasan/sebab lainnya yang dianggap perlu dan baik oleh UBERVEST.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XII. HAK MITRA USAHA PT. UNTUNG BERSAMA INVESTINDO</h4>
                    <ol>
                        <li>Mitra Usaha UBERVEST berhak mendapatkan Nomor ID sebagai Login di Virtual Office</li>
                        <li>Mitra Usaha UBERVEST berhak memasarkan produk UBERVEST dan mensponsori siapa saja, dimana saja sepanjang tidak melanggar kode etik dan peraturan yang telah ditetapkan perusahaan.</li>
                        <li>Setiap Mitra Usaha UBERVEST akan mendapatkan bonus sesuai dengan ketentuan Program Pemasaran (Marketing Plan) yang berlaku.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XIII. LARANGAN & TANGGUNG JAWAB MITRA USAHA PT.  UNTUNG BERSAMA INVESTINDO</h4>
                    <ol>
                        <li>Seorang Mitra Usaha UBERVEST adalah mandiri dan bebas untuk menjalankan usahanya, Oleh karena itu seorang Mitra Usaha UBERVEST tidak boleh mengklaim atau bertindak sebagai seorang karyawan seolah-olah  mempunyai hubungan kerja secara struktural dengan UBERVEST. Tindakan keras akan dilakukan  kepada siapa saja yang melanggar aturan ini.</li>
                        <li>Seorang  Mitra Usaha UBERVEST dilarang  menyampaikan penjelasan atau penawaran program pemasaran (system Marketing Plan) atau yang berhubungan dengan produk dan layanan yang tidak sesuai dengan peraturan yang ditetapkan oleh UBERVEST.</li>
                        <li>Seorang Mitra Usaha UBERVEST dilarang menyampaikan  informasi yang tidak masuk akal, salah  arah atau  tidak  mewakili tentang keuntungan yang dapat diperoleh, dan dilarang meyampaikan bahwa UBERVEST dapat menjamin penghasilan.</li>
                        <li>Seorang Mitra Usaha UBERVEST tidak diperbolehkan meminta atau mempengaruhi Mitra Usaha UBERVEST lain untuk menjual atau  membeli  produk atau layanan yang diluar yang ditentukan/ditawarkan UBERVEST, termasuk merubah, memodifikasi program pemasaran (system Marketing Plan). Seorang Mitra Usaha UBERVEST menyetujui bahwa pelanggaran terhadap aturan ini dapat menyebabkan status keanggotaannya diberhentikan karena dapat mengakibatkan kerugian yang besar terhadap UBERVEST dan  Mitra Usaha UBERVEST yang lainnya.</li>
                        <li>Seorang  Mitra Usaha UBERVEST bertanggung  jawab sendiri terhadap keputusan bisnisnya dan pengeluarannya.</li>
                        <li>Seorang Mitra Usaha UBERVEST harus mematuhi dan bertindak sejalan dengan  peraturan Keanggotaan UBERVEST, kode etik, program pemasaran dan seluruh kebijakan lainnya.</li>
                        <li>Seorang  Mitra Usaha UBERVEST secara pribadi bertanggung  jawab untuk mematuhi aturan  hukum  yang  berlaku  secara  nasional atau peraturan daerah lainnya.</li>
                        <li>Seorang Mitra Usaha UBERVEST harus bertindak sejalan dengan hukum,  peraturan dan kode etik dalam  menjalankan  kemitraannya dan  tidak  terlibat dalam  aktivitas apapun yang dapat merugikan dirinya sendiri dan UBERVEST.</li>
                        <li>Seorang Mitra Usaha UBERVEST dilarang  melakukan  presentasi kepada calon Mitra Usaha baru atau kepada konsumen yang tidak sesuai atau membuat janji yang tidak dapat dipenuhi. Seorang Mitra Usaha UBERVEST dilarang menyampaikan informasi kepada konsumen atau calon Mitra Usaha baru dengan cara yang salah.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XIV. ATURAN PENJUALAN DAN HARGA JUAL</h4>
                    <ol>
                        <li>Harga jual dari semua produk UBERVEST adalah  harga yang telah ditetapkan oleh UBERVEST dan  tidak ada seorang Mitra Usaha UBERVEST pun diijinkan mengurangi atau menaikkan  harga termasuk dengan cara merubah harga yang tertera pada label  harga atau kemasan  produk UBERVEST.</li>
                        <li>Seorang Mitra Usaha UBERVEST tidak diijinkan mengirim, menyalurkan atau  menjual produk dengan cara memberikan potongan harga, hadiah, promosi yang secara kumulatif atau dengan cara apa pun yang menunjukan bahwa produk dimaksud disalurkan atau dijual lebih rendah atau  lebih  tinggi dari harga jual yang telah ditentukan kecuali semua hal diatas dilakukan  setelah ada persetujuan dari UBERVEST.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XV. KETERLIBATAN DALAM PERUSAHAAN MLM LAINNYA</h4>
                    <ol>
                        <li>Seorang Mitra Usaha UBERVEST baik secara langsung  maupun  tidak langsung, baik atas kehendak sendiri atau bersama dan atas permintaan pihak lain, memperkenalkan atau  merekrut Mitra Usaha UBERVEST yang berada dalam jaringan lain untuk bergabung atau berpartisipasi dalam  kegiatan  penjualan  perusahaan  MLM lainnya atau perusahaan bisnis jaringan atau menyalurkan, menjual atau mempromosikan  produk atau jasa perusahaan Kompetitor  UBERVEST. Apabila seorang Mitra Usaha UBERVEST terbukti terlibat dalam masalah tersebut diatas maka pihak UBERVEST akan membatalkan / memberhentikan status keanggotaan Mitra Usaha UBERVEST tersebut.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XVI. PEMBINAAN DAN PELATIHAN ATAU SUPPORT SYSTEM</h4>
                    <ol>
                        <li>Perusahaan melalui support systemnya UBERVEST  akan mengadakan pembinaan dan program–program pelatihan untuk meningkatkan  kemampuan dan  pengetahuan para Leader UBERVEST agar bertindak  benar,  jujur dan bertanggung jawab.</li>
                        <li>Dalam hal ini Mitra Usaha UBERVEST diwajibkan  untuk mengikuti program pembinaan dan pelatihan yang diadakan oleh Support System  baik secara rutin maupun secara berkala.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XVII. SANKSI-SANKSI</h4>
                    <ol>
                        <li>Jika seorang Mitra Usaha UBERVEST melanggar ketentuan yang  tercantum  dalam peraturan keanggotaan UBERVEST, kodeetik, program  pemasaran dan kebijakan perusahaan lainnya, keanggotaan dari Mitra Usaha UBERVEST yang bersangkutan  akan dibekukan atau dibatalkan.</li>
                        <li>
                            <p>Selama masa penyelidikan yang dilakukan  UBERVEST atau setelah dikeluarkannya surat teguran kepada Mitra Usaha UBERVEST yang melakukan pelanggaran, maka akan diterapkan ketentuan sbb :</p>
                            <ol>
                                <li>UBERVEST dibenarkan melakukan teguran secara lisan atau mengeluarkan sebuah surat teguran kepada Mitra Usaha UBERVEST yang melakukan pelanggaran terhadap peraturan keanggotaan  UBERVEST, kode etik, program pemasaran dan kebijakan lainnya.</li>
                                <li>Surat teguran yang telah dikeluarkan  kepada  Mitra Usaha UBERVEST, maka yang bersangkutan harus menyampaikan penjelasan tertulis perihal kasus yang dituduhkan dalam waktu 14 (empat belas) hari dari tanggal dikeluarkannya surat teguran sebagai bahan pertimbangan bagi UBERVEST. Maka pihak Managemen  UBERVEST berhak membekukan/melarang yang bersangkutan untuk berpartisipasi atau  melakukan  kegiatan yang termasuk melakukan order, melakukan penjualan atau pembelian yang berhubungan dengan produk UBERVEST. Kegiatan yang berhubungan dengan jaringan, sponsor, melakukan perubahan data keanggotaan, mengikuti pelatihan, berpartisipasi dalam kegiatan yang diselenggarakan oleh UBERVEST, berpartisipasi dalam kegiatan promosi atau kegiatan yang mengandung  insentif, menerima bonus, komisi atau insentif sampai ada keputusan akhir dan mengikat yang dikeluarkan oleh UBERVEST.</li>
                                <li>Dalam hal Mitra Usaha UBERVEST yang bersangkutan gagal menyampaikan jawaban tertulis dimaksud dalam waktu yang telah ditentukan, UBERVEST berhak mengambil tindakan yang dianggap pantas dan layak.</li>
                                <li>Berdasarkan  informasi yang diperoleh dari sumber terpercaya atau dari hasil penyelidikan yang dilakukan UBERVEST terhadap pernyataan dan fakta yang diperoleh bersamaan dengan informasi yang disampaikan kepada Perusahaan  selama masa tenggang waktu pemberian jawaban, perusahaan  akan mengambil putusan akhir yang selayaknya untuk itu yang meliputi pembatalan status sebagai Mitra Usaha UBERVEST, kode etik, program pemasaran atau kebijakan perusahaan lainnya berdasar kasus demi kasus. Perusahaan akan memberitahukan keputusan yang diambil kepada Mitra Usaha UBERVEST  yang bersangkutan dan  tindakan yang diambil berlaku efektif segera setelah putusan dimaksud dikeluarkan.</li>
                                <li>Dalam hal terjadi pembatalan status Keanggotaan yang bersangkutan dan semua keuntungan yang mungkin diperoleh sesuai dengan  program  pemasaran UBERVEST termasuk tetapi tidak terbatas pada keuntungan yang dapat diperoleh dari kegiatan  promosi, kegiatan yang mengandung  insentif serta hak-hak lainnya akan ditiadakan. Sejak tanggal pembatalan di maksud Mitra Usaha UBERVEST yang bersangkutan secara otomatis dilarang untuk terlibat dalam kegiatan apa pun yang berhubungan dengan  produk, Program Pemasaran UBERVEST.</li>
                                <li>Mitra Usaha UBERVEST yang telah dicabut keanggotaannya dapat mengajukan permohonan  menjadi Mitra Usaha UBERVEST yang baru setelah  melampaui masa 2 (dua) bulan terhitung dari tanggal surat pembatalan keanggotaan dikeluarkan. Tetapi persetujuan permohonan dimaksud  harus diverifikasi dan mendapat persetujuan dari Managemen UBERVEST.</li>
                            </ol>
    
                        </li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XVII. PENGUNAAN NAMA & LOGO PERUSAHAAN</h4>
                    <ol>
                        <li>Logo UBERVEST, merek dagang, merek jasa, nama produk dan  harta komersil lainnya yang terlihat maupun  tidak  terlihat, terdaftar atau tidak terdaftar, rekaman video, peralatan kantor, barang-barang cetakan, yang diperuntukkan dan berhubungan dengan UBERVEST adalah hak milik UBERVEST.Oleh karena itu, hak milik tersebut tidak boleh digunakan, dipindahkan atau diproduksi ulang oleh Mitra Usaha UBERVEST tanpa persetujuan lebih dahulu dari PT. Untung Bersama Investindo.</li>
                        <li>Tidak diperkenankan memasang spanduk yang berhubungan dengan perusahaan sebelum  mendapatkan izin tertulis dari UBERVEST.</li>
                        <li>Mitra Usaha UBERVEST yang  hendak membuka stand pameran produk UBERVEST dalam suatu acara bisnis harus terlebih dahulu mendapat izin secara tertulis dari UBERVEST.</li>
                    </ol>
                </li>
    
                <li class="mb-5 no-number">
                    <h4>BAB XIX. PENUTUP</h4>
                    <p>Demikian Kode etik dan peraturan ini dibuat adalah untuk keamanan  perusahaan PT. Untung Bersama Investindo dan untuk  kenyamanan para Mitra Usaha UBERVEST dalam  menjalankan  usahanya.</p>
                    <p>UBERVEST mempunyai hak penuh dan mutlak kapan saja bahkan tanpa pemberitahuan terlebih dahulu untuk merubah, membedakan atau menambah atau menyesuaikan peraturan Keanggotaan UBERVEST, kode etik, program pemasaran dan kebijakan lainnya.</p>
                </li>
            </ol>
    </div>
    <div class="w-100" style="background:white; height:100px;"></div>
    <div class="" style="position: absolute; left:0; right:0; bottom:0; background:white; height:100px; border-top: 2px solid rgba(0, 0, 0, 0.25)">
        <div class="d-flex p-4">
            <button class="btn btn-privacy-policy-close btn-primary mx-auto">Saya Mengerti</button>
        </div>
    </div>
</div>