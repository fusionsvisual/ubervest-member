@extends('layouts/dashboard_master')

@section('title')
BONUS KANTONG KANAN
@endsection

@section('dashboard-css')
<style>
    .input-container input {
        box-shadow: none;
        padding-left: 16px !important;
        border-radius: 0px;
    }
</style>
@endsection

@section('content')
<div class="row mb-4">

    <div class="col-12 col-md-7 col-xl-7 mb-4 mb-md-0">
        <div class="right-bonus-card h-100 d-flex" style="position: relative;background: linear-gradient(105.44deg, #0F417C -5.68%, #0075FF 26.57%, #77FFE7 118.16%);">
            <div class="h-100 d-flex flex-column p-4 p-lg-5 w-100" style="position: absolute; top:0; left:0;">
                <div class="d-flex justify-content-between mb-3 mb-md-0" style="">
                    <h1 class="text-white my-auto">Commision Sharing</h1>
                    <div class="logo" style="height: 75px; width:75px">
                        <object data="{{ asset('images/ic_logo_icon.svg') }}" class="w-100 h-100" type="image/svg+xml"></object>
                    </div>

                </div>
                <div class="mt-auto">
                    <p style="color: #CDE6FF">WITHDRAWABLE</p>
                    <h1 class="text-white font-bold">Rp{{number_format(Session::get('user.data')->commision_sharing, 0, ',', '.')}}</h1>
                    <h3 class="text-white">Rp{{number_format(Session::get('user.data')->total_commision_sharing, 0, ',', '.')}}</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-5 col-xl-5">
        <div class="rounded-container p-4">
            @if(Session::has('response'))   
                <p class="@if(Session::get('response')->success) text-success @else text-danger @endif mb-3">**{{ Session::get('response')->message }}</p>
            @endif
            <form method="POST" onsubmit="$('.btn-withdraw').prop('disabled', true); return true;">
                @csrf
                <p class="mb-2 font-medium">Amount</p>
                <div class="input-container d-flex mb-3" style="height:55px;">
                    <div class="d-inline-block">
                        <div class="h-100 d-flex" style="border-right:1px solid #c4c4c4;">
                            <p class="my-auto mx-3">Rp.</p>
                        </div>
                    </div>
                    <div class="d-inline-block w-100">
                        <input type="number" name="amount" id="" placeholder="Rp0" class="w-100 h-100 pl-3" style="border:0px; border-top-right-radius:8px; border-bottom-right-radius:8px;" required>
                    </div>
                </div>
                <p class="mb-2 font-medium">Keterangan</p>
                <textarea class="form-control mb-3" name="description" id="" cols="1" rows="5" class="w-100" style="border: 1px solid #ced4da!important; box-shadow:none;" required></textarea>
                <input class="form-control btn-withdraw" type="submit" value="Withdraw">
            </form>
        </div>
    </div>
</div>

{{-- <form action=""> --}}
    <div class="row pb-3">
        <div class="col-12 col-md-7 mb-3">
        </div>

        <div class="col-12 col-md-5 mb-3">
            <div class="row d-flex flex-wrap">
                <div class="col-5">
                    <input class="form-control" type="text" name="start_date" value="02 December 2020">
                </div>
                <div class="col-2 d-flex px-0">
                    <p class="m-auto text-muted">sampai</p>
                </div>
                <div class="col-5">
                    <input class="form-control" type="text" name="end_date" value="31 December 2020">
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="table-responsive mb-4 mb-md-4">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Date</th>
                            <th scope="col">Username</th>
                            <th scope="col">Commision</th>
                        </tr>
                    </thead>
                    <tbody class="item-list" style="overflow-y: auto;height: 120px;">
                        @foreach ($sharings->data as $idx => $sharing)
                        <tr>
                            <td scope="row">{{ $idx+1 }}</td>
                            <td>{{ date_format(date_create($sharing->created_at), "d F Y") }}</td>
                            <td>{{ $sharing->commision_from_user_id->username }}</td>
                            <td>{{ $sharing->commision }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="pagination-container">
                @include('templates.pagination', ["items" => $sharings, "page" => $page])
            </div>
        </div>
    </div>
{{-- </form> --}}

@endsection

@section('dashboard-javascript')
<script type="text/javascript" src="{{ asset('js/pagination.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script>
    $url = "{{ url('right_bonus/commision_sharing/filter') }}";
    $token = "{{ csrf_token() }}";
    $totalCount = "{{ $sharings->total/$sharings->per_page }}"
    
    $(document).ready(function(){
        $('input[name="start_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top left",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $startDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth(), 1));

        $('input[name="end_date"]').datepicker({
            format: 'dd MM yyyy',
            orientation: "top right",
            todayHighlight: true,
            autoclose: true,
        }).on('changeDate', function(e){
            $endDate = $(this).val();
            $page = 1;
            filter()
        }).datepicker('update', new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0));
    });
</script>
@endsection