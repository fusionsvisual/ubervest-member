$url = "";
$search_query = "";
$startDate = "";
$endDate = "";
$page = 1;
$token = "";
$level_query = "";
$totalCount = 0

$(document).ready(function () {
    setupButton();
});

function setupButton() {

    $('.btn-prev').click(function (e) {
        if ($page > 1){
            $page = $page - 1;
            updatePageItem()
            filter();
        }
    });

    $('.btn-next').click(function (e) {
        console.log($totalCount)
        if ($page < $totalCount){
            $page = $page + 1;
            updatePageItem()
            filter();
        }
    });

    $('.btn-page').click(function (e) {
        $page = $(this).data('page');
        updatePageItem()
        filter();
    });
}

function updatePageItem() {
    $('.page-number-item').removeClass('active')
    $('.page-number-item[data-page="'+$page+'"]').addClass('active')
}

function filter() {
    $startDate = $('input[name="start_date"]').val();
    $endDate = $('input[name="end_date"]').val();

    $('.item-list').empty();

    // console.log($url);

    $.ajax({
        type: "POST",
        url: $url,
        data: {
            "_token": $token,
            "page": $page,
            "search_query": $search_query,
            "level_query": $level_query,
            "start_date": $startDate,
            "end_date": $endDate
        },
        dataType: "json",
        success: function (response) {
            // console.log(response);

            $totalCount = response.total_count
            updatePageItem()
            $('.pagination-container').html(response.pagination);
            setupButton()

            $('.item-list').html(response.items);
            didFilterDone()
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            console.log(thrownError);
        }
    });
}

function didFilterDone(){
    
}
