<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AdminController;
use App\Http\Middleware\AdminAuth;
use App\Http\Middleware\MemberAuth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('test_email', [DashboardController::class, 'test_email']);


Route::any('/', [HomeController::class, 'index']);
Route::any('login', [AuthController::class, 'login']);
Route::any('forgot_password', [DashboardController::class, 'forgot_password']);

Route::prefix('/payment')->group(function (){
    Route::get('/{token}', [DashboardController::class, 'payment']);
    Route::post('/{token}/credit_card', [DashboardController::class, 'payment_credit_card']);
    Route::any('/{token}/{provider}', [DashboardController::class, 'payment_detail']);
});

Route::middleware([MemberAuth::class])->group(function () {
    Route::any('logout', [AuthController::class, 'logout']);

    Route::any('register', [DashboardController::class, 'register']);

    Route::post('change_password', [DashboardController::class, 'change_password']);

    Route::any('edit_profile', [DashboardController::class, 'edit_profile']);
    Route::post('get_cities', [DashboardController::class, 'get_cities']);

    Route::prefix('/purchase_package')->group(function (){
        Route::any('buy', [DashboardController::class, 'purchase_package_buy']);
        Route::prefix('history')->group(function (){
            Route::any('/', [DashboardController::class, 'purchase_package_history']);
            Route::post('filter', [DashboardController::class, 'purchase_package_filter']);
            Route::any('/{id}', [DashboardController::class, 'purchase_package_detail']);
            Route::any('/{id}/update', [DashboardController::class, 'purchase_package_update']);

        });

        // Route::any('history', [DashboardController::class, 'purchase_package_history']);
    });

    // Route::any('purchase_package', [DashboardController::class, 'purchase_package']);

    Route::prefix('/upload_data')->group(function (){
        Route::any('/', [DashboardController::class, 'upload_data']);
        Route::any('/upload', [DashboardController::class, 'upload_data_upload']);
    });
    
    Route::prefix('/network/network_tree')->group(function (){
        Route::any('/', [DashboardController::class, 'network_tree']);
        Route::post('filter', [DashboardController::class, 'network_tree_filter']);
    });
    // Route::get('/network/network_tree', [DashboardController::class, 'network_tree']);

    Route::prefix('/network/left_leg')->group(function (){
        Route::get('/', [DashboardController::class, 'left_leg']);
        Route::get('/{level}', [DashboardController::class, 'available_left_leg']);
        Route::post('/filter', [DashboardController::class, 'left_leg_filter']);
    });

    Route::prefix('/network/right_leg')->group(function (){
        Route::get('/', [DashboardController::class, 'right_leg']);
        Route::get('/{level}', [DashboardController::class, 'available_right_leg']);
        Route::post('/filter', [DashboardController::class, 'right_leg_filter']);
    });

    Route::prefix('/network/personal_sponsor')->group(function (){
        Route::any('/', [DashboardController::class, 'personal_sponsor']);
        Route::post('/filter', [DashboardController::class, 'personal_sponsor_filter']);
    });

    Route::prefix('left_bonus/bonus')->group(function (){
        Route::any('/', [DashboardController::class, 'bonus']);
    });

    // Route::get('left_bonus/bonus', [DashboardController::class, 'bonus']);
    Route::any('left_bonus/reward', [DashboardController::class, 'reward']);
    Route::prefix('right_bonus/profit_sharing')->group(function (){
        Route::any('/', [DashboardController::class, 'profit_sharing']);
        Route::any('filter', [DashboardController::class, 'profit_sharing_filter']);
    });

    Route::prefix('right_bonus/commision_sharing')->group(function (){
        Route::any('/', [DashboardController::class, 'commision_sharing']);
        Route::any('filter', [DashboardController::class, 'commision_sharing_filter']);
    });
   
    Route::prefix('withdraw_history')->group(function (){
        Route::prefix('left_pocket')->group(function (){
            Route::get('/', [DashboardController::class, 'withdraw_left_pocket']);
            Route::post('filter', [DashboardController::class, 'withdraw_left_pocket_filter']);
            Route::get('{id}', [DashboardController::class, 'withdraw_left_pocket_detail']);
        });

        Route::prefix('right_pocket')->group(function (){
            Route::get('/', [DashboardController::class, 'withdraw_right_pocket']);
            Route::post('filter', [DashboardController::class, 'withdraw_right_pocket_filter']);
            Route::get('{id}', [DashboardController::class, 'withdraw_right_pocket_detail']);
        });
    });
});

Route::any('admin/login', [AdminController::class, 'login']);
Route::middleware([AdminAuth::class])->group(function () {
    Route::prefix('admin')->group(function (){   
        Route::get('/', [AdminController::class, 'index']); 
        Route::get('logout', [AdminController::class, 'logout']);
        Route::post('change_password', [AdminController::class, 'change_password']);
        Route::prefix('purchase_history')->group(function (){
            Route::get('/', [AdminController::class, 'purchaseHistory']);
            Route::post('filter', [AdminController::class, 'purchase_history_filter']);
            Route::get('/{id}', [AdminController::class, 'purchase_history_detail']);
            Route::post('/{id}/update', [AdminController::class, 'purchase_history_update']);
        });

        Route::prefix('verification_request')->group(function(){
            Route::any('/', [AdminController::class, 'verificationRequest']);
            Route::post('filter', [AdminController::class, 'verification_request_filter']);
        });

        Route::prefix('member_deposit')->group(function(){
            Route::get('/', [AdminController::class, 'memberDeposit']);
            Route::any('create', [AdminController::class, 'createMemberDeposit']);
            Route::get('update', [AdminController::class, 'memberDeposit']);
        });
    
        Route::prefix('member_sharing')->group(function(){
            Route::prefix('profit')->group(function(){
                Route::any('/', [AdminController::class, 'member_profit_sharing']);
                Route::post('filter', [AdminController::class, 'member_sharing_filter']);
                Route::any('/{id}', [AdminController::class, 'member_profit_sharing_update']);
            });
            Route::prefix('commission')->group(function(){
                Route::any('/', [AdminController::class, 'member_commission_sharing']);
                Route::post('filter', [AdminController::class, 'member_commission_sharing_filter']);
                Route::any('/{id}', [AdminController::class, 'member_commission_sharing_update']);
            });
            Route::post('filter', [AdminController::class, 'member_sharing_filter']);
        });

        Route::prefix('claimed_reward')->group(function(){
            Route::any('/', [AdminController::class, 'claimedReward']);
            Route::post('filter', [AdminController::class, 'claimed_reward_filter']);
        });
    
        Route::prefix('member_list')->group(function(){
            Route::get('/', [AdminController::class, 'memberList']);
            Route::post('filter', [AdminController::class, 'member_list_filter']);
            Route::post('create_epin', [AdminController::class, 'member_list_create_epin']);
            Route::any('/{id}', [AdminController::class, 'member_list_update']);
            Route::any('/delete/{id}', [AdminController::class, 'member_list_delete']);
            Route::get('ban/{id}', [AdminController::class, 'member_list_ban']);
            Route::get('unban/{id}', [AdminController::class, 'member_list_unban']);
        });
    
        Route::prefix('withdrawal_request')->group(function(){
            Route::prefix('right_pocket')->group(function(){
                Route::get('/', [AdminController::class, 'withdrawalRequestRightLeg']);
                Route::get('/{id}', [AdminController::class, 'withdraw_right_pocket_detail']);
                Route::post('filter', [AdminController::class, 'withdrawal_request_right_pocket_filter']);
                Route::any('/send/{id}', [AdminController::class, 'withdraw_right_pocket_send']);
                Route::any('/reject/{id}', [AdminController::class, 'withdraw_right_pocket_reject']);
            });
            Route::prefix('left_pocket')->group(function(){
                Route::any('/', [AdminController::class, 'withdrawalRequestLeftPocket']);
                Route::get('/{id}', [AdminController::class, 'withdraw_left_pocket_detail']);
                Route::post('/filter', [AdminController::class, 'withdrawal_request_left_pocket_filter']);
                Route::any('/send/{id}', [AdminController::class, 'withdraw_left_pocket_send']);
                Route::any('/reject/{id}', [AdminController::class, 'withdraw_left_pocket_reject']);
            });
        });

        Route::prefix('epin_list')->group(function(){
            Route::get('/', [AdminController::class, 'epin_list']);
            Route::post('filter', [AdminController::class, 'epin_list_filter']);
            // Route::post('create_epin', [AdminController::class, 'member_list_create_epin']);
            // Route::any('/{id}', [AdminController::class, 'member_list_update']);
            Route::any('/delete/{id}', [AdminController::class, 'epin_list_delete']);
            // Route::get('ban/{id}', [AdminController::class, 'member_list_ban']);
            // Route::get('unban/{id}', [AdminController::class, 'member_list_unban']);
        });
    });
});






