<?php

namespace App\Constants;

class OrderStatus {
   const PAID = 'PAID';
   const UNPAID = 'UNPAID';
   const WAITING = 'WAITING';
   const REJECT = 'REJECT';
}

?>