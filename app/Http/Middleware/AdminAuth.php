<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuth
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('user')){
            if ($request->session()->get('user.role') == "admin"){
                return $next($request);
            }else{
                $request->session()->forget('user');
                return redirect('admin/login');
            }
        }else{
            return redirect('admin/login');
        }
    }
}