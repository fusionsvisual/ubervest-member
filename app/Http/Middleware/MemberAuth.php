<?php

namespace App\Http\Middleware;

use Closure;

class MemberAuth
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('user.role') == "member"){
            return $next($request);
        }else{
            $request->session()->forget('user');
            return redirect('login');
        }
    }
}