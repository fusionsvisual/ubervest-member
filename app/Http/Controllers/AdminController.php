<?php

namespace App\Http\Controllers;

use App\Constants\OrderStatus;
use App\Helper\Helper;
use Illuminate\Http\Request;
use PDO;
use PhpParser\Node\Stmt\Echo_;

class AdminController extends Controller
{

    private $paginate = 10;

    public function index(Request $request){
        if ($request->session()->has('user')){
            if ($request->session()->get('user.role') == "admin"){
                return redirect('admin/purchase_history');
            }else{
                $request->session()->forget('user');
                return redirect('admin/login');
            }
        }else{
            return redirect('admin/login');
        }
    }

    public function login(Request $request){
        if ($request->method() == "POST"){
            $params = [
                "username" => $request->username,
                "password" => $request->password
            ];
            $response = Helper::publicApi('admin/login', $params, 'POST');
            if ($response->success){
                $request->session()->put('user.token', $response->data->token);
                $request->session()->put('user.role', "admin");
                $userResp = Helper::privateApi('admin/profile/details', [], 'GET');
                if ($userResp->success){
                    $request->session()->put('user.data', $userResp->data);
                    return redirect('admin/purchase_history');
                }
            }else{
                return redirect()->back()->with('response', $response);
            }
        }
        return view('admin.admin_login');
    }

    public function change_password(Request $request){
        $params = [
            "password" => $request->old_password,
            "new_password" => $request->new_password
        ];
        $response = Helper::privateApi("admin/profile/change_password", $params, "POST");
        return json_encode($response);
    }

    public function purchaseHistory(Request $request){  
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $histories = Helper::privateApi("admin/purchase_history/read", $params, "GET");

        $data = [
            "histories" => $histories->data,
            "page" => 1
        ];
        return view('admin.admin_purchase_history', $data);
    }

    public function purchase_history_detail(Request $request){
        $history = Helper::privateApi("admin/purchase_history/details", ["purchase_room_id" => $request->id], "GET");
        $data = [
            "history" => $history->data
        ];
        return view('admin.admin_purchase_history_detail', $data);
    }

    public function purchase_history_filter(Request $request){

        $params = [
            "invoice_code" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page,
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d")
        ];

        $histories = Helper::privateApi("admin/purchase_history/read", $params, "GET");
        $data = [
            "items" => $histories->data,
            "page" => $request->page
        ];

        $response = [
            "total_count" => $histories->data->total/$histories->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_purchase_history_item', $data)->render()
        ];

        return $response;
    }

    public function purchase_history_update(Request $request){
        $params = [
            "purchase_room_id" => $request->id,
            "action" => $request->status
        ];

        if ($request->status == OrderStatus::REJECT){
            $params["reject_reason"] = $request->rejected_reason;
        }

        $response = Helper::privateApi("admin/purchase_room/admin_update", $params, "PATCH");
        return redirect()->back()->with('response', $response);
    }

    public function verificationRequest(Request $request){  

        $data = [];

        if ($request->method() == 'POST'){
            $params = [
                "identity_id" => $request->identity_id,
                "action" => $request->action   
            ];
            $response = Helper::privateApi("admin/identity/update", $params, "PATCH");
            return redirect()->back()->with('response', $response);
        }

        $params = [
            "paginate" => $this->paginate,
        ];
        $verifications = Helper::privateApi("admin/identity/read", $params, "GET");  
        $data["requests"] = $verifications->data;
        $data["page"] = 1;

        return view('admin.admin_verification_request', $data);
    }

    public function verification_request_filter(Request $request){  

        $params = [
            "paginate" => $this->paginate,
            "username" => $request->search_query,
            "page" => $request->page
        ];

        $requests = Helper::privateApi("admin/identity/read", $params, "GET");  
        $data = [
            "items" => $requests->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];

        $response = [
            "total_count" => $requests->data->total/$requests->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_verification_request_item', $data)->render()
        ];

        return $response;
    }

    public function memberDeposit(Request $request){    
        return view('admin.admin_member_deposit');
    }

    public function createMemberDeposit(Request $request){    
        switch ($request->method()) {
            case 'POST':
                return redirect('admin/member_deposit');
                break;
            
            default:
                return view('admin.input_deposit');
                break;
        }
    }

    public function member_profit_sharing(Request $request){   
        if ($request->method() == 'POST'){
            $params = [
                "body" => [],
                "files" => [
                    "file" => file_get_contents($_FILES["csv"]['tmp_name'])
                ],
                "ext" => pathinfo($_FILES["csv"]['name'], PATHINFO_EXTENSION)
            ];
            $response = Helper::privateApi('admin/profit_sharing/upload_csv', $params, 'UPLOAD_FILE');
            // echo json_encode($response);
            return redirect()->back()->with('response', $response);
        }

        $params = [
            "paginate" => $this->paginate
        ];
        $sharings =  Helper::privateApi("admin/member_sharing/read", $params, "GET");
        $data = [
            "sharings" => $sharings->data,
            "page" => 1
        ];
        return view('admin.admin_member_profit_sharing', $data);
    }

    public function member_sharing_filter(Request $request){ 
        $params = [
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];

        $sharings = Helper::privateApi("admin/member_sharing/read", $params, "GET");
        $data = [
            "items" => $sharings->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];

        $response = [
            "total_count" => $sharings->data->total/$sharings->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_profit_sharing_item', $data)->render()
        ];

        return $response;
    }

    public function member_commission_sharing(Request $request){   
        if ($request->method() == 'POST'){
            $params = [
                "body" => [],
                "files" => [
                    "file" => file_get_contents($_FILES["csv"]['tmp_name'])
                ],
                "ext" => pathinfo($_FILES["csv"]['name'], PATHINFO_EXTENSION)
            ];
            $response = Helper::privateApi('admin/commision_sharing/upload_csv', $params, 'UPLOAD_FILE');
            // echo json_encode($response);

            return redirect()->back()->with('response', $response);
        }
        
        $params = [
            "paginate" => $this->paginate
        ];
        $sharings =  Helper::privateApi("admin/member_sharing/read", $params, "GET");
        $data = [
            "sharings" => $sharings->data,
            "page" => 1
        ];
        return view('admin.admin_member_commission_sharing', $data);
    }

    public function member_commission_sharing_filter(Request $request){ 
        $params = [
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];

        $sharings = Helper::privateApi("admin/member_sharing/read", $params, "GET");
        $data = [
            "items" => $sharings->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];

        $response = [
            "total_count" => $sharings->data->total/$sharings->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_commission_sharing_item', $data)->render()
        ];

        return $response;
    }

    public function member_commission_sharing_update(Request $request){   
        if($request->method() == 'POST'){
            $params = [
                "user_id" => $request->id,
                "commision_sharing" => $request->commission_sharing_amount,
            ];
            $response =  Helper::privateApi("admin/member_sharing/update", $params, "PATCH");
            return redirect('admin/member_sharing/commission')->with('response', $response);
        }
        $sharing = Helper::privateApi("admin/member_sharing/read", ["user_id" => $request->id], "GET");
        $data = [
            "sharing" => $sharing->data
        ];
        return view('admin.forms.admin_commission_sharing_form', $data);
    }

    public function member_profit_sharing_update(Request $request){  
        if($request->method() == 'POST'){
            $params = [
                "user_id" => $request->id,
                "profit_sharing" => $request->profit_sharing_amount,
            ];
            $response = Helper::privateApi("admin/member_sharing/update", $params, "PATCH");
            return redirect('admin/member_sharing/profit')->with('response', $response);
        }  
        $sharing = Helper::privateApi("admin/member_sharing/read", ["user_id" => $request->id], "GET");
        $data = [
            "sharing" => $sharing->data
        ];
        return view('admin.forms.admin_profit_sharing_form', $data);
    }

    public function claimedReward(Request $request){  
        $data = [];
        if ($request->method() == 'POST'){
            $response = Helper::privateApi("admin/claimed_reward/review", ["claimed_reward_id" => $request->claimed_reward_id], "PATCH");
            // echo json_encode($response);
            return redirect()->back()->with('response', $response);
        }
        
        $params = [
            "paginate" => $this->paginate
        ];
        $rewards = Helper::privateApi("admin/claimed_reward/read", $params, "GET");
        $data["claimed_rewards"] = $rewards->data;
        $data["page"] = 1;
        
        return view('admin.admin_claimed_reward', $data);
    }

    public function claimed_reward_filter(Request $request){ 
        $params = [
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];

        $rewards = Helper::privateApi("admin/claimed_reward/read", $params, "GET");
        $data = [
            "items" => $rewards->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];

        $response = [
            "total_count" => $rewards->data->total/$rewards->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_claimed_reward_item', $data)->render()
        ];

        return $response;
    }

    public function memberList(Request $request){   
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $users = Helper::privateApi("admin/user/read_all_user", $params, "GET");
        $data = [
            "users" => $users->data,
            "page" => 1
        ];
        return view('admin.admin_member_list', $data);
    }

    public function member_list_filter(Request $request){   
        $params = [
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $users = Helper::privateApi("admin/user/read_all_user", $params, "GET");
        
        $data = [
            "items" => $users->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];
        
        $response = [
            "total_count" => $users->data->total/$users->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_member_list_item', $data)->render()
        ];
        return $response;
    }

    public function member_list_update(Request $request){    
        $users = Helper::privateApi("admin/user/read_all_user", [], "GET");
        $user = Helper::privateApi("admin/user/read_all_user", ["user_id" => $request->id], "GET");

        // echo json_encode($user);

        switch ($request->method()) {
            case 'POST':
                $params = [
                    "user_id" => $request->id,
                    "sponsor_user_id" => $request->sponsor_id,
                    "meta_id" => $request->meta_id,
                    "username" => $request->username,
                    "email" => $request->email,
                    "phone_number" => $request->phone_number,
                    "province_name" => $request->province_name,
                    "city_name" => $request->city_name,
                    "bank_code" => $request->bank_code,
                    "account_holder_name" => $request->account_holder_name,
                    "account_number" => $request->account_number,
                    "deposit" => $request->deposit,
                    "fast_track_bonus" => $request->fast_track_bonus,
                    "total_fast_track_bonus" =>  $user->data->total_fast_track_bonus - ($user->data->fast_track_bonus - $request->fast_track_bonus),
                    "seasonal_bonus" => $request->seasonal_bonus,
                    "total_seasonal_bonus" => $user->data->total_seasonal_bonus - ($user->data->seasonal_bonus - $request->seasonal_bonus),
                    "link_up_bonus" => $request->link_up_bonus,
                    "total_link_up_bonus" => $user->data->total_link_up_bonus - ($user->data->link_up_bonus - $request->link_up_bonus),
                    "profit_sharing" => $request->profit_sharing,
                    "total_profit_sharing" => $user->data->total_profit_sharing - ($user->data->profit_sharing - $request->profit_sharing),
                    "commision_sharing" => $request->commision_sharing,
                    "total_commision_sharing" => $user->data->total_commision_sharing - ($user->data->commision_sharing - $request->commision_sharing),
                    "temporary_left_bv" => $request->temporary_left_bv,
                    "temporary_right_bv" => $request->temporary_right_bv,
                    "left_bv" => $request->left_bv,
                    "right_bv" => $request->right_bv,
                ];
                $response = Helper::privateApi("admin/user/update_user", $params, "PATCH");
                return redirect()->back()->with('response', $response);
                break;
            
            default:
                $data = [
                    "users" => $users->data->data,
                    "user" => $user->data
                ];
                
                return view('admin.input_member', $data);
                break;
        }
    }

    public function member_list_ban(Request $request){   
        $params = [
            "user_id" => $request->id,
            "action" => "ban"
        ];
        $response =  Helper::privateApi("admin/user/banned", $params, "PATCH");
        return redirect()->back()->with('response', $response);
    }

    public function member_list_unban(Request $request){    
        $params = [
            "user_id" => $request->id,
            "action" => "unban"
        ];
        $response =  Helper::privateApi("admin/user/banned", $params, "PATCH");
        // echo json_encode($response);
        return redirect()->back()->with('response', $response);
    }

    public function member_list_delete(Request $request){    
        $params = [
            "user_id" => $request->id,
        ];
        $response = Helper::privateApi("admin/user/delete_account", $params, "DELETE");
        return redirect()->back()->with('response', $response);
    }

    public function member_list_create_epin(Request $request){    
        $params = [
            "purchase_packages" => [
                ["package_id" => $request->package_id] 
            ]
        ];
        $response = Helper::privateApi("admin/purchase_room/create", $params, "POST");
        // print(json_encode($params));
        // print(json_encode($response));
        return redirect()->back()->with('response', $response);
    }

    public function withdrawalRequestLeftPocket(Request $request){  
        if ($request->method() == "POST"){
            $params = [
                "withdraw_id" => $request->withdraw_id,
                "action" => $request->action,
            ];
            echo json_encode($params);
            $response = Helper::privateApi("admin/withdraw/update", $params, "PATCH");
            return redirect()->back()->with('response', $response);
        }else{

        }
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $withdraws = Helper::privateApi("admin/withdraw/read", $params, "GET");
        
        $data = [
            "withdraws" => $withdraws->data,
            "page" => 1
        ];  
        return view('admin.admin_withdraw_left_pocket', $data);
    }

    public function withdraw_left_pocket_detail(Request $request){   
        $params = [
            "withdraw_id" => $request->id
        ];
        $withdraw = Helper::privateApi("admin/withdraw/read", $params, "GET");
        $data = [
            "withdraw" => $withdraw->data
        ];
        return view('admin.forms.admin_withdraw_left_pocket_form', $data);
    }

    public function withdrawal_request_left_pocket_filter(Request $request){
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $withdraws = Helper::privateApi("admin/withdraw/read", $params, "GET");
        // echo json_encode($withdraws);        
        $data = [
            "items" => $withdraws->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];
        
        $response = [
            "total_count" => $withdraws->data->total/$withdraws->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_withdrawal_request_item', $data)->render()
        ];
        return $response;
    }

    public function withdraw_left_pocket_send(Request $request){   
        $params = [
            "action" => "send",
            "withdraw_id" => $request->id
        ];
        $response = Helper::privateApi("admin/withdraw/update", $params, "PATCH");
        return redirect('admin/withdrawal_request/left_pocket')->with('response', $response);
    }

    public function withdraw_left_pocket_reject(Request $request){   
        $params = [
            "action" => "reject",
            "withdraw_id" => $request->id
        ];
        $response = Helper::privateApi("admin/withdraw/update", $params, "PATCH");
        return redirect('admin/withdrawal_request/left_pocket')->with('response', $response);
    }

    public function withdrawalRequestRightLeg(Request $request){   
        if ($request->method() == "POST"){
            $params = [
                "withdraw_id" => $request->withdraw_id,
                "action" => $request->action,
            ];
            echo json_encode($params);
            $response = Helper::privateApi("admin/withdraw/update", $params, "PATCH");
            return redirect()->back()->with('response', $response);
        }else{

        }
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $withdraws = Helper::privateApi("admin/profit_commision_sharing_withdraw/read", $params, "GET");
        $data = [
            "withdraws" => $withdraws->data,
            "page" => 1
        ];   
        return view('admin.admin_withdraw_right_pocket', $data);
    }

    public function withdrawal_request_right_pocket_filter(Request $request){
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $withdraws = Helper::privateApi("admin/profit_commision_sharing_withdraw/read", $params, "GET");
        $data = [
            "items" => $withdraws->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];
        
        $response = [
            "total_count" => $withdraws->data->total/$withdraws->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_withdrawal_right_pocket_item', $data)->render()
        ];
        return $response;
    }

    public function withdraw_right_pocket_detail(Request $request){   
        $params = [
            "profit_commision_sharing_withdraw_id" => $request->id
        ];
        $withdraw = Helper::privateApi("admin/profit_commision_sharing_withdraw/read", $params, "GET");
        $data = [
            "withdraw" => $withdraw->data
        ];
        return view('admin.forms.admin_withdraw_right_pocket_form', $data);
    }

    public function withdraw_right_pocket_send(Request $request){   
        $params = [
            "action" => "send",
            "profit_commision_sharing_withdraw_id" => $request->id
        ];
        $response = Helper::privateApi("admin/profit_commision_sharing_withdraw/update", $params, "PATCH");
        return redirect('admin/withdrawal_request/right_pocket')->with('response', $response);
    }

    public function withdraw_right_pocket_reject(Request $request){   
        $params = [
            "action" => "reject",
            "profit_commision_sharing_withdraw_id" => $request->id
        ];
        $response = Helper::privateApi("admin/profit_commision_sharing_withdraw/update", $params, "PATCH");
        return redirect('admin/withdrawal_request/right_pocket')->with('response', $response);
    }

    public function epin_list(Request $request){
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $response = Helper::privateApi("admin/purchase_package/admin_read", $params, "GET");
        $data = [
            "e_pins" => $response->data,
            "page" => 1
        ];
        return view('admin.admin_epin_list', $data);
    }

    public function epin_list_filter(Request $request){
        $params = [
            "username" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $epins = Helper::privateApi("admin/purchase_package/admin_read", $params, "GET");
        
        $data = [
            "items" => $epins->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];
        
        $response = [
            "total_count" => $epins->data->total/$epins->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('admin.items.admin_epin_list_item', $data)->render()
        ];
        return $response;
    }

    public function epin_list_delete(Request $request){
        $params = [
            "purchase_package_id" => $request->id,
        ];  
        $response = Helper::privateApi("admin/purchase_package/delete", $params, "DELETE");
        return redirect()->back()->with('response', $response);
    }

    public function logout(Request $request){
        $request->session()->forget('user');
        return redirect('admin/login');
    }
}
