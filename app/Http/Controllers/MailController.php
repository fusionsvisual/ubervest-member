<?php

namespace App\Http\Controllers;

use App\Helper\DateFormatter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailController extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        date_default_timezone_set('Asia/Jakarta');
        return $this->subject('Please complete your payment')
        ->view('mail.email', ["data" => $this->data]);
    }
}
