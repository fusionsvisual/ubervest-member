<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    private $paginate = 10;

    public function __construct()
    {
        date_default_timezone_set("Asia/Bangkok");

    }

    public function register(Request $request){
        $params = [
            "username" => $request->username,
            "email" => $request->email,
            "e_pin" => $request->e_pin,
            "leg" => $request->leg,
            "phone_number" => $request->phone_number,
            "gender_id" => $request->gender_id,
            "province_name" => $request->province,
            "city_name" => $request->city,
            "upline_user_id" => $request->upline_id,
            "bank_code" => $request->bank_code,
            "account_number" => $request->account_number,
            "account_holder_name" => $request->account_holder_name
        ];
        $response = Helper::privateApi('user/register', $params, 'POST');
        return redirect()->back()->with('response', $response);
    }

    public function change_password(Request $request){
        $params = [
            "password" => $request->old_password,
            "new_password" => $request->new_password
        ];
        $data = [];

        $response = Helper::privateApi('user/profile/change_password', $params, 'PATCH');        

        if ($response->success){
            $userResp = Helper::privateApi('user/profile/details', [], 'GET');
            $request->session()->put('user.data', $userResp->data);
        }
        return json_encode($response);
    }

    public function forgot_password(Request $request){
        $params = [
            "email" => $request->email
        ];
        $response = Helper::publicApi('user/password/email', $params, 'POST');
        return redirect()->back()->with('response', $response);
    }

    public function edit_profile(Request $request){  

        $data = [];

        if ($request->method() == 'POST'){
            $params = [
                "phone_number" => $request->phone_number,
                "heir_name" => $request->heir_name,
                "heir_relationship" => $request->heir_relationship,
                "email" => $request->email,
                "gender_id" => $request->gender_id,
                "province_name" => $request->province_name,
                "city_name" => $request->city_name,
                "postal_code" => $request->postal_code,
                "account_number" => $request->account_number,
                "account_holder_name" => $request->account_holder_name,
                "bank_code" => $request->bank_code,
                "npwp" => $request->npwp_number
            ];

            $response = Helper::privateApi('user/profile/update', $params, 'PATCH');
            $userResp = Helper::privateApi('user/profile/details', [], 'GET');
            $request->session()->put('user.data', $userResp->data);
            return redirect()->back()->with('response', $response);
        }

        $user = $request->session()->get('user.data');

        return view('edit_profile', $data);
    }

    public function get_cities(Request $request){  

        $cities = Helper::privateApi('user/city/read', ["province_id" => $request->province_id], 'GET');
        return $cities->data;
    }
    
    public function purchase_package_buy(Request $request){  

        $packages = Helper::privateApi('user/package/read', [], 'GET');
        
        if ($request->method() == 'POST'){
            $params = [
                "email" => $request->email,
                "phone_number" => $request->phone_number,
                "description" => $request->description
            ];
            $params['purchase_packages'] = [];
            foreach($packages->data as $idx => $package){
                if($request->input("package".$package->id) > 0){
                    for ($i=0; $i < $request->input("package".$package->id); $i++) { 
                        array_push($params['purchase_packages'], [
                            "package_id" => $package->id
                        ]);
                    }
                }
            }
            $response = Helper::privateApi('user/purchase_room/create', $params, 'POST');
            if ($response->success){
                // Mail::to($request->email)->send(new MailController($response->data));
                return redirect('purchase_package/history/'.$response->data->purchase_room->id)->with('is_new', true);
            }
            return redirect()->back()->with('response', $response);
        }

        $data = [
            "packages" => $packages->data
        ];
        return view('purchase_package', $data);
    }

    public function purchase_package_update(Request $request){
        $uploadParams = [
            "body" => [],
            "images" => [
                "image" => $request->payment_receipt
            ]
        ];
        $uploadResp = json_decode(Helper::privateApi('user/purchase_room/upload_payment_receipt', $uploadParams, 'UPLOAD'));
        if ($uploadResp->success){
            $imageUrl = $uploadResp->data;

            $updateParams = [
                "payment_receipt" => $imageUrl,
                "purchase_room_id" => $request->id,
                "bank_name" => $request->bank_name,
                "account_name" => $request->bank_account_name,
                "account_number" => $request->bank_account_number
            ];
            $updateResp = Helper::privateApi('user/purchase_room/input_evidence_of_transfer', $updateParams, 'PATCH');
            return redirect()->back()->with('response', $updateResp);
        }else{
            return redirect()->back()->with('response', $uploadResp);
        }
    }

    public function upload_data(Request $request){     
        
        $data = [];
        
        if($request->method() == "POST"){

            $params = [
                "thickmill_phone_number" => $request->tickmill_phone,
                "identity_card" => $request->identity_card_link,
                "proof_of_address" => $request->proof_of_address_link
            ];

            $response = Helper::privateApi('user/profile/upload_data', $params, 'PATCH');
            // if (!$response->success){
            //     $data["message"] = $response->message;
            // }else{
            //     return redirect('upload_data');
            // }
            return redirect()->back()->with('response', $response);
        }

        $userResp = Helper::privateApi('user/profile/details', [], 'GET');
        $request->session()->put('user.data', $userResp->data);

        return view('upload_data', $data);
    }

    public function upload_data_upload(Request $request){
        $params = [
            "body" => [
                "type" => $request->type
            ],
            "images" => [
                "image" => file_get_contents($_FILES["image"]['tmp_name'])
            ]
        ];
        $response = Helper::privateApi('user/profile/upload_data_image', $params, 'UPLOAD');
        return $response;
    }

    public function network_tree(Request $request){   

        $user_id = $request->session()->get('user.data')->id;

        if ($request->method() == 'POST'){
            return redirect('network/network_tree')->with("upline_id", $request->upline_id);
        }

        if ($request->session()->get('upline_id')){
            $user_id = $request->session()->get('upline_id');
        }

        $userResp = Helper::privateApi('user/profile/details', [], 'GET');
        $request->session()->put('user.data', $userResp->data);

        $user = $request->session()->get('user.data');
        $network_tree = Helper::privateApi("user/network/tree", ["user_id" => $user_id], "GET");


        $ePin_codes = Helper::privateApi("user/purchase_package/read",["is_used" => false], "GET");
        $data = [
            "network_tree" => $network_tree->data,
            "ePin_codes" => $ePin_codes->data,
            "user" => $user
        ];
        return view('network_tree', $data);
    }

    public function network_tree_filter(Request $request){           
        $network_tree = Helper::privateApi("user/network/tree",["user_id" => $request->user_id], "GET");

        if ($request->method == 'BACK'){
            $network_tree = Helper::privateApi("user/network/tree",["user_id" => $network_tree->data[0]->upline_user_id], "GET");
        }
        
        $data = [
            "items" => view('items/network_tree_item', ["network_tree" => $network_tree->data])->render(),
            "current_branch" => $network_tree->data[0]
        ];

        return $data;
    }

    public function left_leg(){
        $params = [
            "which_leg" => "LEFT",
            "paginate" => $this->paginate
        ];
        $legs = Helper::privateApi('user/network/read_leg', $params, "GET");
        $total_level = Helper::privateApi('user/network/list_of_levels', ["which_network" => "leg"], "GET");

        $available_legs = Helper::privateApi('user/network/read_empty_user', ["which_leg" => "LEFT"], "GET");
        $data = [
            "legs" => $legs->data,
            "available_legs" => $available_legs->data,
            "total_level" => $total_level->data,
            "page" => 1
        ];
        return view('left_leg', $data);
    }

    public function left_leg_filter(Request $request){
        $params = [
            "which_leg" => "LEFT",
            "paginate" => $this->paginate,
            "page" => $request->page,
            "level" => $request->level_query,
            "search_by_username" => $request->search_query
        ];

        $legs = Helper::privateApi("user/network/read_leg", $params, "GET");
        $data = [
            "items" => $legs->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];

        $response = [
            "total_count" => $legs->data->total/$legs->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.leg_item', $data)->render()
        ];

        return $response;
    }

    public function available_left_leg(Request $request){
        $available_legs = Helper::privateApi('user/network/read_upline_empty_user', ["which_leg" => "LEFT", "level" => $request->route('level')], "GET");
        $data = [
            "available_legs" => $available_legs->data,
        ];
        return view('available_left_leg', $data);
    }

    public function right_leg(){        
        $params = [
            "which_leg" => "RIGHT",
            "paginate" => $this->paginate
        ];
        $legs = Helper::privateApi('user/network/read_leg', $params, "GET");
        $total_level = Helper::privateApi('user/network/list_of_levels', ["which_network" => "leg"], "GET");

        $available_legs = Helper::privateApi('user/network/read_empty_user', ["which_leg" => "RIGHT"], "GET");
        $data = [
            "legs" => $legs->data,
            "available_legs" => $available_legs->data,
            "total_level" => $total_level->data,
            "page" => 1
        ];
        return view('right_leg', $data);
    }

    public function right_leg_filter(Request $request){
        $params = [
            "which_leg" => "RIGHT",
            "paginate" => $this->paginate,
            "page" => $request->page,
            "level" => $request->level_query,
            "search_by_username" => $request->search_query
        ];

        $legs = Helper::privateApi("user/network/read_leg", $params, "GET");
        $data = [
            "items" => $legs->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];

        $response = [
            "total_count" => $legs->data->total/$legs->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.leg_item', $data)->render()
        ];

        return $response;
    }

    public function available_right_leg(Request $request){
        $available_legs = Helper::privateApi('user/network/read_upline_empty_user', ["which_leg" => "RIGHT", "level" => $request->route('level')], "GET");
        $data = [
            "available_legs" => $available_legs->data,
        ];
        return view('available_right_leg', $data);
    }

    public function personal_sponsor(Request $request){   

        if ($request->method() == 'POST'){
            $params = [
                "user_id" => $request->user_id,
                "e_pin" => $request->e_pin
            ];
            // print json_encode($params);
            $response = Helper::privateApi('user/profile/upgrade_user', $params, "PATCH");
            // print json_encode($response);
            return redirect()->back()->with('response', $response);
        }

        $total_level = Helper::privateApi('user/network/list_of_levels', ["which_network" => "sponsor"], "GET");
        $params = [
            "paginate" => $this->paginate,
        ];

        $ePin_codes = Helper::privateApi("user/purchase_package/read_epin_upgrade", array(), "GET");
        $members = Helper::privateApi('user/network/read_personal_sponsor', $params, "GET");
        $data = [
            "members" => $members->data,
            "total_level" => $total_level->data,
            "ePin_codes" => $ePin_codes->data,
            "page" => 1
        ];

        return view('personal_sponsor', $data);
    }

    public function personal_sponsor_filter(Request $request){

        $params = [
            "paginate" => $this->paginate,
            "page" => $request->page,
            "level" => $request->level_query,
            "search_by_username" => $request->search_query
        ];

        // echo json_encode($params);

        $members = Helper::privateApi('user/network/read_personal_sponsor', $params, "GET");
        // echo json_encode($members);
        $data = [
            "items" => $members->data,
            "page" => $request->page,
            "paginate" => $this->paginate,
        ];

        $response = [
            "total_count" => $members->data->total/$members->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.personal_sponsor_item', $data)->render()
        ];
        return $response;

    }

    public function bonus(Request $request){   
        $data = [];
        if($request->method() == 'POST'){
            $params = [
                "seasonal_withdraw_amount" => $request->seasonal,
                "fast_track_withdraw_amount" => $request->fast_track,
                "link_up_withdraw_amount" => $request->link_up,
                "description" => $request->description
            ];
            $response = Helper::privateApi('user/withdraw/create', $params, "POST");

            return redirect()->back()->with('response', $response);
        }     
        $userResp = Helper::privateApi('user/profile/details', [], 'GET');
        $request->session()->put('user.data', $userResp->data);

        return view('bonus', $data);
    }

    public function bonus_withdraw(Request $request){   
        
    }

    public function reward(Request $request){ 
        
        if ($request->method() == "POST"){
            $response = Helper::privateApi('user/claimed_reward/create', ["reward_id" => $request->reward_id], "POST"); 
            $userResp = Helper::privateApi('user/profile/details', [], 'GET');
            $request->session()->put('user.data', $userResp->data);

            return redirect()->back()->with('response', $response);
        }else{
            $data = [];
            $rewards = Helper::privateApi('user/reward/read', [], "GET");   
            $data["rewards"] = $rewards->data;
            return view('reward', $data);
        }
    }

    public function profit_sharing(Request $request){  
        if ($request->method() == "POST"){
            $params = [
                "type" => "profit",
                "amount" => $request->amount,
                "description" => $request->description
            ];
            $response = Helper::privateApi('user/profit_commision_sharing_withdraw/create', $params, "POST"); 
            return redirect()->back()->with('response', $response);
        }
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $sharings = Helper::privateApi("user/profit_sharing/read", $params, "GET");
        $data = [
            "sharings" => $sharings->data,
            "page" => 1
        ];   
        $userResp = Helper::privateApi('user/profile/details', [], 'GET');
        $request->session()->put('user.data', $userResp->data);  

        return view('profit_sharing', $data);
    }

    public function profit_sharing_filter(Request $request){ 
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $sharings = Helper::privateApi("user/profit_sharing/read", $params, "GET");
        $data = [
            "items" => $sharings->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];
        
        $response = [
            "total_count" => $sharings->data->total/$sharings->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.profit_sharing_item', $data)->render()
        ];
        return $response;
    }

    public function commision_sharing(Request $request){   
        if ($request->method() == "POST"){
            $params = [
                "type" => "commision",
                "amount" => $request->amount,
                "description" => $request->description
            ];
            $response = Helper::privateApi('user/profit_commision_sharing_withdraw/create', $params, "POST"); 
            return redirect()->back()->with('response', $response);
        }

        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $sharings = Helper::privateApi("user/commision_sharing/read", $params, "GET");
        $data = [
            "sharings" => $sharings->data,
            "page" => 1
        ];
        $userResp = Helper::privateApi('user/profile/details', [], 'GET');
        $request->session()->put('user.data', $userResp->data);  

        return view('commision_sharing', $data);
    }

    public function commision_sharing_filter(Request $request){ 
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $sharings = Helper::privateApi("user/commision_sharing/read", $params, "GET");
        $data = [
            "items" => $sharings->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];
        
        $response = [
            "total_count" => $sharings->data->total/$sharings->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.commission_sharing_item', $data)->render()
        ];
        return $response;
    }

    public function withdraw_left_pocket(Request $request){        
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $withdraws = Helper::privateApi("user/withdraw/read", $params, "GET");
        $data = [
            "withdraws" => $withdraws->data,
            "page" => 1
        ];
        return view('withdraw_left_pocket', $data);
    }

    public function withdraw_left_pocket_detail(Request $request){   
        $params = [
            "withdraw_id" => $request->id
        ];
        $withdraw = Helper::privateApi("user/withdraw/read", $params, "GET");
        $data = [
            "withdraw" => $withdraw->data
        ];
        return view('forms.withdraw_left_pocket_form', $data);
    }

    public function withdraw_left_pocket_filter(Request $request){        
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $histories = Helper::privateApi("user/withdraw/read", $params, "GET");
        
        $data = [
            "items" => $histories->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];
        
        $response = [
            "total_count" => $histories->data->total/$histories->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.withdraw_left_pocket_item', $data)->render()
        ];
        return $response;
    }

    public function withdraw_right_pocket(Request $request){        
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $withdraws = Helper::privateApi("user/profit_commision_sharing_withdraw/read", $params, "GET");
        $data = [
            "withdraws" => $withdraws->data,
            "page" => 1
        ];
        return view('withdraw_right_pocket', $data);
    }

    public function withdraw_right_pocket_detail(Request $request){   
        $params = [
            "profit_commision_sharing_withdraw_id" => $request->id
        ];
        $withdraw = Helper::privateApi("user/profit_commision_sharing_withdraw/read", $params, "GET");
        $data = [
            "withdraw" => $withdraw->data
        ];
        return view('forms.withdraw_right_pocket_form', $data);
    }

    public function withdraw_right_pocket_filter(Request $request){        
        $params = [
            "start_date" => date_format(date_create($request->start_date),"Y-m-d"),
            "end_date" => date_format(date_create($request->end_date),"Y-m-d"),
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        $histories = Helper::privateApi("user/profit_commision_sharing_withdraw/read", $params, "GET");
        
        $data = [
            "items" => $histories->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];
        
        $response = [
            "total_count" => $histories->data->total/$histories->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.withdraw_right_pocket_item', $data)->render()
        ];
        return $response;
    }

    public function purchase_package_history(){ 
        $params = [
            "paginate" => $this->paginate,
            "page" => 1
        ];
        $histories = Helper::privateApi("user/purchase_room/read", $params, "GET");
        $data = [
            "histories" => $histories->data,
            "page" => 1
        ];
        return view('purchase_package_history', $data);
    }

    public function purchase_package_filter(Request $request){ 

        $params = [
            "invoice_code" => $request->search_query,
            "paginate" => $this->paginate,
            "page" => $request->page
        ];
        
        if ($request->start_date != ""){
            $params['start_date'] = date_format(date_create($request->start_date),"Y-m-d");
        }

        if ($request->end_date != ""){
            $params['end_date'] = date_format(date_create($request->end_date),"Y-m-d");
        }

        $histories = Helper::privateApi('user/purchase_room/read', $params, "GET");
        
        $data = [
            "items" => $histories->data,
            "page" => $request->page,
            "paginate" => $this->paginate
        ];
        
        $response = [
            "total_count" => $histories->data->total/$histories->data->per_page,
            "pagination" => view('templates.pagination', $data)->render(),
            "items" => view('items.purchase_package_history_item', $data)->render()
        ];
        return $response;
    }

    public function purchase_package_detail(Request $request){  
        $history = Helper::privateApi('user/purchase_room/details', ["purchase_room_id" => $request->id], "GET");    
        $data = [
            "history" => $history->data
        ];
        return view('purchase_history_detail', $data);
    }

    public function payment(Request $request){
        $purchase_room = Helper::privateApi('user/purchase_room/read_by_token', ["token" => $request->token], "GET");    
        $data = [
            "purchase_room" => $purchase_room->data
        ];
        return view('payment', $data);
    }

    public function payment_credit_card(Request $request){
        $params = [
            "token_id" => $request->token_id,
            "invoice_code" => $request->invoice_code,
            "amount" => ($request->amount + ($request->amount * 29/971)) + 2000
        ];
        echo json_encode($params);
        $response = Helper::privateApi('user/xendit/credit_card_create_charge', $params, "POST");  
        echo json_encode($response);  
        if ($response->success){
            return redirect('purchase_package')->with('response', $response);
        }else{
            return redirect()->back()->with('response', $response);
        }
    }

    public function payment_detail(Request $request){

        $purchase_room = Helper::privateApi('user/purchase_room/details', ["purchase_room_id" => $request->id], "GET");    
        $data = [
            "purchase_room" => $purchase_room->data
        ];

        switch ($request->provider) {
            case "mandiri":
            case "bri":
            case "bni":
            case "permata":
            case "bca":
                $params = [
                    "external_id" => "va-".$request->provider."-".$purchase_room->data->id,
                    "bank_code" => strtoupper($request->provider),
                    "expected_amount" => $purchase_room->data->total,
                ];
                $response = Helper::privateApi('user/xendit/create_virtual_account_payment', $params, "POST");    
                $data = [
                    "response" => $response->data
                ];
                return view('payment_bank_transfer', $data);
                break;
            default:
                if ($request->method() == "POST"){
                    
                    $params = [
                        "invoice_code" => $purchase_room->data->invoice_code,
                        "amount" => $purchase_room->data->total,
                        "phone_number" => $request->phone_number,
                    ];

                    if ($request->provider == 'ovo'){
                        $response = Helper::privateApi('user/xendit/create_ovo_payment', $params, "POST");
                        if ($response->success){
                            return redirect('purchase_package/buy');
                        }else{
                            return redirect()->back()->with('response', $response);
                        }
                    }else{
                        $response = Helper::privateApi('user/xendit/create_dana_payment', $params, "POST");
                        if ($response->success){
                            return redirect($response->data->checkout_url);
                        }else{
                            return redirect()->back()->with('response', $response);
                        }
                    }
                }
                return view('payment_ewallet', $data);
                break;
        }
    }
}
