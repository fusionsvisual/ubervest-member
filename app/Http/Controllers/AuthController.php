<?php

namespace App\Http\Controllers;

use App\Constants\NetworkStatus;
use App\Helper\Helper;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request){    
        $data = [];

        if ($request->method() == 'POST'){
            $params = [
                "username" => $request->username,
                "password" => $request->password
            ];
            $response = Helper::publicApi('user/login', $params, 'POST');
            // echo json_encode($response->success);
            if ($response->success){
                $request->session()->put('user.token', $response->data->token);
                $request->session()->put('user.role', "member");
                $userResp = Helper::privateApi('user/profile/details', [], 'GET');
                
                if ($userResp->success){
                    $request->session()->put('user.data', $userResp->data);
                    return redirect('purchase_package/buy');
                }else{
                    $data['message'] = $response->message;
                }
            }else{
                $data['message'] = $response->message;
            }
        }

        return view('login', $data);
    }

    public function register(){
        return view('register');
    }

    public function logout(Request $request){
        $request->session()->forget('user');
        return redirect('login');
    }

    public function storeDataForm(){
        return view('store_data_form');
    }
}
